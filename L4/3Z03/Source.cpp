#include <iostream>
#include <string>

#include "MyBinaryTree.h"

int main()
{
	MyBinaryTree<std::string> tree;

	tree.addElement(true, "glowny");
	tree.addElement(true, "pierwszy prawy");
	tree.addElement(true, "drugi prawy");


	//tree.moveUp();
	tree.moveUp();
	tree.showNodeInfo();
	
	tree.removeTree();
	tree.showNodeInfo();

	while (1);
	return 0;
}