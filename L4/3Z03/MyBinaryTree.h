#pragma once

#include "MyHeap.h"

template<class T>
class MyBinaryTree
{
	struct Node
	{
		T Value;
		Node *Root;
		Node *Lnode;
		Node *Rnode;

		int NodeLevel;		//poziom wezla = dl. sciezki
		int NodeDegree;		//liczba krawedzi z i do wezla
		int NodeHeight;		//najdluzsza scierzka od wezla do liscia
	};

	Node *MainRoot;
	Node *CurrentRoot;

	int TreeHeight;			//najdluzsza sciezka od korzenia do liscia

	void increaseHeight(const int &h, Node *n);	//zwieksza (o ile istnieje taka potrzeba) wartosc wysokosci wezla/drzewa
	void decreaseHeight(const int &h, Node *n);	//zmniejsza (o ile istnieje taka potrzeba) wartosc wysokosci wezla/drzewa

	bool remove(Node*);		//rekurencyjne usuwanie el. pod wezlem

public:
	MyBinaryTree();
	~MyBinaryTree();

	void addElement(const bool &toRight, const T &val);
	bool removeElement();
	void removeTree();

	void moveUp();
	void moveDown(const bool &toRight);

	void showPath() const;
	void showNodeInfo() const;
	//void showTree() const;

};

template<class T>
MyBinaryTree<T>::MyBinaryTree()
{
	MainRoot = CurrentRoot = nullptr;
	TreeHeight = 0;
}

template<class T>
MyBinaryTree<T>::~MyBinaryTree()
{
}

template<class T>
void MyBinaryTree<T>::addElement(const bool &toRight, const T &val)
{
	if (CurrentRoot == nullptr)
	{
		MainRoot = CurrentRoot = new Node;
		MainRoot->Value = val;
		MainRoot->Root = nullptr;
		MainRoot->Lnode = nullptr;
		MainRoot->Rnode = nullptr;

		MainRoot->NodeLevel = 0;
		MainRoot->NodeDegree = 0;
		MainRoot->NodeHeight = 0;

		TreeHeight = 0;

		return;
	}

	if (toRight && CurrentRoot->Rnode != nullptr) return;
	if (!toRight && CurrentRoot->Lnode != nullptr) return;

	Node *temp = new Node;

	temp->Value = val;
	temp->Root = CurrentRoot;
	temp->Lnode = nullptr;
	temp->Rnode = nullptr;

	temp->NodeLevel = CurrentRoot->NodeLevel + 1;
	temp->NodeDegree = 1;
	CurrentRoot->NodeDegree += 1;
	temp->NodeHeight = 0;

	if (toRight) CurrentRoot->Rnode = temp;
	else CurrentRoot->Lnode = temp;

	CurrentRoot = temp;

	increaseHeight(1, CurrentRoot);
}


template<class T>
bool MyBinaryTree<T>::remove(Node *n)
{
	if (n->Rnode == nullptr && n->Lnode == nullptr) return true;

	if (n->Rnode != nullptr)
		if (remove(n->Rnode)) delete n->Rnode;

	if (n->Lnode != nullptr)
		if (remove(n->Lnode)) delete n->Lnode;

	return false;
}


template<class T>
bool MyBinaryTree<T>::removeElement()
{
	if (CurrentRoot == nullptr) return false;
	if (CurrentRoot->Rnode != nullptr || CurrentRoot->Lnode != nullptr)
	{
		char a;
		std::cout << "Czy chcesz usunac poddrzewo? [t/n] ";
		std::cin >> a;

		if (a == 'n'&&a == 'N') return false;
		
		remove(CurrentRoot);
	}
	

	Node *temp = CurrentRoot;
	CurrentRoot = CurrentRoot->Root;

	decreaseHeight(temp->NodeHeight, temp);

	if (CurrentRoot != nullptr)
	{
		if (temp == CurrentRoot->Rnode) CurrentRoot->Rnode = nullptr;
		else CurrentRoot->Lnode = nullptr;
	}
	else
		MainRoot = nullptr;

	delete temp;
	return true;
}


template<class T>
void MyBinaryTree<T>::removeTree()
{
	if (MainRoot == nullptr) return;

	remove(MainRoot);

	delete MainRoot;
	MainRoot = CurrentRoot = nullptr;
}


template<class T>
void MyBinaryTree<T>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<T> *path = new MyStack<T>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != NULL)
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}

void MyBinaryTree<std::string>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<std::string> *path = new MyStack<std::string>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != "")
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}


template<class T>
void MyBinaryTree<T>::showNodeInfo() const
{
	if (MainRoot == nullptr) return;

	std::cout << "NodeLevel(dl. sciezki do wezla): " << CurrentRoot->NodeLevel <<
		"\nNodeDegree(ilosc polaczen): " << CurrentRoot->NodeDegree <<
		"\nNodeHeight(dl. sciezki od wezla): " << CurrentRoot->NodeHeight << std::endl;

	showPath();
	std::cout << std::endl;
}


template<class T>
void MyBinaryTree<T>::increaseHeight(const int &h, Node *n)
{
	if (n->Root == nullptr)
	{
		if (n->NodeHeight > TreeHeight)
			TreeHeight = n->NodeHeight;

		return;
	}

	n = n->Root;

	if (n->NodeHeight < h)
	{
		n->NodeHeight = h;
		increaseHeight(h + 1, n);
	}
}


template<class T>
void MyBinaryTree<T>::decreaseHeight(const int &h, Node *n)
{
	if (n->Root == nullptr)	return;

	if (n == n->Root->Rnode)
	{
		if (n->Root->Lnode == nullptr)
		{
			n->Root->NodeHeight = n->Root->NodeHeight - (h + 1);
			decreaseHeight(h, n->Root);
		}
		else if (n->NodeHeight > n->Root->Lnode->NodeHeight)
		{
			n->Root->NodeHeight = n->Root->Lnode->NodeHeight + 1;
			decreaseHeight(h, n->Root);
		}
	}

	if (n == n->Root->Lnode)
	{
		if (n->Root->Rnode == nullptr)
		{
			n->Root->NodeHeight = n->Root->NodeHeight - (h + 1);
			decreaseHeight(h, n->Root);
		}
		else if (n->NodeHeight > n->Root->Rnode->NodeHeight)
		{
			n->Root->NodeHeight = n->Root->Rnode->NodeHeight + 1;
			decreaseHeight(h, n->Root);
		}
	}

}


template<class T>
void MyBinaryTree<T>::moveUp()
{
	if (CurrentRoot->Root == nullptr) return;

	CurrentRoot = CurrentRoot->Root;
}


template<class T>
void MyBinaryTree<T>::moveDown(const bool &toRight)
{
	if (toRight && CurrentRoot->Rnode == nullptr) return;
	if (!toRight && CurrentRoot->Lnode == nullptr) return;

	if(toRight) CurrentRoot = CurrentRoot->Rnode;
	else CurrentRoot = CurrentRoot->Lnode;
}