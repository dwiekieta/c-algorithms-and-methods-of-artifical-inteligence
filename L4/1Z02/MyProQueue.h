#pragma once

#include "MyProQueueRoot.h"

template<class T>
class MyProQueue
{
	MyDeque<MyProQueueRoot<T>*> *QueueRoot;

	int MaxPriority;

	int searchRoot(const int&);

public:
	MyProQueue();
	~MyProQueue();

	void addElement(const int &p, const int &val);

	void showQueue(const bool &fromMaximum = true);
};

template<class T>
MyProQueue<T>::MyProQueue()
{
	MaxPriority = 0;
	QueueRoot = new MyDeque<MyProQueueRoot<T>*>;
}

template<class T>
MyProQueue<T>::~MyProQueue()
{
	delete QueueRoot;
}

template<class T>
int MyProQueue<T>::searchRoot(const int &r)
{
	if (QueueRoot->isEmpty()) return -1;

	int i = 0;
	for (; i < QueueRoot->size(); ++i) 
		if ((*QueueRoot)[i]->getPriority() == r) return i;

		return -1;
}

template<class T>
void MyProQueue<T>::addElement(const int &p, const int &val)
{
	int root = searchRoot(p);

	if (p > MaxPriority) MaxPriority = p;

	if (QueueRoot->isEmpty() || root == -1)
	{
		QueueRoot->addFrontElement(new MyProQueueRoot<T>(p));
		(*QueueRoot)[0]->addElement(val);

		return;
	}
	
	(*QueueRoot)[root]->addElement(val);
}









template<class T>
void  MyProQueue<T>::showQueue(const bool &fromMaximum = true)
{
	if (QueueRoot->isEmpty()) return;
	
	int root;

	if(fromMaximum)
		for (int i = MaxPriority; i >= 0; --i)
		{
			root = searchRoot(i);
			if(root != -1)
			(*QueueRoot)[root]->showRoot();
		}
	else
		for (int i = 0; i <= MaxPriority; ++i)
		{
			root = searchRoot(i);
			if (root != -1)
				(*QueueRoot)[root]->showRoot();
		}
}