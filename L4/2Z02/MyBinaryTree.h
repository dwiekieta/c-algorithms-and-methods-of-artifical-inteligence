#pragma once

#include "MyStack.h"

template<class T>
class MyBinaryTree
{
	struct Node
	{
		T Value;
		Node *Root;
		Node *Lnode;
		Node *Rnode;

		int NodeLevel;		//poziom wezla = dl. sciezki
		int NodeDegree;		//liczba krawedzi z i do wezla
		int NodeHeight;		//najdluzsza scierzka od wezla do liscia
	};

	Node *MainRoot;
	Node *CurrentRoot;

	int TreeHeight;			//najdluzsza sciezka od korzenia do liscia

	void increaseHeight(const int &h, Node *n);	//zwieksza (o ile istnieje taka potrzeba) wartosc wysokosci wezla/drzewa
	void decreaseHeight(const int &h, Node *n);	//zmniejsza (o ile istnieje taka potrzeba) wartosc wysokosci wezla/drzewa

	void addInRandomPlace(Node*, const T&);

	bool remove(Node*);		//rekurencyjne usuwanie el. pod wezlem
	void show(Node*);		//rekurencyjne wyswietlenie wartosci wezla;
	void showInOrder(Node*);
	void showPostOrder(Node*);

public:
	MyBinaryTree();
	~MyBinaryTree();

	bool addElement(const bool &toRight, const T &val);
	void addElement(const T&);
	bool removeElement();
	void earseElement(const bool &toRight);		//usuwa el. bez usuwania poddrzewa po stronie parametru
	void removeTree();

	void moveUp();
	void moveDown(const bool &toRight);

	void showPath() const;
	void showNodeInfo() const;
	void showTree();

	bool isEmpty() const;
	int getTreeHeight() const;
	bool isLastOnLevel() const;

	void preOrder();
	void inOrder();
	void postOrder();
};

template<class T>
MyBinaryTree<T>::MyBinaryTree()
{
	MainRoot = CurrentRoot = nullptr;
	TreeHeight = 0;
}

template<class T>
MyBinaryTree<T>::~MyBinaryTree()
{
	removeTree();
}

template<class T>
void MyBinaryTree<T>::increaseHeight(const int &h, Node *n)
{
	if (n->Root == nullptr)
	{
		if (n->NodeHeight > TreeHeight)
			TreeHeight = n->NodeHeight;

		return;
	}

	n = n->Root;

	if (n->NodeHeight < h)
	{
		n->NodeHeight = h;
		increaseHeight(h + 1, n);
	}
}


template<class T>
void MyBinaryTree<T>::decreaseHeight(const int &h, Node *n)
{
	if (n->Root == nullptr)	return;

	/*if (n == n->Root->Rnode)
	{
		if (n->Root->Lnode == nullptr)
		{
			n->Root->NodeHeight = n->Root->NodeHeight - (h + 1);
			decreaseHeight(h, n->Root);
		}
		else if (n->NodeHeight > n->Root->Lnode->NodeHeight)
		{
			n->Root->NodeHeight = n->Root->Lnode->NodeHeight + 1;
			decreaseHeight(h, n->Root);
		}
	}*/

	if (n == n->Root->Lnode)
	{
		if (n->Root->Rnode == nullptr)
		{
			n->Root->NodeHeight = n->Root->NodeHeight - (h + 1);
			decreaseHeight(h, n->Root);
		}
		else if (n->NodeHeight > n->Root->Rnode->NodeHeight)
		{
			n->Root->NodeHeight = n->Root->Rnode->NodeHeight + 1;
			decreaseHeight(h, n->Root);
		}
	}

}

template<class T>
void MyBinaryTree<T>::addInRandomPlace(Node *n, const T &val)
{
	bool r = (bool)(rand() % 2);

	CurrentRoot = n;

	if (CurrentRoot == MainRoot && r) return;
	if (this->addElement(r, val)) return;

	if (r) addInRandomPlace(n->Rnode, val);
	else addInRandomPlace(n->Lnode, val);
}

template<class T>
void MyBinaryTree<T>::show(Node *n)
{
	if (n == nullptr) return;

	if (n->Root == nullptr)  std::cout << "[" << n->NodeLevel << "] " << n->Value << std::endl;
	else
	{
		for (int i = 0; i < n->NodeLevel; ++i) std::cout << ' ';

		std::cout << "[" << n->NodeLevel << "] " << n->Value << std::endl;
	}

	show(n->Lnode);
	show(n->Rnode);
}

template<class T>
void MyBinaryTree<T>::showInOrder(Node *n)
{
	if (n == nullptr) return;

	showInOrder(n->Lnode);

	for (int i = 0; i < n->NodeLevel; ++i) std::cout << ' ';
	if (n->Root == nullptr) std::cout << "[" << n->NodeLevel << "] " << n->Value << std::endl;
	else
	{
		if (n == n->Root->Lnode) std::cout << "[" << n->NodeLevel << "] L: " << n->Value << std::endl;
		else std::cout << "[" << n->NodeLevel << "] R: " << n->Value << std::endl;
	}

	showInOrder(n->Rnode);
}

template<class T>
void MyBinaryTree<T>::showPostOrder(Node *n)
{
	if (n == nullptr) return;

	showInOrder(n->Lnode);
	showInOrder(n->Rnode);

	for (int i = 0; i < n->NodeLevel; ++i) std::cout << ' ';
	if (n->Root == nullptr) std::cout << "[" << n->NodeLevel << "] " << n->Value << std::endl;
	else
	{
		if (n == n->Root->Lnode) std::cout << "[" << n->NodeLevel << "] L: " << n->Value << std::endl;
		else std::cout << "[" << n->NodeLevel << "] R: " << n->Value << std::endl;
	}
}

template<class T>
bool MyBinaryTree<T>::addElement(const bool &toRight, const T &val)
{
	if (CurrentRoot == nullptr)
	{
		MainRoot = CurrentRoot = new Node;
		MainRoot->Value = val;
		MainRoot->Root = nullptr;
		MainRoot->Lnode = nullptr;
		MainRoot->Rnode = nullptr;

		MainRoot->NodeLevel = 0;
		MainRoot->NodeDegree = 0;
		MainRoot->NodeHeight = 0;

		TreeHeight = 0;

		return true;
	}

	if (toRight && CurrentRoot->Rnode != nullptr) return false;
	if (!toRight && CurrentRoot->Lnode != nullptr) return false;

	Node *temp = new Node;

	temp->Value = val;

	temp->Lnode = nullptr;
	temp->Rnode = nullptr;

	if (toRight)
	{
		temp->Root = CurrentRoot->Root;
		temp->NodeLevel = CurrentRoot->NodeLevel;
		if(CurrentRoot->Root != nullptr) CurrentRoot->Root->NodeDegree += 1;
		CurrentRoot->Rnode = temp;
	}
	else
	{
		temp->Root = CurrentRoot;
		temp->NodeLevel = CurrentRoot->NodeLevel + 1;
		CurrentRoot->NodeDegree += 1;
		CurrentRoot->Lnode = temp;
	}

	temp->NodeDegree = 1;
	temp->NodeHeight = 0;

	CurrentRoot = temp;

	if(!toRight) increaseHeight(1, CurrentRoot);

	return true;
}

template<class T>
void MyBinaryTree<T>::addElement(const T &val)
{
	addInRandomPlace(MainRoot, val);
}

template<class T>
bool MyBinaryTree<T>::remove(Node *n)
{
	if (n->Rnode == nullptr && n->Lnode == nullptr) return true;

	if (n->Rnode != nullptr)
		if (remove(n->Rnode)) delete n->Rnode;

	if (n->Lnode != nullptr)
		if (remove(n->Lnode)) delete n->Lnode;

	return false;
}


template<class T>
bool MyBinaryTree<T>::removeElement()
{
	if (CurrentRoot == nullptr) return false;
	if (CurrentRoot->Rnode != nullptr || CurrentRoot->Lnode != nullptr)
	{
		char a;
		std::cout << "Czy chcesz usunac poddrzewo? [t/n] ";
		std::cin >> a;

		if (a == 'n'&&a == 'N') return false;
		
		remove(CurrentRoot);
	}
	

	Node *temp = CurrentRoot;
	CurrentRoot = CurrentRoot->Root;

	decreaseHeight(temp->NodeHeight, temp);

	if (CurrentRoot != nullptr)
	{
		if (temp == CurrentRoot->Rnode) CurrentRoot->Rnode = nullptr;
		else CurrentRoot->Lnode = nullptr;
	}
	else
		MainRoot = nullptr;

	delete temp;
	return true;
}

template<class T>
void MyBinaryTree<T>::earseElement(const bool &toRight)
{
	if (CurrentRoot == nullptr) return;

	if (toRight && CurrentRoot->Rnode == nullptr) return;
	if (!toRight && CurrentRoot->Lnode == nullptr) return;
	
	Node *temp;

	if (toRight)
	{
		if (CurrentRoot->Rnode->Lnode != nullptr)
		{
			remove(CurrentRoot->Rnode->Lnode);
			delete CurrentRoot->Rnode->Lnode;
		}

		temp = CurrentRoot->Rnode;
		CurrentRoot->Rnode = CurrentRoot->Rnode->Rnode;
	}
	else
	{
		if (CurrentRoot->Lnode->Rnode != nullptr)
		{
			remove(CurrentRoot->Lnode->Rnode);
			delete CurrentRoot->Lnode->Rnode;
		}

		temp = CurrentRoot->Lnode;
		CurrentRoot->Lnode = CurrentRoot->Lnode->Lnode;
	}

	decreaseHeight(temp->NodeHeight, temp);
	delete temp;
}


template<class T>
void MyBinaryTree<T>::removeTree()
{
	if (MainRoot == nullptr) return;

	remove(MainRoot);

	delete MainRoot;
	MainRoot = CurrentRoot = nullptr;
}


template<class T>
void MyBinaryTree<T>::moveUp()
{
	if (CurrentRoot->Root == nullptr) return;

	CurrentRoot = CurrentRoot->Root;
}


template<class T>
void MyBinaryTree<T>::moveDown(const bool &toRight)
{
	if (toRight && CurrentRoot->Rnode == nullptr) return;
	if (!toRight && CurrentRoot->Lnode == nullptr) return;

	if (toRight) CurrentRoot = CurrentRoot->Rnode;
	else CurrentRoot = CurrentRoot->Lnode;
}


template<class T>
void MyBinaryTree<T>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<T> *path = new MyStack<T>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (!path->isEmpty())
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}


template<class T>
void MyBinaryTree<T>::showNodeInfo() const
{
	if (MainRoot == nullptr) return;

	std::cout << "NodeLevel(dl. sciezki do wezla): " << CurrentRoot->NodeLevel <<
		"\nNodeDegree(ilosc polaczen): " << CurrentRoot->NodeDegree <<
		"\nNodeHeight(dl. sciezki od wezla): " << CurrentRoot->NodeHeight << std::endl;

	showPath();
	std::cout << std::endl;
}


template<class T>
void MyBinaryTree<T>::showTree() 
{
	show(MainRoot);
}


template<class T>
bool MyBinaryTree<T>::isEmpty() const
{
	if (MainRoot == nullptr) return true;
	return false;
}

template<class T>
int MyBinaryTree<T>::getTreeHeight() const
{
	return TreeHeight;
}

template<class T>
bool MyBinaryTree<T>::isLastOnLevel() const
{
	if (CurrentRoot->Rnode == nullptr) return true;
	return false;
}

template<class T>
void MyBinaryTree<T>::preOrder()
{
	std::cout << "PreOrder Tree" << std::endl;
	showTree();
}

template<class T>
void MyBinaryTree<T>::inOrder()
{
	std::cout << "InOrder Tree" << std::endl;
	showInOrder(MainRoot);
}

template<class T>
void MyBinaryTree<T>::postOrder()
{
	std::cout << "PostOrder Tree" << std::endl;
	showPostOrder(MainRoot);
}