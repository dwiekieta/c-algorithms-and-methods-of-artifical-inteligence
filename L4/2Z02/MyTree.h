#pragma once

#include "MyBinaryTree.h"

template<class T>
class MyTree
{
	MyBinaryTree<T> *TreeRoot;

public:
	MyTree();
	~MyTree();

	void addOnLevel(const T&);
	void addNewLevel(const T&);
	void addElement(const T&);

	void moveUp();
	void moveOnLevel();
	void moveDown();

	void removeElement();
	void removeTree();

	void showTree() const;
	void showPath() const;
	void showNodeInfo() const;

	void preOrder() const;
	void postOrder() const;
};

template<class T>
MyTree<T>::MyTree()
{
	TreeRoot = new MyBinaryTree<T>;
}

template<class T>
MyTree<T>::~MyTree()
{
	removeTree();

	delete TreeRoot;
}

template<class T>
void MyTree<T>::addOnLevel(const T &val)
{
	if (TreeRoot->getTreeHeight() < 1) return;
	std::cout << TreeRoot->getTreeHeight() << std::endl;

	TreeRoot->addElement(true, val);
}

template<class T>
void MyTree<T>::addNewLevel(const T &val)
{
	TreeRoot->addElement(false, val);
}

template<class T>
void MyTree<T>::addElement(const T &val)
{
	TreeRoot->addElement(val);
}

template<class T>
void MyTree<T>::removeElement()
{
	if (TreeRoot->isEmpty()) return;

	TreeRoot->earseElement(true);
	return;
}


template<class T>
void MyTree<T>::removeTree()
{
	TreeRoot->removeTree();
}

template<class T>
void MyTree<T>::showTree() const
{
	TreeRoot->showTree();
}

template<class T>
void MyTree<T>::showPath() const
{
	TreeRoot->showPath();
}

template<class T>
void MyTree<T>::showNodeInfo() const
{
	TreeRoot->showNodeInfo();
}

template<class T>
void MyTree<T>::moveUp()
{
	TreeRoot->moveUp();
}

template<class T>
void MyTree<T>::moveOnLevel()
{
	if (TreeRoot->isLastOnLevel())
	{
		TreeRoot->moveUp();
		TreeRoot->moveDown(false);
	}

	else TreeRoot->moveDown(true);
}

template<class T>
void MyTree<T>::moveDown()
{
	TreeRoot->moveDown(false);
}

template<class T>
void MyTree<T>::preOrder() const
{
	TreeRoot->preOrder();
}

template<class T>
void MyTree<T>::postOrder() const
{
	TreeRoot->postOrder();
}