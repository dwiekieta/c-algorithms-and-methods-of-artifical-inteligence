#include <iostream>
#include <string>

#include "MyTree.h"

int main()
{
	MyTree<int> tree;

	for (int i = 0; i < 100; ++i) tree.addElement(i);

	//tree.showNodeInfo();
	tree.showTree();

	//tree.moveDown();
	
	//tree.showNodeInfo();

	//tree.removeElement();
	//std::cout << "\n\n\n\n\n\n";

	//tree.showTree();

	tree.preOrder();
	std::cout << "\n\n\n\n\n\n";
	tree.postOrder();

	while (1);
	return 0;
}