#include <iostream>
#include <ctime>

#include"MyProQueue.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyProQueue<int> kolejka;

	timeStart = clock();

	for (int i = 0; i < 10; ++i)
		kolejka.addElement(10, i);

	for (int i = 0; i < 10; ++i)
		kolejka.addElement(1, i+20);

	timeStop = clock();

	kolejka.showQueue();
	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (true);

	return 0;
}