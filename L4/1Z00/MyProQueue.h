#pragma once

template<class T>
class MyProQueue
{
	struct MyQueueItem
	{
		T ItemValue;
		int Key;
		MyQueueItem *NextItem;
	};

	MyQueueItem *FirstItem;
	MyQueueItem *LastItem;


public:
	MyProQueue();
	~MyProQueue();
	
	void addElement(const int&, const T&);
	void removeElement(const int &k = 0);
	void removeQueue();

	void showQueue();

};


template<class T>
MyProQueue<T>::MyProQueue()
{
	FirstItem = LastItem = nullptr;
}


template<class T>
MyProQueue<T>::~MyProQueue()
{
	removeQueue();
}

template<class T>
void MyProQueue<T>::addElement(const int &k, const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyQueueItem;

		FirstItem->ItemValue = val;
		FirstItem->Key = k;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	MyQueueItem *temp = new MyQueueItem;

	temp->ItemValue = val;
	temp->Key = k;


	/* Sprawdzene kolejnosci - na poczatku el. o najwyzszym priorytecie
	*		jezeli ten sam to dodawnanie na koniec
	*/

	if (LastItem->Key == k)
	{
		temp->NextItem = LastItem->NextItem;
		LastItem->NextItem = temp;
		LastItem = temp;

		return;
	}

	if (LastItem->Key > k)
	{
		if (LastItem->NextItem == nullptr)
		{
			LastItem->NextItem = temp;
			LastItem = temp;
			LastItem->NextItem = nullptr;

			return;
		}
		else
		{
			while (LastItem->NextItem->Key >= k)
			{
				if (LastItem->NextItem->NextItem == nullptr)
				{
					LastItem->NextItem->NextItem = temp;
					LastItem = temp;
					LastItem->NextItem = nullptr;

					return;
				}
				else
					LastItem = LastItem->NextItem;
			}

			temp->NextItem = LastItem->NextItem;
			LastItem->NextItem = temp;
			LastItem = temp;

			return;
		}
	}


	if (LastItem->Key < k)
	{
		LastItem = FirstItem;
		
		if
	}






	/*LastItem = FirstItem;
	if (FirstItem->NextItem != nullptr)		//sprawdzanie hierarchi 
	{
		while ((LastItem->NextItem->Key + 1) > k)
		{
			if (LastItem->NextItem->NextItem == nullptr) break;
			LastItem = LastItem->NextItem;
		}
	}
	else if ((FirstItem->Key + 1) > k)
	{
		temp->NextItem = nullptr;
		FirstItem->NextItem = temp;

		return;
	}


	
	if (LastItem == FirstItem)
	{
		temp->NextItem = LastItem;
		FirstItem = LastItem = temp;
	}
	else
	{
		temp->NextItem = LastItem->NextItem;
		LastItem->NextItem = temp;
		LastItem = temp;
	}
	*/
}


template<class T>
void MyProQueue<T>::removeElement(const int &k)
{
	if (FirstItem == nullptr) return;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return;
	}

	MyQueueItem *curr = FirstItem;
	MyQueueItem *temp;

	while (curr->NextItem->Key > k)
	{
		if (curr->NextItem->NextItem == nullptr) break;

		curr = curr->NextItem;		
	}

	if (curr->NextItem->Key != k) return;

	temp = curr->NextItem;
	curr->NextItem = curr->NextItem->NextItem;

	delete temp;
}


template<class T>
void MyProQueue<T>::removeQueue()
{
	while (FirstItem != nullptr)
		removeElement();
}


template<class T>
void MyProQueue<T>::showQueue()
{
	std::cout << "Kolejka: " << std::endl;

	int el = 0;
	MyQueueItem *temp = FirstItem;

	do
	{
		std::cout << "[" << el << "] (" << temp->Key << ") " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}