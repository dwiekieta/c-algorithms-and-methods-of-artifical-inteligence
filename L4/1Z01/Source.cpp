#include <iostream>

#include "MyProQueueNode.h"



int main()
{
	MyProQueueRoot<int> kolejka;

	for (int i = 0; i < 10; ++i) kolejka.addElement(i);

	kolejka.showRoot();

	for (int i = 0; i < 1; ++i) kolejka.removeElement();

	kolejka.showRoot();

	std::cout << kolejka[4] << std::endl;
	std::cout << kolejka.getSize() << std::endl;

	kolejka.removeRoot();
	while (1);
	return 0;
}