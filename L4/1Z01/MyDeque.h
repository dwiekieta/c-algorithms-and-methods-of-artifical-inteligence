#pragma once

template<class T>
class MyDeque
{
	struct MyDequeItem
	{
		T ItemValue;
		MyDequeItem *NextItem;
	};

	MyDequeItem *FirstItem;
	MyDequeItem *LastItem;

public:
	MyDeque();
	~MyDeque();

	void addFrontElement(const T&);
	void addEndElement(const T&);
	bool removeFrontElement();
	bool removeEndElement();
	void removeDeqeue();

	void showDeqeue();

	T getFirst();
	T getLast();
	
	bool isEmpty() const;
	int size() const;

	T& operator[](const int&) const;
	T& operator[](const int&);

};


template<class T>
MyDeque<T>::MyDeque()
{
	FirstItem = LastItem = nullptr;
}


template<class T>
MyDeque<T>::~MyDeque()
{
	removeDeqeue();
}


template<class T>
void MyDeque<T>::addFrontElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyDequeItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	MyDequeItem *temp = new MyDequeItem;
	temp->NextItem = FirstItem;

	FirstItem = temp;
	FirstItem->ItemValue = val;

}


template<class T>
void MyDeque<T>::addEndElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyDequeItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	LastItem->NextItem = new MyDequeItem;
	LastItem = LastItem->NextItem;

	LastItem->ItemValue = val;
	LastItem->NextItem = nullptr;
}


template<class T>
bool MyDeque<T>::removeFrontElement()
{
	if (FirstItem == nullptr) return false;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return false;
	}

	MyDequeItem *temp = FirstItem;
	FirstItem = FirstItem->NextItem;

	delete temp;
	return true;
}


template<class T>
bool MyDeque<T>::removeEndElement()
{
	if (LastItem == nullptr) return false;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return false;
	}

	MyDequeItem *temp = FirstItem;
	while (temp->NextItem != LastItem)
		temp = temp->NextItem;
	
	delete LastItem;

	LastItem = temp;
	return true;
}



template<class T>
void MyDeque<T>::removeDeqeue()
{
	while (removeFrontElement());
		
}


template<class T>
void MyDeque<T>::showDeqeue()
{
	std::cout << "Kolejka z dwoma koncami: " << std::endl;

	int el = 0;
	MyDequeItem *temp = FirstItem;

	do
	{
		std::cout << "[ " << el << " ] " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}


template<class T>
T MyDeque<T>::getFirst()
{
	if (FirstItem == nullptr) throw "Brak el.";
	return FirstItem->ItemValue;
}


template<class T>
T MyDeque<T>::getLast()
{
	if (FirstItem == nullptr) throw "Brak el.";
	return LastItem->ItemValue;
}


template<class T>
bool MyDeque<T>::isEmpty() const
{
	if (FirstItem == nullptr) return true;
	return false;
}


template<class T>
int MyDeque<T>::size() const
{
	if (isEmpty()) return 0;

	MyDequeItem *temp = FirstItem;
	int size = 1;

	for (size; temp != LastItem; ++size) temp = temp->NextItem;
	return size;
}


template<class T>
T& MyDeque<T>::operator[](const int& o) const
{
	if (FirstItem == nullptr) throw "Brak el.";
	
	MyDequeItem * temp = FirstItem;
	for (int i = 0; i < o; ++i)
	{
		temp = temp->NextItem;
		if (temp == nullptr) throw "Brak el.";
	}

	return temp->ItemValue;
}

template<class T>
T& MyDeque<T>::operator[](const int &o)
{
	if (FirstItem == nullptr) throw "Brak el.";

	MyDequeItem * temp = FirstItem;
	for (int i = 0; i < o; ++i)
	{
		temp = temp->NextItem;
		if (temp == nullptr) throw "Brak el.";
	}

	return temp->ItemValue;
}