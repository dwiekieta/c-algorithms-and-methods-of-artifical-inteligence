#pragma once

#include "MyHeap.h"

template<class T>
class MyBinaryTree
{
	struct Node
	{
		T Value;
		Node *Root;
		Node *Lnode;
		Node *Rnode;

		int NodeLevel;		//poziom wezla = dl. sciezki
		int NodeDegree;		//liczba krawedzi z i do wezla
		int NodeHeight;		//najdluzsza scierzka od wezla do liscia
	};

	Node *MainRoot;
	Node *CurrentRoot;

	int TreeHeight;			//najdluzsza sciezka od korzenia do liscia

	void increaseHeight(const int &h, Node *n);	//zwieksza (o ile istnieje taka potrzeba) wartosc wysokosci wezla/drzewa

public:
	MyBinaryTree();
	~MyBinaryTree();

	void addElement(const bool &toRight, const T &val);

	void showPath() const;
	void showNodeInfo() const;
	//void showTree() const;

};

template<class T>
MyBinaryTree<T>::MyBinaryTree()
{
	MainRoot = CurrentRoot = nullptr;
	TreeHeight = 0;
}

template<class T>
MyBinaryTree<T>::~MyBinaryTree()
{
}

template<class T>
void MyBinaryTree<T>::addElement(const bool &toRight, const T &val)
{
	if (CurrentRoot == nullptr)
	{
		MainRoot = CurrentRoot = new Node;
		MainRoot->Value = val;
		MainRoot->Root = nullptr;
		MainRoot->Lnode = nullptr;
		MainRoot->Rnode = nullptr;

		MainRoot->NodeLevel = 0;
		MainRoot->NodeDegree = 0;
		MainRoot->NodeHeight = 0;

		TreeHeight = 0;

		showPath();
		return;
	}

	if (toRight && CurrentRoot->Rnode != nullptr) return;
	if (!toRight && CurrentRoot->Lnode != nullptr) return;

	Node *temp = new Node;

	temp->Value = val;
	temp->Root = CurrentRoot;
	temp->Lnode = nullptr;
	temp->Rnode = nullptr;

	temp->NodeLevel = CurrentRoot->NodeLevel + 1;
	temp->NodeDegree = 1;
	CurrentRoot->NodeDegree += 1;
	temp->NodeHeight = 0;

	if (toRight) CurrentRoot->Rnode = temp;
	else CurrentRoot->Lnode = temp;

	CurrentRoot = temp;

	increaseHeight(1, CurrentRoot);

	showPath();
}

template<class T>
void MyBinaryTree<T>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<T> *path = new MyStack<T>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != NULL)
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}

void MyBinaryTree<std::string>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<std::string> *path = new MyStack<std::string>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != "")
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}


template<class T>
void MyBinaryTree<T>::showNodeInfo() const
{
	std::cout << "NodeLevel(dl. scierzki do wezla): " << CurrentRoot->NodeLevel <<
		"\nNodeDegree(ilosc polaczen): " << CurrentRoot->NodeDegree <<
		"\nNodeHeight(dl. scierzki od wezla): " << CurrentRoot->NodeHeight << std::endl;

	showPath();
}


template<class T>
void MyBinaryTree<T>::increaseHeight(const int &h, Node *n)
{
	if (n->Root == nullptr)
	{
		if (n->NodeHeight > TreeHeight)
			TreeHeight = n->NodeHeight;

		return;
	}

	n = n->Root;

	if (n->NodeHeight < h)
	{
		n->NodeHeight = h;
		increaseHeight(h + 1, n);
	}
}