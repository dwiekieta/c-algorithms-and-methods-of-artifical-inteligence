#pragma once

template <class T>
class MyStack
{
	struct MyStackItem {
		T ItemValue;
		MyStackItem* PreviousItem;
	};

	MyHeapItem *LastItem;

public:
	MyStack();
	~MyStack();

	void addElement(const T&);
	T getElement() const;
	void removeElement();
	void removeHeap();

	void showHeap() const;
};


template<class T>
MyStack<T>::MyStack()
{
	LastItem = nullptr;
}


template<class T>
MyStack<T>::~MyStack()
{
	removeHeap();
}


template<class T>
void MyStack<T>::addElement(const T& val)
{
	if (LastItem == nullptr)
	{
		LastItem = new MyHeapItem;
		LastItem->PreviousItem = nullptr;
	}
	else 
	{
		MyHeapItem *temp = new MyHeapItem;
		temp->PreviousItem = LastItem;
		LastItem = temp;
	}

	LastItem->ItemValue = val;
}


template<class T>
T MyStack<T>::getElement() const
{
	if (LastItem == nullptr) return NULL;

	return LastItem->ItemValue;
}

std::string MyStack<std::string>::getElement() const
{
	if (LastItem == nullptr) return std::string("");

	return LastItem->ItemValue;
}


template<class T>
void MyStack<T>::removeElement()
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;

	LastItem = LastItem->PreviousItem;
	delete temp;
}


template<class T>
void MyStack<T>::removeHeap()
{
	while (LastItem != nullptr)
		removeElement();
}


template<class T>
void MyStack<T>::showHeap() const
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;
	int elements = 0;

	for (; temp != nullptr; elements++)
		temp = temp->PreviousItem;

	temp = LastItem;
	for (; temp != nullptr; elements--)
	{
		std::cout << "[ " << elements << " ] " << temp->ItemValue << std::endl;
		temp = temp->PreviousItem;
	}
}
