#include <iostream>
#include <string>

#include "MyBinaryTree.h"

int main()
{
	MyBinaryTree<int> tree;

	for (int i = 0; i < 20; ++i) tree.addElement(i);

	
	tree.inOrder();
	std::cout << "\n\n\n\n\n\n";
	tree.preOrder();
	std::cout << "\n\n\n\n\n\n";
	tree.postOrder();
	while (1);
	return 0;
}