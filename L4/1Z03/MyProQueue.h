#pragma once

#include "MyProQueueRoot.h"

template<class T>
class MyProQueue
{
	MyDeque<MyProQueueRoot<T>*> *QueueRoot;

	int MaxPriority;
	int MinPriority;

	int searchRoot(const int&);

public:
	MyProQueue();
	~MyProQueue();

	void addElement(const int &p, const int &val);

	bool removeElement(const bool &fromMaximum = true);
	void removeQueue();

	void showQueue(const bool &fromMaximum = true);
};

template<class T>
MyProQueue<T>::MyProQueue()
{
	MaxPriority = 0;
	MinPriority = -1;

	QueueRoot = new MyDeque<MyProQueueRoot<T>*>;
}

template<class T>
MyProQueue<T>::~MyProQueue()
{
	removeQueue();
	delete QueueRoot;
}

template<class T>
int MyProQueue<T>::searchRoot(const int &r)
{
	if (QueueRoot->isEmpty()) return -1;

	int i = 0;
	for (; i < QueueRoot->size(); ++i) 
		if ((*QueueRoot)[i] != nullptr)
			if((*QueueRoot)[i]->getPriority() == r) return i;

		return -1;
}

template<class T>
void MyProQueue<T>::addElement(const int &p, const int &val)
{
	int root = searchRoot(p);

	if (p > MaxPriority) MaxPriority = p;
	if (MinPriority == -1) MinPriority = MaxPriority;
	if (p < MinPriority) MinPriority = p;

	if (QueueRoot->isEmpty() || root == -1)
	{
		QueueRoot->addFrontElement(new MyProQueueRoot<T>(p));
		(*QueueRoot)[0]->addElement(val);

		return;
	}
	
	(*QueueRoot)[root]->addElement(val);
}


template<class T>
bool  MyProQueue<T>::removeElement(const bool &fromMaximum)
{
	if (QueueRoot->isEmpty()) return false;

	int root;

	if (fromMaximum)
		for (int i = MaxPriority; i >= MinPriority; --i)
		{
			root = searchRoot(i);
			if (root != -1 && !(*QueueRoot)[root]->isEmpty())
			{
				(*QueueRoot)[root]->removeElement();
				
				return true;
			}
		}
	else
		for (int i = MinPriority; i <= MaxPriority; ++i)
		{
			root = searchRoot(i);
			if (root != -1 && !(*QueueRoot)[root]->isEmpty())
			{
				(*QueueRoot)[root]->removeElement();
				return true;
			}
		}

	return false;
}


template<class T>
void MyProQueue<T>::removeQueue()
{
	while (removeElement());
}



template<class T>
void  MyProQueue<T>::showQueue(const bool &fromMaximum)
{
	if (QueueRoot->isEmpty()) return;
	
	int root;

	if(fromMaximum)
		for (int i = MaxPriority; i >= MinPriority; --i)
		{
			root = searchRoot(i);
			if(root != -1)
			(*QueueRoot)[root]->showRoot();
		}
	else
		for (int i = MinPriority; i <= MaxPriority; ++i)
		{
			root = searchRoot(i);
			if (root != -1)
				(*QueueRoot)[root]->showRoot();
		}
}