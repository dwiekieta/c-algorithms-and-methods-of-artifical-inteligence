#pragma once

#include "MyDeque.h"

template<class T>
class MyProQueueRoot
{
	int Priority;

	MyDeque<T> *Elements;

public:
	MyProQueueRoot(const int &p = 0);
	~MyProQueueRoot();

	void addElement(const T&);
	T& getElement() const;

	int getSize() const;
	int getPriority() const;

	void removeElement();
	void removeRoot();

	void showRoot() const;

	bool isEmpty() const;

	T& operator[](const int&) const;
	T& operator[](const int&);
};

template<class T>
MyProQueueRoot<T>::MyProQueueRoot(const int &p)
{
	Priority = p;
	Elements = new MyDeque<T>;
}

template<class T>
MyProQueueRoot<T>::~MyProQueueRoot()
{
	removeRoot();

	delete Elements;
}

template<class T>
void MyProQueueRoot<T>::addElement(const T &val)
{
	Elements->addElement(val);
}

template<class T>
T& MyProQueueRoot<T>::getElement() const
{
	return Elements->getFirst();
}

template<class T>
int MyProQueueRoot<T>::getSize() const
{
	return Elements->size();
}

template<class T>
int MyProQueueRoot<T>::getPriority() const
{
	return Priority;
}

template<class T>
void MyProQueueRoot<T>::removeElement()
{
	Elements->removeEndElement();
}

template<class T>
void MyProQueueRoot<T>::removeRoot()
{
	Elements->removeDeqeue();
}

template<class T>
void MyProQueueRoot<T>::showRoot() const
{
	for (int i = 0; i < Elements->size(); ++i) std::cout << "(" << Priority << ") " << (*Elements)[i] << std::endl;
}

template<class T>
bool MyProQueueRoot<T>::isEmpty() const
{
	return Elements->isEmpty();
}

template<class T>
T& MyProQueueRoot<T>::operator[](const int &o) const
{
	return (*Elements)[o];
}

template<class T>
T& MyProQueueRoot<T>::operator[](const int &o)
{
	return (*Elements)[o];
}
