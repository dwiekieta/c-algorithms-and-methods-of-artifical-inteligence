#pragma once

#include "MyHeap.h"

template<class T>
class MyBinaryTree
{
	struct Node
	{
		T Value;
		Node *Root;
		Node *Lnode;
		Node *Rnode;
	};

	Node *MainRoot;
	Node *CurrentRoot;

public:
	MyBinaryTree();
	~MyBinaryTree();

	void addElement(const bool &toRight, const T &val);

	void showPath() const;
	//void showTree() const;

};

template<class T>
MyBinaryTree<T>::MyBinaryTree()
{
	MainRoot = CurrentRoot = nullptr;
}

template<class T>
MyBinaryTree<T>::~MyBinaryTree()
{
}

template<class T>
void MyBinaryTree<T>::addElement(const bool &toRight, const T &val)
{
	if (CurrentRoot == nullptr)
	{
		MainRoot = CurrentRoot = new Node;
		MainRoot->Value = val;
		MainRoot->Root = nullptr;
		MainRoot->Lnode = nullptr;
		MainRoot->Rnode = nullptr;

		showPath();
		return;
	}

	Node *temp = new Node;

	temp->Value = val;
	temp->Root = CurrentRoot;
	temp->Lnode = nullptr;
	temp->Rnode = nullptr;

	if (toRight) CurrentRoot->Rnode = temp;
	else CurrentRoot->Lnode = temp;

	CurrentRoot = temp;

	showPath();
}

template<class T>
void MyBinaryTree<T>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<T> *path = new MyStack<T>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != NULL)
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}

void MyBinaryTree<std::string>::showPath() const
{
	if (CurrentRoot == nullptr) return;

	Node *temp = CurrentRoot;
	MyStack<std::string> *path = new MyStack<std::string>;

	do
	{
		path->addElement(temp->Value);
		temp = temp->Root;
	} while (temp != nullptr);

	std::cout << "Sciezka: ";

	while (path->getElement() != "")
	{
		std::cout << path->getElement() << "/";
		path->removeElement();
	}

	std::cout << std::endl;

	delete path;
}