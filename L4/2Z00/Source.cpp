#include <iostream>
#include <string>

#include "MyTree.h"

int main()
{
	MyTree<int> tree;

	for (int i = 0; i < 100; ++i)
	{
		if(rand()%2) tree.addOnLevel(i);
		else tree.addNewLevel(i);

		if (rand() % 2 && i > 3) tree.moveOnLevel();
	}


	for (int i = 0; i<10; ++i)  tree.removeElement();
	tree.removeTree();
	tree.showNodeInfo();
	tree.showTree();

	while (1);
	return 0;
}