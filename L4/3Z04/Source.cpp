#include <iostream>
#include <string>

#include "MyBinaryTree.h"

int main()
{
	MyBinaryTree<int> tree;

	for (int i = 0; i < 60; ++i) tree.addElement(rand()%2, i);
	for (int i = 0; i < 50; ++i)
	{
		tree.addElement(rand() % 2, i);
		tree.moveUp();
		tree.moveUp();
	}

	//tree.showNodeInfo();
	
	tree.removeTree();
	tree.showTree();

	for (int i = 0; i < 60; ++i) tree.addElement(rand() % 2, i+90);
	for (int i = 0; i < 50; ++i)
	{
		tree.addElement(rand() % 2, i+1000);
		tree.moveUp();
		tree.moveUp();
	}
	tree.showTree();

	while (1);
	return 0;
}