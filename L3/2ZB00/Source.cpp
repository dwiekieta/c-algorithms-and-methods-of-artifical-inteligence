#include <iostream>

#include "MyQeueArray.h"


int main()
{
	MyQeueArray<int> queue;

	for (int i = 0; i < 5000; ++i)
		queue.addElement(i);

	queue.showQueue();

	for (int i = 0; i < 23; ++i)
		queue.removeElement();

	queue.showQueue();

	while (1);
	return 0;
}