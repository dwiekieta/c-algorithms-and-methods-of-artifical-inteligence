#include<iostream>

#include"Hanoi.h"

MyHanoi::MyHanoi()
{
	Heap1 = new MyHeap<int>;
	Heap2 = new MyHeap<int>;
	Heap3 = new MyHeap<int>;

	ShowStep = false;
}

MyHanoi::~MyHanoi()
{
	delete Heap1;
	delete Heap2;
	delete Heap3;
}

void MyHanoi::setHanoi(const int &lvl)
{
	Size = lvl;
	for (int i = lvl; i; i--)
		Heap1->addElement(i);
}

void MyHanoi::startHanoi(const bool &show)
{
	ShowStep = show;
	move(Size -1, Heap1, Heap2);
}


MyHeap<int>* MyHanoi::minimum(MyHeap<int>* x)
{
	if (x == Heap1)
	{
		std::cout << "minimum(Heap1): ";
		if (*Heap2 < *Heap3)
		{
			std::cout << "Heap2\n";
			return Heap2;
		}
		else
		{
			std::cout << "Heap3\n";
			return Heap3;
		}
	}
	else if (x == Heap2)
	{
		std::cout << "minimum(Heap2): ";
		if (*Heap1 < *Heap3)
		{
			std::cout << "Heap1\n";
			return Heap1;
		}
		else
		{
			std::cout << "Heap3\n";
			return Heap3;
		}
	}
	else
	{
		std::cout << "minimum(Heap3): ";
		if (*Heap1 < *Heap2)
		{
			std::cout << "Heap1\n";
			return Heap1;
		}
		else
		{
			std::cout << "Heap2\n";
			return Heap2;
		}
	}
}


MyHeap<int>* MyHanoi::maximum(MyHeap<int> *x)
{
	if (x == Heap1)
	{
		std::cout << "maximum(Heap1): ";
		if ((*Heap2 > *Heap3) && (Heap3->getValue() - x->getValue()) != 1 || 
			(Heap2->getValue() - x->getValue()) == 1)
		{
			std::cout << "Heap2\n";
			return Heap2;
		}
		else
		{
			std::cout << "Heap3\n";
			return Heap3;
		}
	}
	else if (x == Heap2)
	{
		std::cout << "maximum(Heap2): ";
		if ((*Heap1 > *Heap3) && (Heap3->getValue() - x->getValue()) != 1 ||
			(Heap1->getValue() - x->getValue()) == 1)
		{
			std::cout << "Heap1\n";
			return Heap1;
		}
		else
		{
			std::cout << "Heap3\n";
			return Heap3;
		}
	}
	else
	{
		std::cout << "maximum(Heap3): ";
		if ((*Heap1 > *Heap2) && (Heap2->getValue() - x->getValue()) != 1 ||
			(Heap1->getValue() - x->getValue()) == 1)
		{
			std::cout << "Heap1\n";
			return Heap1;
		}
		else
		{
			std::cout << "Heap2\n";
			return Heap2;
		}
	}
}

void MyHanoi::move(MyHeap<int> *from, MyHeap<int> *to)
{
	MyHeap<int> *min, *max;
	  
	while (*from > *to)
	{
		max = maximum(from);

		move(from, max);
		
	}

	to->addElement(from->moveElement());
	if (ShowStep) showStep();	

	int i;
	std::cin >> i;

}


void MyHanoi::move(int ile, MyHeap<int> *from, MyHeap<int> *to)
{
	if (ile == 0) return;

	for (int i = ile; i; --i)
		move(from, to);
	
	move(from, Heap3);

	if (to == Heap2) move(ile - 1, to, Heap1);
	else move(ile - 1, to, Heap2);
}


void MyHanoi::showStep()
{
	for (int i = Size; i; --i)
	{
		std::cout << "[ " << i << " ]\t" << 
			Heap1->getHeapLevel(i) << '\t' <<
			Heap2->getHeapLevel(i) << '\t' <<
			Heap3->getHeapLevel(i) << std::endl;
	}

	std::cout << std::endl;
}