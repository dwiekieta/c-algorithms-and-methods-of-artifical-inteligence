#pragma once

#include "MyHeap.h"

class MyHanoi
{
	MyHeap<int> *Heap1;
	MyHeap<int> *Heap2;
	MyHeap<int> *Heap3;

	MyHeap<int>* minimum(MyHeap<int>*);
	MyHeap<int>* maximum(MyHeap<int>*);
	void move(MyHeap<int>*from, MyHeap<int>*to);
	void move(int ile, MyHeap<int>*from, MyHeap<int>*to);
	void showStep();

	bool ShowStep;
	int Size;

public:
	MyHanoi();
	~MyHanoi();

	void setHanoi(const int&);
	void startHanoi(const bool&);
};

