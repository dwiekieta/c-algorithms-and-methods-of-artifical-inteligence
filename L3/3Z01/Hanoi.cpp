#include<iostream>

#include"Hanoi.h"

MyHanoi::MyHanoi()
{
	Heap1 = new MyHeap<int>;
	Heap2 = new MyHeap<int>;
	Heap3 = new MyHeap<int>;

	ShowStep = false;
}

MyHanoi::~MyHanoi()
{
	delete Heap1;
	delete Heap2;
	delete Heap3;
}

void MyHanoi::setHanoi(const int &lvl)
{
	Size = lvl;
	for (int i = lvl; i; i--)
		Heap1->addElement(i);
}

void MyHanoi::startHanoi(const bool &show)
{
	ShowStep = show;
	move();
}

void MyHanoi::move()
{
	 move(Heap1, Heap2);
}

MyHeap<int>* MyHanoi::minimum(MyHeap<int>* x, MyHeap<int>* y, MyHeap<int>* z)
{
	if(x->isEmpty())

	if (x->getValue() < y->getValue() && y->getValue() < z->getValue()) return x;
	if (x->getValue() < z->getValue() && z->getValue() < y->getValue()) return x;

	if (y->getValue() < x->getValue() && x->getValue() < z->getValue()) return y;
	if (y->getValue() < z->getValue() && z->getValue() < x->getValue()) return x;

	return z;
}

MyHeap<int>* MyHanoi::minimum(MyHeap<int>* x, MyHeap<int>* y)
{
	if (y->isEmpty()) return x;
	if (x->isEmpty()) return y;
	if (x->getValue() < y->getValue()) return x;
	return y;
}

MyHeap<int>* MyHanoi::maximum(MyHeap<int>* r, MyHeap<int>* x, MyHeap<int>* y)
{
	if (x->isEmpty() && y->isEmpty()) return x;
	if (x->isEmpty() && (y->getValue() - r->getValue()) != 1) return x;
	else return y;
	if (y->isEmpty() && (x->getValue() - r->getValue()) != 1) return y;
	else return x;
	if (r->getValue() < x->getValue() && r->getValue() < y->getValue())
	{
		if ((x->getValue() - r->getValue()) == 1) return x;
		if ((y->getValue() - r->getValue()) == 1) return y;
		if ((x->getValue() - r->getValue()) > (y->getValue() - r->getValue())) return x;
		return y;
	} 

	if (r->getValue() < x->getValue()) return x;
	return y;
}

void MyHanoi::move(MyHeap<int>*from, MyHeap<int>*to)
{
	if(!Heap3->isEmpty())
		if (Heap3->getElements() == Size && Heap3->getValue() == 1) return;

	to->addElement(from->moveElement());

	if (ShowStep) showStep();

	MyHeap<int> *min;

	if (to == Heap1) min = minimum(Heap2, Heap3);
	else if (to == Heap2) min = minimum(Heap1, Heap3);
	else min = minimum(Heap2, Heap1);

	if (min == Heap1) move(min, maximum(min, Heap2, Heap3));
	else if (min == Heap2) move(min, maximum(min, Heap1, Heap3));
	else move(min, maximum(min, Heap2, Heap1));
}


void MyHanoi::showStep()
{
	std::cout << "Sterta 1:\n"; 
	if (!Heap1->isEmpty()) Heap1->showHeap();
	std::cout << "Sterta 2:\n";
	if (!Heap2->isEmpty()) Heap2->showHeap();
	std::cout << "Sterta 3:\n";
	if (!Heap3->isEmpty()) Heap3->showHeap();
	std::cout << "\n\n\n\n";

}