#pragma once

template<typename T>
struct MyHeapItem {
	T ItemValue;
	MyHeapItem* PreviousItem;
};

template <class T>
class MyHeap
{
	MyHeapItem<T> *LastItem;
	int HeapElements;

public:
	MyHeap();
	~MyHeap();

	void addElement(const T&);
	void addElement(MyHeapItem<T>*);
	MyHeapItem<T>* moveElement();
	void removeElement();
	void removeHeap();

	void showHeap() const;

	bool isEmpty() const;
	
	T getValue() const;
	int getElements() const;
};


template<class T>
MyHeap<T>::MyHeap()
{
	LastItem = nullptr;
	HeapElements = 0;
}


template<class T>
MyHeap<T>::~MyHeap()
{
	removeHeap();
}


template<class T>
void MyHeap<T>::addElement(const T& val)
{
	if (LastItem == nullptr)
	{
		LastItem = new MyHeapItem<T>;
		LastItem->PreviousItem = nullptr;
	}
	else 
	{
		MyHeapItem<T> *temp = new MyHeapItem<T>;
		temp->PreviousItem = LastItem;
		LastItem = temp;
	}

	LastItem->ItemValue = val;
	HeapElements++;
}


template<class T>
void MyHeap<T>::addElement(MyHeapItem<T> *item)
{
	if (LastItem == nullptr)
	{
		LastItem = item;
		LastItem->PreviousItem = nullptr;
	}
	else
	{
		item->PreviousItem = LastItem;
		LastItem = item;
	}

	HeapElements++;
}

template<class T>
MyHeapItem<T>* MyHeap<T>::moveElement()
{
	if (LastItem == nullptr) return nullptr;

	HeapElements--;

	MyHeapItem<T> *temp = LastItem;
	LastItem = LastItem->PreviousItem;

	return temp;
}

template<class T>
void MyHeap<T>::removeElement()
{
	if (LastItem == nullptr) return;

	MyHeapItem<T> *temp = LastItem;

	LastItem = LastItem->PreviousItem;
	delete temp;
	
	HeapElements--;
}


template<class T>
void MyHeap<T>::removeHeap()
{
	while (LastItem != nullptr)
		removeElement();
}


template<class T>
void MyHeap<T>::showHeap() const
{
	if (LastItem == nullptr) return;

	MyHeapItem<T> *temp = LastItem;
	int elements = 0;

	for (; temp != nullptr; elements++)
		temp = temp->PreviousItem;

	temp = LastItem;
	for (; temp != nullptr; elements--)
	{
		std::cout << "[ " << elements << " ] " << temp->ItemValue << std::endl;
		temp = temp->PreviousItem;
	}
}


template<class T>
bool MyHeap<T>::isEmpty() const
{
	if (HeapElements == 0) return true;
	return false;
}

template<class T>
T MyHeap<T>::getValue() const
{
	if (LastItem == nullptr) return -1;
	return LastItem->ItemValue;
}

template<class T>
int MyHeap<T>::getElements() const
{
	return HeapElements;
}
