#include <iostream>

#include "Hanoi.h"


int main()
{
	MyHanoi hanoi;
	
	hanoi.setHanoi(10);
	hanoi.startHanoi(true);

	while (1);

	return 0;
}