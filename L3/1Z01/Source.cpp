#include <iostream>
#include <ctime>

#include "MyHeap.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyHeap<int,unsigned long> heap;
	double dokladnosc = 1;

	timeStart = clock();
	for (int i = 0; i < 500000; ++i) heap.addElement(i);
	timeStop = clock();
	
	//for (int j = 0; j < dokladnosc; j++) heap.sumHeap();
	

	//heap.showHeapSum();

	std::cout << "Czas dodawania: " << difftime(timeStop, timeStart)/dokladnosc << " [ms]\n";

	while (1);

	return 0;
}