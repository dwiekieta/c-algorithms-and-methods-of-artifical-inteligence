#pragma once

template <class T, class S>
class MyHeap
{
	struct MyHeapItem {
		T ItemValue;
		MyHeapItem* PreviousItem;
	};

	MyHeapItem *LastItem;
	S Sum;

public:
	MyHeap();
	~MyHeap();

	void addElement(const T&);
	void removeElement();
	void removeHeap();

	void showHeap() const;
	void showHeapSum() const;

	void sumHeap();
};


template<class T, class S>
MyHeap<T,S>::MyHeap()
{
	LastItem = nullptr;
}


template<class T, class S>
MyHeap<T,S>::~MyHeap()
{
	removeHeap();
}


template<class T, class S>
void MyHeap<T,S>::addElement(const T& val)
{
	if (LastItem == nullptr)
	{
		LastItem = new MyHeapItem;
		LastItem->PreviousItem = nullptr;
	}
	else 
	{
		MyHeapItem *temp = new MyHeapItem;
		temp->PreviousItem = LastItem;
		LastItem = temp;
	}

	LastItem->ItemValue = val;
}


template<class T, class S>
void MyHeap<T,S>::removeElement()
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;

	LastItem = LastItem->PreviousItem;
	delete temp;
}


template<class T, class S>
void MyHeap<T,S>::removeHeap()
{
	while (LastItem != nullptr)
		removeElement();
}


template<class T, class S>
void MyHeap<T,S>::showHeap() const
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;
	int elements = 0;

	for (; temp != nullptr; elements++)
		temp = temp->PreviousItem;

	temp = LastItem;
	for (; temp != nullptr; elements--)
	{
		std::cout << "[ " << elements << " ] " << temp->ItemValue << std::endl;
		temp = temp->PreviousItem;
	}

	std::cout << "Suma elementow w stosie: " << Sum << std::endl;
}


template<class T, class S>
void MyHeap<T, S>::showHeapSum() const
{
	if (LastItem == nullptr) return;

	std::cout << "Suma elementow w stosie: " << Sum << std::endl;
}


template<class T, class S>
void MyHeap<T, S>::sumHeap()
{
	Sum = 0;

	MyHeapItem *temp = LastItem;
	while (temp != nullptr)
	{
		Sum += temp->ItemValue;
		temp = temp->PreviousItem;
	}
}
