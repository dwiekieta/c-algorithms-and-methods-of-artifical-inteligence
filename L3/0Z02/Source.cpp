#include <iostream>
#include <conio.h>

using namespace std;

// Funkcja rozwi�zuj�ca rekurencyjnie zagadk� wie� hanoi.
void hanoi(int lk, char A, char B, char C)
{
	if (lk > 0)
	{
		hanoi(lk - 1, A, C, B); // Przek�ada lk-1 kr��k�w ze �r�d�owej wie�y (A) na pomocnicz� (B) przy u�yciu wie�y docelowej (C).
		cout << A << " --> " << C << endl; // Wy�wietla ruch w konsoli.
		cout << lk << endl;
		hanoi(lk - 1, B, A, C); // Przek�ada lk-1 kr��k�w z pomocniczej wie�y (B) na docelow� (C) przy u�yciu wie�y �r�d�owej (A).
	}
}

int main()
{
	int lk; // Liczba kr��k�w
	cin >> lk;
	cout << endl;

	hanoi(lk, 'A', 'B', 'C'); // Wywo�anie funkcji "hanoi".
	while (1);

	return 0;
}
