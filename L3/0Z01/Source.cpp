#include <iostream>

void replacement(int i)
{
	if (i)
	{
		std::cout << i << std::endl;
		replacement(i - 1);
		std::cout << i << std::endl;
	}
}


int main()
{
	replacement(5);

	while (1);
	return 0;
}