#pragma once

template <class T>
class MyHeap
{
	struct MyHeapItem {
		T ItemValue;
		MyHeapItem* PreviousItem;
	};

	MyHeapItem *LastItem;

public:
	MyHeap();
	~MyHeap();

	void addElement(const T&);
	void removeElement();
	void removeHeap();

	void showHeap() const;
};


template<class T>
MyHeap<T>::MyHeap()
{
	LastItem = nullptr;
}


template<class T>
MyHeap<T>::~MyHeap()
{
	removeHeap();
}


template<class T>
void MyHeap<T>::addElement(const T& val)
{
	if (LastItem == nullptr)
	{
		LastItem = new MyHeapItem;
		LastItem->PreviousItem = nullptr;
	}
	else 
	{
		MyHeapItem *temp = new MyHeapItem;
		temp->PreviousItem = LastItem;
		LastItem = temp;
	}

	LastItem->ItemValue = val;
}


template<class T>
void MyHeap<T>::removeElement()
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;

	LastItem = LastItem->PreviousItem;
	delete temp;
}


template<class T>
void MyHeap<T>::removeHeap()
{
	while (LastItem != nullptr)
		removeElement();
}


template<class T>
void MyHeap<T>::showHeap() const
{
	if (LastItem == nullptr) return;

	MyHeapItem *temp = LastItem;
	int elements = 0;

	for (; temp != nullptr; elements++)
		temp = temp->PreviousItem;

	temp = LastItem;
	for (; temp != nullptr; elements--)
	{
		std::cout << "[ " << elements << " ] " << temp->ItemValue << std::endl;
		temp = temp->PreviousItem;
	}
}
