#pragma once

template<class T, class S>
class MyHeapArray
{
	T* Array;
	T* ArrayEnd;
	T* ArrayElement;
	
	void create();
	void resize();

	S Sum;

public: 
	MyHeapArray();
	~MyHeapArray();

	void addElement(const T&);
	bool removeElement();
	void removeHeap();

	void showHeap() const;
	void showHeapSum() const;

	void sumHeap();
};

template<class T, class S>
MyHeapArray<T,S>::MyHeapArray()
{
	Array = ArrayEnd = ArrayElement = nullptr;
}


template<class T, class S>
MyHeapArray<T,S>::~MyHeapArray()
{
	removeHeap();
}


template<class T, class S>
void MyHeapArray<T,S>::create()
{
	if (Array != nullptr) removeHeap();

	Array = new T[8];
	ArrayEnd = Array + 8;
	ArrayElement = Array;
}


template<class T, class S>
void MyHeapArray<T,S>::resize()
{
	unsigned long size = ArrayEnd - Array;
	unsigned long fill = ArrayElement - Array;
	double fillRange = (double)fill / size;

	T* temp;
	T* lastTemp;
	if (fillRange > 0.9)
	{
		temp = new T[2 * size];
		lastTemp = temp + (2 * size);
	}
	else if (fillRange < 0.45 && size > 8)
	{
		temp = new T[size / 2];
		lastTemp = temp + (size / 2);
	}
	else return;

	for (unsigned long i = 0; i < fill; ++i)
		*(temp + i) = *(Array + i);

	delete[] Array;
	Array = temp;
	ArrayEnd = lastTemp;
	ArrayElement = Array + fill;
}


template<class T, class S>
void MyHeapArray<T,S>::addElement(const T& val)
{
	if (Array == nullptr)
		create();

	resize();
	*ArrayElement = val;
	ArrayElement = ArrayElement + 1;
}


template<class T, class S>
bool MyHeapArray<T,S>::removeElement()
{
	if (ArrayElement == Array) return false;

	ArrayElement--;
	resize();
	return true;
}


template<class T, class S>
void MyHeapArray<T,S>::removeHeap()
{
	while (removeElement());

	delete[] Array;
	Array = ArrayEnd = ArrayElement = nullptr;
}


template<class T, class S>
void MyHeapArray<T,S>::showHeap() const
{
	if (Array == nullptr) return;

	unsigned long size = ArrayElement - Array;

	for (; size; --size)
		std::cout << "[ " << size << " ] " << Array[size - 1] << std::endl;
}


template<class T, class S>
void MyHeapArray<T, S>::showHeapSum() const
{
	if (Array == nullptr) return;

	std::cout << "Suma elementow: " << Sum << std::endl;
}


template<class T, class S>
void MyHeapArray<T, S>::sumHeap()
{
	Sum = 0;

	for (unsigned long i = 0; Array + i < ArrayElement; ++i)
		Sum += *(Array + i);
}