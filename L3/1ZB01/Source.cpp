#include <iostream>
#include <ctime>

#include "MyHeapArray.h"

int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyHeapArray<int,unsigned long> heap;
	
	double dokladnosc = 1;

	timeStart = clock();
	for (int i = 0; i < 1000000; ++i) heap.addElement(i);
	timeStop = clock();
	
	//for (int i = 0; i < dokladnosc; ++i) heap.sumHeap();

	//heap.showHeapSum();

	std::cout << "Czas dodawania: " << difftime(timeStop, timeStart)/dokladnosc << " [ms]\n";

	while (1);
	return 0;
}