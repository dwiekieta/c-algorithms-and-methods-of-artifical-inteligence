#include <iostream>

#include "MyHeap.h"


int main()
{
	MyHeap<int> heap;

	for (int i = 0; i < 10; ++i) heap.addElement(i);

	heap.showHeap();
	heap.removeElement();
	heap.showHeap();
	heap.removeHeap();
	heap.showHeap();

	while (1);

	return 0;
}