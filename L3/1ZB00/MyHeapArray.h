#pragma once

template<class T>
class MyHeapArray
{
	T* Array;
	T* ArrayEnd;
	T* ArrayElement;
	
	void create();
	void resize();

public: 
	MyHeapArray();
	~MyHeapArray();

	void addElement(const T&);
	bool removeElement();
	void removeHeap();

	void showHeap() const;
};

template<class T>
MyHeapArray<T>::MyHeapArray()
{
	Array = ArrayEnd = ArrayElement = nullptr;
}


template<class T>
MyHeapArray<T>::~MyHeapArray()
{
	removeHeap();
}


template<class T>
void MyHeapArray<T>::create()
{
	if (Array != nullptr) removeHeap();

	Array = new T[8];
	ArrayEnd = Array + 8;
	ArrayElement = Array;
}


template<class T>
void MyHeapArray<T>::resize()
{
	unsigned long size = ArrayEnd - Array;
	unsigned long fill = ArrayElement - Array;
	double fillRange = (double)fill / size;

	//std::cout << "size: " << size << " fill: " << fill << " fillRange: " << fillRange << std::endl;

	T* temp;
	T* lastTemp;
	if (fillRange > 0.9)
	{
		temp = new T[2 * size];
		lastTemp = temp + (2 * size);
	}
	else if (fillRange < 0.45 && size > 8)
	{
		temp = new T[size / 2];
		lastTemp = temp + (size / 2);
	}
	else return;

	for (unsigned long i = 0; i < fill; ++i)
		*(temp + i) = *(Array + i);

	delete[] Array;
	Array = temp;
	ArrayEnd = lastTemp;
	ArrayElement = Array + fill;
}


template<class T>
void MyHeapArray<T>::addElement(const T& val)
{
	if (Array == nullptr)
		create();

	resize();
	*ArrayElement = val;
	ArrayElement = ArrayElement + 1;
}


template<class T>
bool MyHeapArray<T>::removeElement()
{
	if (ArrayElement == Array) return false;

	ArrayElement--;
	resize();
	return true;
}


template<class T>
void MyHeapArray<T>::removeHeap()
{
	while (removeElement());

	delete[] Array;
	Array = ArrayEnd = ArrayElement = nullptr;
}


template<class T>
void MyHeapArray<T>::showHeap() const
{
	if (Array == nullptr) return;

	unsigned long size = ArrayElement - Array;

	for (; size; --size)
		std::cout << "[ " << size << " ] " << Array[size - 1] << std::endl;

}