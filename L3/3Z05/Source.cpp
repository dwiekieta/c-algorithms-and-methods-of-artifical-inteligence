#include <iostream>
#include <ctime>

#include "Hanoi.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyHanoi hanoi;
	
	//double dokladnosc = 100;

	timeStart = clock();
	hanoi.setHanoi(30);
	hanoi.startHanoi(true);
	timeStop = clock();

	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);

	return 0;
}