#pragma once

#include "MyHeap.h"

class MyHanoi
{
	MyHeap<int> *Heap1;
	MyHeap<int> *Heap2;
	MyHeap<int> *Heap3;

	void move(int, MyHeap<int>*, MyHeap<int>*, MyHeap<int>*);

	int ShowStep;
	int Size;
	int Step;

public:
	MyHanoi();
	~MyHanoi();

	void setHanoi(const int&);
	void startHanoi(const int&);
	void showHanoi();
};

