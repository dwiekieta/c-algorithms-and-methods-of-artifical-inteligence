#include <iostream>
#include <ctime>

#include "Hanoi.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyHanoi hanoi;
	
	//double dokladnosc = 100;

	hanoi.setHanoi(25);

	timeStart = clock();
	hanoi.startHanoi(101);
	timeStop = clock();

	hanoi.showHanoi();

	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);

	return 0;
}