#include<iostream>

#include"Hanoi.h"

MyHanoi::MyHanoi()
{
	Heap1 = new MyHeap<int>;
	Heap2 = new MyHeap<int>;
	Heap3 = new MyHeap<int>;

	ShowStep = 0;
	Step = 0;
}

MyHanoi::~MyHanoi()
{
	delete Heap1;
	delete Heap2;
	delete Heap3;
}

void MyHanoi::setHanoi(const int &lvl)
{
	Size = lvl;
	for (int i = lvl; i; i--)
		Heap1->addElement(i);
}

void MyHanoi::startHanoi(const int &show)
{
	ShowStep = show;
	move(Size, Heap1, Heap2, Heap3);
}


void MyHanoi::move(int ile, MyHeap<int> *A, MyHeap<int> *B, MyHeap<int> *C)
{
	if (ile > 0)
	{
		move(ile - 1, A, C, B);
			C->addElement(A->moveElement());
			if (Step == ShowStep) showHanoi();
			Step++;
		move(ile - 1, B, A, C);
	}
}


void MyHanoi::showHanoi()
{
	for (int i = Size; i; --i)
	{
		std::cout << "[ " << i << " ]\t" << 
			Heap1->getHeapLevel(i) << '\t' <<
			Heap2->getHeapLevel(i) << '\t' <<
			Heap3->getHeapLevel(i) << std::endl;
	}

	std::cout << std::endl;
}