#pragma once

template<typename T>
struct MyHeapItem {
	T ItemValue;
	MyHeapItem* PreviousItem;
};

template <class T>
class MyHeap
{
	MyHeapItem<T> *LastItem;
	int HeapElements;

public:
	MyHeap();
	~MyHeap();

	void addElement(const T&);
	void addElement(MyHeapItem<T>*);

	MyHeapItem<T>* moveElement();

	bool removeElement();
	void removeHeap();

	void showHeap() const;
	T getHeapLevel(const int&) const;

	T getValue() const;

	bool operator<(const MyHeap<T>&);
	bool operator>(const MyHeap<T>&);
};


template<class T>
MyHeap<T>::MyHeap()
{
	LastItem = nullptr;
	HeapElements = 0;
}


template<class T>
MyHeap<T>::~MyHeap()
{
	removeHeap();
}


template<class T>
void MyHeap<T>::addElement(const T& val)
{
	if (LastItem == nullptr)
	{
		LastItem = new MyHeapItem<T>;
		LastItem->PreviousItem = nullptr;
	}
	else 
	{
		MyHeapItem<T> *temp = new MyHeapItem<T>;
		temp->PreviousItem = LastItem;
		LastItem = temp;
	}

	LastItem->ItemValue = val;
	HeapElements++;
}


template<class T>
void MyHeap<T>::addElement(MyHeapItem<T> *item)
{
	if (item == nullptr) return;

	item->PreviousItem = LastItem;
	LastItem = item;

	HeapElements++;
}


template<class T>
MyHeapItem<T>* MyHeap<T>::moveElement()
{
	if (LastItem == nullptr) return nullptr;

	HeapElements--;

	MyHeapItem<T> *temp = LastItem;
	LastItem = LastItem->PreviousItem;

	return temp;
}

template<class T>
bool MyHeap<T>::removeElement()
{
	if (LastItem == nullptr) return false;

	MyHeapItem<T> *temp = LastItem;

	LastItem = LastItem->PreviousItem;
	delete temp;
	
	HeapElements--;
	return true;
}


template<class T>
void MyHeap<T>::removeHeap()
{
	while (removeElement());
}


template<class T>
void MyHeap<T>::showHeap() const
{
	if (LastItem == nullptr) return;

	MyHeapItem<T> *temp = LastItem;

	for (int i = HeapElements; temp != nullptr; --i)
	{
		std::cout << "[ " << i << " ] " << temp->ItemValue << std::endl;
		temp = temp->PreviousItem;
	}
}


template<class T>
T MyHeap<T>::getHeapLevel(const int &lvl) const
{
	if (HeapElements < lvl) return NULL;

	MyHeapItem<T> *temp = LastItem;

	for (int i = HeapElements; i > lvl; --i) temp = temp->PreviousItem;

	return temp->ItemValue;	
}


template<class T>
T MyHeap<T>::getValue() const
{
	if (LastItem == nullptr) return NULL;

	return LastItem->ItemValue;
}


template<class T>
bool MyHeap<T>::operator<(const MyHeap<T> &y)
{
	if (LastItem == nullptr && y.getValue() == NULL) return false;
	else if (LastItem == nullptr && y.getValue() != NULL) return false;	//bo bardziej sie oplaci interpretowac pusty bufor jako el. wiekszy od wybranego 
	else if (LastItem != nullptr && y.getValue() == NULL) return true;
	else return (LastItem->ItemValue < y.getValue());
}


template<class T>
bool MyHeap<T>::operator>(const MyHeap<T> &y)
{
	if (LastItem == nullptr && y.getValue() == NULL) return false;
	else if (LastItem == nullptr && y.getValue() != NULL) return true;
	else if (LastItem != nullptr && y.getValue() == NULL) return false;
	else return (LastItem->ItemValue > y.getValue());
}