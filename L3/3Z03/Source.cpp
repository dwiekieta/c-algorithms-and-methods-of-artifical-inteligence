#include <iostream>
#include <ctime>

#include "Hanoi.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyHanoi hanoi;
	
	double dokladnosc = 100;

	/*
	MyHeap<int> heap, heap1;

	for (int i = 0; i < 100; ++i) heap.addElement(i);
	heap.showHeap();

	for (int i = 0; i < 50; ++i) heap1.addElement(heap.moveElement());

	heap.showHeap();
	heap1.showHeap();

	heap.showHeapLevel(2);
	heap1.showHeapLevel(2);

	std::cout << "h1<h2 " << (heap < heap1) << " h1>h2 " << (heap > heap1) << std::endl;
	*/

	timeStart = clock();
	hanoi.setHanoi(6);
	hanoi.startHanoi(true);
	timeStop = clock();

	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);

	return 0;
}