#pragma once


template<class T, class S>
class MyQeueArray
{
	T* Array;
	T* ArrayBegin;
	T* ArrayEnd;
	T* ArrayElement;

	void create();
	void resize();

	S Sum;

public:
	MyQeueArray();
	~MyQeueArray();

	void addElement(const T&);
	bool removeElement();
	void removeQeue();

	void showQueue() const;
	void showSum() const;

	void sumQueue();
};


template<class T, class S>
MyQeueArray<T,S>::MyQeueArray()
{
	Array = ArrayElement = ArrayEnd = ArrayBegin = nullptr;
}


template<class T, class S>
MyQeueArray<T,S>::~MyQeueArray()
{
	removeQeue();
}


template<class T, class S>
void MyQeueArray<T,S>::create()
{
	if (Array != nullptr) removeQeue();

	Array = new T[8];
	ArrayEnd = Array + 8;
	ArrayElement = ArrayBegin = Array;
}


template<class T, class S>
void MyQeueArray<T,S>::resize()
{
	unsigned long size = ArrayEnd - Array;
	unsigned long fill = ArrayElement - ArrayBegin;
	double fillRange = (double)fill / size;

	//std::cout << "size: " << size << " fill: " << fill << " fillRange: " << fillRange << std::endl;

	T* temp;
	T* tempLast;

	if (fillRange > 0.9)
	{
		temp = new T[size * 2];
		tempLast = temp + (size * 2);
	}
	else if (fillRange < 0.45 && size > 8)
	{
		temp = new T[size / 2];
		tempLast = temp + (size / 2);
	}
	else return;

	for (unsigned long i = 0; i < fill; ++i)
		*(temp + i) = *(ArrayBegin + i);

	delete[] Array;
	Array = ArrayBegin = temp;
	ArrayEnd = tempLast;
	ArrayElement = Array + fill;
}


template<class T, class S>
void MyQeueArray<T,S>::addElement(const T&val)
{
	if (Array == nullptr) create();

	resize();
	*ArrayElement = val;
	ArrayElement++;
}


template<class T, class S>
bool MyQeueArray<T,S>::removeElement()
{
	if (Array == ArrayElement) return false;

	ArrayBegin++;
	resize();

	return true;
}


template<class T, class S>
void MyQeueArray<T,S>::removeQeue()
{
	while (removeElement());
	Array = ArrayElement = ArrayEnd = ArrayBegin = nullptr;
}


template<class T, class S>
void MyQeueArray<T,S>::showQueue() const
{
	if (Array == nullptr) return;


	unsigned long size = ArrayElement - ArrayBegin;

	for (unsigned long i = 0; i < size; ++i)
		std::cout << "[ " << i << " ] " << *(ArrayBegin + i) << std::endl;
}

template<class T, class S>
void MyQeueArray<T, S>::showSum() const
{
	if (Array == nullptr) return;

	std::cout << "Suma elementow: " << Sum << std::endl;
}


template<class T, class S>
void MyQeueArray<T, S>::sumQueue() 
{
	Sum = 0;

	for (unsigned long i = 0; ArrayBegin + i < ArrayElement; ++i)
		Sum += *(ArrayBegin + i);
}