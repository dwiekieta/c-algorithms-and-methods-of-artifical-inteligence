#include <iostream>
#include <ctime>

#include "MyQeueArray.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyQeueArray<int,unsigned long> queue;

	double dokladnosc = 100;

	for (int i = 0; i < 500000; ++i)
		queue.addElement(rand()%100);

	timeStart = clock();
	for (int i = 0; i < dokladnosc; ++i)
		queue.sumQueue();
	timeStop = clock();

	queue.showQueue();

	std::cout << "Czas dodawania: " << difftime(timeStop, timeStart)/dokladnosc << " [ms]\n";

	while (1);
	return 0;
}