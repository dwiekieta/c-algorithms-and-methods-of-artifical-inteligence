#include <iostream>

using namespace std;
int main()
{
	int *wskaznik = new int[10];

	cout << "wsk: " << wskaznik << " *wsk: " << *wskaznik << endl;

	wskaznik += 1;
	cout << "wsk: " << wskaznik << " *wsk: " << *wskaznik << endl;

	*wskaznik = 1;
	cout << "wsk: " << wskaznik << " *wsk: " << *wskaznik << endl;

	*wskaznik = *(wskaznik - 1);
	cout << "wsk: " << wskaznik << " *wsk: " << *wskaznik << endl;

	cout << "wsk: " << wskaznik - (wskaznik+5) << " *wsk: " << *wskaznik << endl;

	int a = 2, b = 5;
	double c = (double)a / b;

	cout << "c = " << c << endl;

	while (1);
	return 0;
}