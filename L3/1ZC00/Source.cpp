#include <iostream>
#include <stack>
#include <ctime>


int main()
{
	clock_t t_start, t_stop;
	std::stack<int> stos;

	long rozmiar = 100000;

	for (int i = 0; i < rozmiar; ++i)
		stos.push(rand() % 100);

	unsigned long long suma = 0;

	t_start = clock();
	for (long i = 0; i < rozmiar; ++i)
	{
		suma += stos.top();
		stos.pop();
	}
	t_stop = clock();

	std::cout << "Suma stosu: " << suma << std::endl;
	std::cout << "Czas sumowania: " << difftime(t_stop, t_start) << " [ms]" << std::endl;

	while (1);
	return 0;
}