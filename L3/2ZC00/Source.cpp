#include <iostream>
#include <queue>
#include <ctime>


int main()
{
	clock_t t_start, t_stop;
	std::queue<int> kolejka;

	long rozmiar = 500000;

	unsigned long long suma = 0;

	for (long i = 0; i < rozmiar; ++i)
		kolejka.push(rand() % 100);

	t_start = clock();
	for (long i = 0; i < rozmiar; ++i)
	{
		suma += kolejka.front();
		kolejka.pop();
	}
	t_stop = clock();

	std::cout << "Suma kolejki: " << suma << std::endl;
	std::cout << "Czas sumowania: " << difftime(t_stop, t_start) << " [ms]" << std::endl;

	while (1);
	return 0;
}