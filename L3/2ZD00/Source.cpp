#include <iostream>
#include <ctime>

#include "MyQeueArray.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyQeueArray<int,unsigned long> queue;

	timeStart = clock();

	for (int i = 0; i < 500000; ++i)
		queue.addElement(i);

	timeStop = clock();

	queue.showSum();

	std::cout << "Czas dodawania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);
	return 0;
}