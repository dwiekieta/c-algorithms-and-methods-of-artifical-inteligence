#pragma once

template<class T, class S>
class MyQueue
{
	struct MyQueueItem
	{
		T ItemValue;
		MyQueueItem *NextItem;
	};

	MyQueueItem *FirstItem;
	MyQueueItem *LastItem;

	S Sum;

public:
	MyQueue();
	~MyQueue();
	
	void addElement(const T&);
	void removeElement();
	void removeQueue();

	void showQueue();
	void showSum();

	void sumQueueElements();
};


template<class T, class S>
MyQueue<T,S>::MyQueue()
{
	FirstItem = LastItem = nullptr;
}


template<class T, class S>
MyQueue<T,S>::~MyQueue()
{
	removeQueue();
}


template<class T, class S>
void MyQueue<T,S>::addElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyQueueItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	LastItem->NextItem = new MyQueueItem;
	LastItem = LastItem->NextItem;

	LastItem->ItemValue = val;
	LastItem->NextItem = nullptr;
}


template<class T, class S>
void MyQueue<T,S>::removeElement()
{
	if (FirstItem == nullptr) return;

	MyQueueItem *temp = FirstItem;

	FirstItem = FirstItem->NextItem;
	delete temp;
}


template<class T, class S>
void MyQueue<T,S>::removeQueue()
{
	while (FirstItem != nullptr)
		removeElement();
}


template<class T, class S>
void MyQueue<T,S>::showQueue()
{
	std::cout << "Kolejka: " << std::endl;

	int el = 0;
	MyQueueItem *temp = FirstItem;

	do
	{
		std::cout << "[ " << el << " ] " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}



template<class T, class S>
void MyQueue<T, S>::sumQueueElements()
{
	Sum = 0;

	MyQueueItem *temp = FirstItem;
	while (temp != nullptr)
	{
		Sum += temp->ItemValue;
		temp = temp->NextItem;
	}
}


template<class T, class S>
void MyQueue<T, S>::showSum()
{

	std::cout << "Suma elementow: " << Sum << std::endl;
}