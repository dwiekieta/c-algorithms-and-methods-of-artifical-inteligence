#include <iostream>
#include <ctime>

#include "MyQueue.h"


int main()
{
	clock_t timeStart, timeStop;

	MyQueue<int, unsigned long> queue;

	double dokladnosc = 1000;

	timeStart = clock();
	for (long i = 0; i < 1000000; ++i) queue.addElement(i);
	timeStop = clock();
	
	for (int i = 0; i < dokladnosc; ++i)
		queue.sumQueueElements();
	

	queue.showSum();

	std::cout << "Czas dodawania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);

	return 0;
}