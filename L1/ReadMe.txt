Solution "L1" involves eight projects:

I.1 1Z00
	This project is an solution for creating and printing dynamic two dimension arrays
I.2 1Z01
	Adds ability to filling arrays by numbers
I.3 1Z02
	Adds ability to:
		- creating arrays from file,
		- saveing arrays to file,
		- finding maximum value in array,
		- managing arrays from menu.
II.1 2Z00
	Lunchs all abilities from I.3 

III 3Z00
	This project is an solution for calculating powers and factorials of numbers
IV.1 4Z00
	This project is an solution for check if word is a palindrome
IV.2 4Z01
	Adds recursive method