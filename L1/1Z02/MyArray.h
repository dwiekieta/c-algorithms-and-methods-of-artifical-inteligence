#pragma once
class MyArray
{
	int **Array;

	int Wiersze;
	int Kolumny;
	bool ArrayExist;

	bool create();

public:
	MyArray(int w = 0, int k = 0);
	~MyArray();

	void create(int w, int k);
	void remove();

	void show();
	
	void solidFill(int);
	void randFill(int min = 0, int max = 1);
	int globalMax() const;
	int globalMin() const;

	int randInt(int min, int max);
};

