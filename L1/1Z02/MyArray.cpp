#include "MyArray.h"
#include <iostream>


MyArray::MyArray(int w, int k)
{
	Wiersze = w;
	Kolumny = k;

	ArrayExist = create();
}


MyArray::~MyArray()
{
	remove();
}


bool MyArray::create()
{
	if (!(Wiersze && Kolumny)) return false;

	Array = new int*[Wiersze];
	for (int i = 0; i < Wiersze; ++i) Array[i] = new int[Kolumny];

	return true;
}


void MyArray::create(int w, int k)
{
	if (ArrayExist) remove();

	Wiersze = w;
	Kolumny = k;

	ArrayExist = create();
}


void MyArray::remove()
{
	if (ArrayExist)
	{
		for (int i = 0; i < Wiersze; ++i) delete[] Array[i];
		delete[] Array;
	}

	Wiersze = Kolumny = 0;

	ArrayExist = false;
}


void MyArray::show()
{
	std::cout << "\tTablica 2D\tw: " << Wiersze << " k: " << Kolumny << " E: " << ArrayExist << " max = " << globalMax() << " min = " << globalMin() << std::endl;

	if (!ArrayExist) return;

	for (int w = 0; w < Wiersze; ++w)
	{
		for (int k = 0; k < Kolumny; ++k)
		{
			std::cout.width(7);
			std::cout << Array[w][k];
		}
		std::cout << std::endl;
	}
}


void MyArray::solidFill(int n)
{
	if (!ArrayExist) return;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) Array[w][k] = n;
}


void MyArray::randFill(int min, int max)
{
	if (!ArrayExist) return;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) Array[w][k] = randInt(min,max);
}


int MyArray::randInt(int min, int max)
{
	if (!(max - min)) return 0;

	return (std::rand() % (max - min)) + min;
}

int MyArray::globalMax()const
{
	if (!ArrayExist) return 0;

	int max = 0;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) if (Array[w][k] > max) max = Array[w][k];

	return max;
}


int MyArray::globalMin()const
{
	if (!ArrayExist) return 0;

	int min = globalMax();

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) if (Array[w][k] < min) min = Array[w][k];

	return min;
}