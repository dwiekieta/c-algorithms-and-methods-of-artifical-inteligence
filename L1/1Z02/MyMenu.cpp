#include <iostream>
#include <string>

#include "MyMenu.h"
#include "MyArray.h"

void mainMenu(MyArray* arr)
{
	int o = 0;
	do
	{
		std::cout << "==== MENU =====" <<
			"\n\t1. Tablica" <<
			//"\n\t2. Lista" <<
			//"\n\t3. Kopiec" <<
			"\n\t0. Wyjscie" << std::endl;

		do
		{
			std::cin >> o;
		} while (!(o == 1 || o == 0));

		if(o) itemMenu(arr);
	} while (o);

}

void itemMenu(MyArray* arr)
{
	int o = 0;
	do
	{
		std::cout << "---- Tablica ----" <<
			"\n\t1. Wczytaj z pliku tekstowego" <<
			"\n\t2. Wczytaj z pliku binarnego" <<
			"\n\t3. Zapisz do pliku tekstowego" <<
			"\n\t4. Zapisz do pliku binarnego" <<
			"\n\t5. Wypelnij losowo w podanym zakresie" <<
			"\n\t6. Wyswietl" <<
			"\n\t7. Znajdz maksymlna wartosc"
			"\n\t8. Test" <<
			"\n\t0. Powrot" << std::endl;
		
		do
		{
			std::cin >> o;
		} while (!(o < 9));

		switch (o)
		{
		case 1: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			//arr.openFromTextFile(np);
		} break;
		case 2: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			//arr.openFromBinaryFile(np);
		} break;
		case 3: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			//arr.saveToTextFile(np);
		} break;
		case 4: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			//MyArray.saveToBinaryFile(np);
		} break;
		case 5: arr->randFill();
			break;
		case 6: arr->show();
			break;
		case 7: arr->globalMax();
		case 8:
			break;
		};

	} while (o);
}