/*
*
*/


#include <iostream>

#include "MyArray.h"
#include "MyList.h"
#include "MyMenu.h"


int main()
{
	MyArray *Arr = new MyArray;
	MyArray *Arr1 = new MyArray;
	MyList<MyArray> *list = new MyList<MyArray>;
	//MyList<MyArray> *temp;

	mainMenu(Arr);

	Arr->create(20, 30);
	Arr->randFill(2,1000);

	Arr1->create(6, 5);
	Arr1->solidFill(4);
	
	list->setData(Arr);
	list->setNextItem(new MyList<MyArray>(list));
	list = list->nextItem();
	list->setData(Arr1);
	list = list->previousItem();

	(list->getData())->show();
	list = list->nextItem();
	(list->getData())->show();
	

	int k;
	std::cin >> k;
	return 0;
}