#include "MyArray.h"
#include <iostream>


MyArray::MyArray(int w, int k)
{
	Wiersze = w;
	Kolumny = k;

	ArrayExist = create();
}


MyArray::~MyArray()
{
	remove();
}


bool MyArray::create()
{
	if (!(Wiersze && Kolumny)) return false;

	Array = new int*[Wiersze];
	for (int i = 0; i < Wiersze; ++i) Array[i] = new int[Kolumny];

	return true;
}


void MyArray::create(int w, int k)
{
	if (ArrayExist) remove();

	Wiersze = w;
	Kolumny = k;

	ArrayExist = create();
}


void MyArray::remove()
{
	if (ArrayExist)
	{
		for (int i = 0; i < Wiersze; ++i) delete[] Array[i];
		delete[] Array;
	}

	Wiersze = Kolumny = 0;

	ArrayExist = false;
}


void MyArray::show()
{
	std::cout << "\tTablica 2D\tw: " << Wiersze << " k: " << Kolumny << " E: " << ArrayExist <<  std::endl;

	if (!ArrayExist) return;

	for (int w = 0; w < Wiersze; ++w)
	{
		std::cout.width(7);
		for (int k = 0; k < Kolumny; ++k) std::cout << Array[w][k];
		std::cout << std::endl;
	}
}