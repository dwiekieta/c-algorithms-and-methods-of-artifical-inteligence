#include "MyFile.h"



MyFile::MyFile()
{
	FileExist = false;
}


MyFile::~MyFile()
{
	if (FileExist) File.close();
}


void MyFile::read(MyArray* arr, bool TextType, std::string name)
{
	if (TextType)
	{
		int w, k;

		File.open(name, std::ios::in);

		if (!File.is_open()) return;

		File >> w >> k;
		while (!arr->create(w, k));

		for (int i = 0; i < arr->getWiersze(); ++i)
			for (int j = 0; j < arr->getKolumny(); ++j)
			{
				File >> w;
				arr->setArray(i, j, w);
			}
	}

	else
	{
		int *w, *k;

		File.open(name, std::ios::in | std::ios::binary);

		if (!File.is_open()) return;

		char *cint = new char[sizeof(int)];
		File.read(cint, sizeof(int));
		w = (int*)(cint);

		cint = new char[sizeof(int)];
		File.read(cint, sizeof(int));
		k = (int*)(cint);

		while (!arr->create(*w, *k));

		delete w;
		delete k;

		for (int i = 0; i < arr->getWiersze(); ++i)
			for (int j = 0; j < arr->getKolumny(); ++j)
			{
				cint = new char[sizeof(int)];
				File.read(cint, sizeof(int));
				w = (int*)(cint);

				arr->setArray(i, j, *w);
				delete w;
			}
	}

	File.close();
}


void MyFile::write(MyArray* arr, bool TextType, std::string name)
{
	if (TextType)
	{
		File.open(name, std::ios::out | std::ios::trunc);

		if (!File.is_open()) return;

		File << arr->getWiersze() << " " << arr->getKolumny() << "\n";

		for (int i = 0; i < arr->getWiersze(); ++i)
			for (int j = 0; j < arr->getKolumny(); ++j)
				File << arr->getArray(i, j) << " ";
	}

	else
	{
		File.open(name, std::ios::out | std::ios::trunc | std::ios::binary);

		if (!File.is_open()) return;

		int w = arr->getWiersze();
		int k = arr->getKolumny();

		File.write((char*)(&w), sizeof(int));
		File.write((char*)(&k), sizeof(int));

		for (int i = 0; i < arr->getWiersze(); ++i)
			for (int j = 0; j < arr->getKolumny(); ++j)
			{
				w = arr->getArray(i, j);
				File.write((char*)(&w), sizeof(int));
			}

	}

	File.close();
}