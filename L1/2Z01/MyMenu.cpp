#include <iostream>
#include <string>

#include "MyMenu.h"
#include "MyArray.h"
#include "MyFile.h"

void mainMenu(MyArray* arr)
{
	int o = 0;
	do
	{
		std::cout << "==== MENU =====" <<
			"\n\t1. Tablica" <<
			//"\n\t2. Lista" <<
			//"\n\t3. Kopiec" <<
			"\n\t0. Wyjscie" << std::endl;

		do
		{
			std::cin >> o;
		} while (!(o == 1 || o == 0));

		if(o) itemMenu(arr);
	} while (o);

}

void itemMenu(MyArray* arr)
{
	int o = 0;
	do
	{
		std::cout << "---- Tablica ----" <<
			"\n\t1. Wczytaj z pliku tekstowego" <<
			"\n\t2. Wczytaj z pliku binarnego" <<
			"\n\t3. Zapisz do pliku tekstowego" <<
			"\n\t4. Zapisz do pliku binarnego" <<
			"\n\t5. Uworz tablice" <<
			"\n\t6. Wypelnij losowo w podanym zakresie" <<
			"\n\t7. Wyswietl" <<
			"\n\t8. Znajdz maksymlna wartosc"
			"\n\t9. Test" <<
			"\n\t0. Powrot" << std::endl;
		
		do
		{
			std::cin >> o;
		} while (!(o < 9));

		switch (o)
		{
		case 1: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			MyFile plik;
			plik.read(arr, true, np);
		} break;
		case 2: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			MyFile plik;
			plik.read(arr, false, np);
		} break;
		case 3: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			MyFile plik;
			plik.write(arr, true, np);
		} break;
		case 4: {
			std::cout << "Podaj nazwe pliku: " << std::endl;
			std::string np;
			std::cin >> np;

			MyFile plik;
			plik.write(arr, false, np);
		} break;
		case 5: {
			int w, k;
			std::cout << "Podaj liczbe wierszy: " << std::endl;
			std::cin >> w;
			std::cout << "Podaj liczbe kolumn: " << std::endl;
			std::cin >> k;

			arr->create(w, k);
		} break;
		case 6: {
			int a, b;
			std::cout << "Podaj minimum zakresu: " << std::endl;
			std::cin >> a;
			std::cout << "Podaj maksimum zakresu: " << std::endl;
			std::cin >> b;

			arr->randFill(a,b);
		}
			break;
		case 7: arr->show();
			break;
		case 8: {
			std::cout << "Maksymalna wartosc: " <<  arr->globalMax() << std::endl;
		} break;
		case 9:
			break;
		};

	} while (o);
}