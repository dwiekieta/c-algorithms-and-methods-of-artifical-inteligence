#pragma once

#include <fstream>

#include "MyArray.h"

class MyFile
{
	std::fstream File;

	bool FileExist;

public:
	MyFile();
	~MyFile();

	void read(MyArray*, bool, std::string);
	void write(MyArray*, bool, std::string);
};

