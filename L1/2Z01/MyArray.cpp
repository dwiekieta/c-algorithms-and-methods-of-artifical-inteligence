#include "MyArray.h"
#include <iostream>


MyArray::MyArray(int w, int k)
{
	Wiersze = w;
	Kolumny = k;

	Exist = create();
}


MyArray::~MyArray()
{
	remove();
}


bool MyArray::create()
{
	if (!(Wiersze && Kolumny)) return false;

	Array = new int*[Wiersze];
	for (int i = 0; i < Wiersze; ++i) Array[i] = new int[Kolumny];

	return true;
}


bool MyArray::create(int w, int k)
{
	if (Exist) remove();

	Wiersze = w;
	Kolumny = k;

	Exist = create();

	return Exist;
}


void MyArray::remove()
{
	if (Exist)
	{
		for (int i = 0; i < Wiersze; ++i) delete[] Array[i];
		delete[] Array;
	}

	Wiersze = Kolumny = 0;

	Exist = false;
}


void MyArray::show()
{
	std::cout << "\tTablica 2D\tw: " << Wiersze << " k: " << Kolumny << " E: " << Exist << " max = " << globalMax() << " min = " << globalMin() << std::endl;

	if (!Exist) return;

	for (int w = 0; w < Wiersze; ++w)
	{
		for (int k = 0; k < Kolumny; ++k)
		{
			std::cout.width(7);
			std::cout << Array[w][k];
		}
		std::cout << std::endl;
	}
}


void MyArray::solidFill(int n)
{
	if (!Exist) return;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) Array[w][k] = n;
}


void MyArray::randFill(int min, int max)
{
	if (!Exist) return;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) Array[w][k] = randInt(min,max);
}


int MyArray::randInt(int min, int max)
{
	if (!(max - min)) return 0;

	return (std::rand() % (max - min)) + min;
}

int MyArray::globalMax()const
{
	if (!Exist) return 0;

	int max = 0;

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) if (Array[w][k] > max) max = Array[w][k];

	return max;
}


int MyArray::globalMin()const
{
	if (!Exist) return 0;

	int min = globalMax();

	for (int w = 0; w < Wiersze; ++w)
		for (int k = 0; k < Kolumny; ++k) if (Array[w][k] < min) min = Array[w][k];

	return min;
}


int MyArray::getArray(int w, int k) const
{
	return Array[w][k];
}


int MyArray::getWiersze() const
{
	return Wiersze;
}


int MyArray::getKolumny() const
{
	return Kolumny;
}


void MyArray::setArray(int w, int k, int v) 
{
	Array[w][k] = v;
}