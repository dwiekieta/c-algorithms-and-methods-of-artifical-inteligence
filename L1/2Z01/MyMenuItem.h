#pragma once

#include <iostream>

#include "MyArray.h"

template <class T>
class MyMenuItem
{
	std::string ItemName;
	std::string ItemMenu;

	T *Item;

	bool Exist;

public:
	MyMenuItem(T* = nullptr);
	~MyMenuItem();

	std::string showItemMenu();
};

template <class T>
MyMenuItem<T>::MyMenuItem(T *t)
{
	Exist = false;
	Item = t;
}

