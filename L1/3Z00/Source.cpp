#include <iostream>

long Potega(int x, int p);
long Silnia(int z);

int main()
{
	int x = 5;
	int p = 10;
	int z = 9;

	std::cout << "Potegowanie: " << x << "^" << p << " = " << Potega(x, p) << std::endl;
	std::cout << "Silnia: " << z << " = " << Silnia(z) << std::endl;

	while (1);

	return 0;
}

long Potega(int x, int p)
{
	if (!p) return 1;

	long t = x;
	for (int i = 1; i < p; ++i) t *= x;
	return t;
}


long Silnia(int z)
{
	if (!z) return 1;

	long t = 1;
	for (int i = 1; i <= z; ++i) t = t * i;
	return t;
}