#include <iostream>

#include "MyArray.h"


int main()
{
	MyArray Arr;

	Arr.show();

	Arr.create(4, 6);
	Arr.solidFill(3);
	Arr.show();

	Arr.create(15, 20);
	Arr.randFill(2, 8);
	Arr.show();

	int a;
	std::cin >> a;

	return 0;
}