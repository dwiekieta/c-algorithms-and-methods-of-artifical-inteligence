#pragma once
#include <iostream>

template <class T>
class MyList
{
	MyList* Previous;
	MyList* Next;
	T* Data;

public:
	MyList();
	MyList(MyList*);
	~MyList();

	void setPreviousItem(MyList*);
	void setNextItem(MyList*);

	MyList* previousItem() const;
	MyList* nextItem() const;

	T* getData() const;
	void setData(T*);
};


template <class T>
MyList<T>::MyList()
{
	Previous = nullptr;
	Next = nullptr;

	Data = new T;
}


template <class T>
MyList<T>::MyList(MyList<T>* pr)
{
	Previous = pr;
	Next = nullptr;

	Data = new T;
}


template <class T>
MyList<T>::~MyList()
{
	delete Data;
}


template <class T>
void MyList<T>::setPreviousItem(MyList<T>* pi)
{
	Previous = pi;
}


template <class T>
void MyList<T>::setNextItem(MyList<T>* ni)
{
	Next = ni;
}


template <class T>
MyList<T>* MyList<T>::previousItem() const
{
	return Previous;
}


template <class T>
MyList<T>* MyList<T>::nextItem() const
{
	return Next;
}


template <class T>
T* MyList<T>::getData() const
{
	return Data;
}


template <class T>
void MyList<T>::setData(T* t)
{
	Data = t;
}