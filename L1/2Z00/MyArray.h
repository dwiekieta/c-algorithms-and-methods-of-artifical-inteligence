#pragma once
class MyArray
{
	int **Array;

	int Wiersze;
	int Kolumny;
	bool Exist;

	bool create();

public:
	MyArray(int w = 0, int k = 0);
	~MyArray();

	bool create(int w, int k);
	void remove();

	void show();
	
	void solidFill(int);
	void randFill(int min = 0, int max = 1);
	int globalMax() const;
	int globalMin() const;

	int randInt(int min, int max);

	int getArray(int w, int k) const;
	int getWiersze() const;
	int getKolumny() const;

	void setArray(int w, int k, int v);
	
};

