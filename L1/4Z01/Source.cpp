#include <iostream>
#include <string>

bool Palindrom(const std::string &, int);

int main()
{
	std::string w;
	while (1)
	{
		std::cout << "Wprowadz slowo: ";
		std::cin >> w;
		std::cout << "\"" << w << "\" ";
		if (!Palindrom(w.c_str(),0)) std::cout << "nie ";
		std::cout << "jest palindromem" << std::endl;
	}

	return 0;
}

bool Palindrom(const std::string &s, int p)
{
	bool isP = true;
	int l = s.length();


	if (p < (l / 2))
		(s[p] == s[l - p - 1]) ? isP = Palindrom(s.c_str(), p + 1) : isP = false;

	return isP;
}