#include <iostream>
#include <string>

bool Palindrom(std::string);

int main()
{
	std::string w;
	while (1)
	{
		std::cout << "Wprowadz slowo: ";
		std::cin >> w;
		std::cout << "\"" << w << "\" ";
		if (!Palindrom(w)) std::cout << "nie ";
		std::cout << "jest palindromem" << std::endl;
	}

	return 0;
}

bool Palindrom(std::string s)
{
	bool p = true;
	int l = s.length();

	for (int i = 0; i < (l / 2); ++i)
	{
		if (s[i] != s[l - i - 1]) p = false;
		break;
	}

	return p;
}