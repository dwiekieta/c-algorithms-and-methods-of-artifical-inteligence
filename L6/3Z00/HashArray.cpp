#include "HashArray.h"
#include <math.h>
#include <iostream>


template<class T>
LinkHashArray<T>::LinkHashArray()
{
	Array = nullptr;
	ArraySize = 0;
}

template<class T>
LinkHashArray<T>::~LinkHashArray()
{
	if (ArraySize) delete[] Array;
}


template<class T>
bool LinkHashArray<T>::PrimaryTest(const int&n)
{
	if (n < 2) return false;

	if (!(n % 2))
	{
		if (n == 2) return true;
		else return false;
	}

	for (int i = 3; i < sqrt(n); i+=2)
		if (!(n%i)) return false;

	return true;
}


template<class T>
int LinkHashArray<T>::NearstPrimary(const int&p, const bool&up)
{
	int i = p;

	for (; !PrimaryTest(i) && i > 1; (up) ? ++i : --i);

	return i;
}

template<class T>
int LinkHashArray<T>::HashFunction(const T&arg)
{
	if (!ArraySize) return -1;

	if (arg < 0)
	{
		int a = arg * -1;
		return a%ArraySize;
	}

	return arg%ArraySize;
}



template<class T>
void LinkHashArray<T>::setArray()
{
	bool m;

	std::cout << "Wprowadz rozmiar tablicy: ";
	std::cin >> ArraySize;
	std::cout << std::endl << "min/max [0/1]: ";
	std::cin >> m;

	ArraySize = NearstPrimary(ArraySize, m);

	Array = new MyQeueArray<T>*[ArraySize];
	for (int i = 0; i < ArraySize; ++i)
		Array[i] = new MyQeueArray<T>;
}


template<class T>
void LinkHashArray<T>::addElement(const T&el)
{
	if (!ArraySize) return;

	if (!el)
	{
		return;
	}

	int index = HashFunction(el);
	Array[0]->
}


template<class T>
int LinkHashArray<T>::findElement(const T&el)
{
	if (!ArraySize)return -1;

	int index = HashFunction(el);
	int i = index;

	for (; Array[i];)
	{
		if (Array[i] == el) return i;

		i += index;
		if (i > ArraySize - 1) i = i - ArraySize;
		if (!i) return -2;
	}

	return -2;
}


template<class T>
void LinkHashArray<T>::removeElement(const T&el)
{
	if (!ArraySize) return;

	int index = findElement(el);

	if (index < 0) return;

	if (!index) *Array = 1;
	else Array[index] = 0;
}

template<class T>
void LinkHashArray<T>::show() const
{
	if (!ArraySize) return;

	for (int i = 0; i < ArraySize; ++i)
		std::cout << "[" << i << "] " << Array[i] << std::endl;
}

template class LinkHashArray<int>;