#include <iostream>

#include "HashArray.h"

int main()
{
	LinkHashArray<int> HArray;

	HArray.setArray();

	for (int i = 0; i < 5; ++i) HArray.addElement(i);
	for (int i = 0; i < 5; ++i) HArray.addElement(-4);

	HArray.show();

	std::cout << "ind: " << HArray.findElement(5) << std::endl;

	HArray.removeElement(3);
	HArray.show();

	while (1);
	return 0;
}