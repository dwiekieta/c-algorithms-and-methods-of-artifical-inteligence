#pragma once
#include "MyQeueArray.h"

template<class T>
class LinkHashArray
{
	MyQeueArray<MyQeueArray<T>>* Array;
	int ArraySize;

	bool PrimaryTest(const int&);
	int NearstPrimary(const int&p, const bool&up = true);
	int HashFunction(const T&);

public:
	LinkHashArray();
	~LinkHashArray();

	void setArray();
	void addElement(const T&);
	int findElement(const T&);
	void removeElement(const T&);

	void show() const;

};

