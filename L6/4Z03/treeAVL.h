#pragma once

#include "TreeNode.h"

template<class T>
class treeAVL
{
	TreeNode<T>* MainRoot;
	TreeNode<T>* CurrentNode;

	int TreeHeight;

	void rotationRR(TreeNode<T>*, TreeNode<T>*);
	void rotationLL(TreeNode<T>*, TreeNode<T>*);
	void rotationRL(TreeNode<T>*, TreeNode<T>*, TreeNode<T>*);
	void rotationLR(TreeNode<T>*, TreeNode<T>*, TreeNode<T>*);

	void rotation(TreeNode<T>*);

	void showPostOrder(TreeNode<T>*);
	void showInOrder(TreeNode<T>*);
	void showPreOrder(TreeNode<T>*);

public:
	treeAVL();
	~treeAVL();

	void addNode(const T&);
	void removeNode();

	void showTree() const;
};

template<class T>
treeAVL<T>::treeAVL()
{
	MainRoot = CurrentNode = nullptr;
}

template<class T>
treeAVL<T>::~treeAVL()
{

}

template<class T>
void treeAVL<T>::rotationRR(TreeNode<T>*A, TreeNode<T>*B)
{
	A->setRightNode(B->getLeftNode());
	B->setLeftNode(A);

	B->setLocalRootNode(A->getLocalRootNode());
	A->setLocalRootNode(B);
	if(A->getRightNode() != nullptr) A->getRightNode()->setLocalRootNode(A);
}

template<class T>
void treeAVL<T>::rotationLL(TreeNode<T>*A, TreeNode<T>*B)
{
	A->setLeftNode(B->getRightNode());
	B->setRightNode(A);

	B->setLocalRootNode(A->getLocalRootNode());
	A->setLocalRootNode(B);
	if(A->getLeftNode() != nullptr) A->getLeftNode()->setLocalRootNode(A);
}

template<class T>
void treeAVL<T>::rotationRL(TreeNode<T>*A, TreeNode<T>*B, TreeNode<T>*C)
{
	rotationLL(B, C);
	rotationRR(A, C);
}

template<class T>
void treeAVL<T>::rotationLR(TreeNode<T>*A, TreeNode<T>*B, TreeNode<T>*C)
{
	rotationRR(B, C);
	rotationLL(A, C);
}

template<class T>
void treeAVL<T>::rotation(TreeNode<T>*node)
{
	if (node == nullptr) return;

	bool rotated = false;
	bool rightOfRoot = false;

	TreeNode<T>* rootOfNode = node->getLocalRootNode();
	if (rootOfNode != nullptr)
		if (rootOfNode->getRightNode() == node) rightOfRoot = true;

	if (node->getNodeBalanceRange() == 2)
	{
		if(node->getLeftNode()->getNodeBalanceRange() == -1) rotationLR(node, node->getLeftNode(), node->getLeftNode()->getRightNode());
		else rotationLL(node, node->getLeftNode());

		rotated = true;
	}
	else if (node->getNodeBalanceRange() == -2)
	{
		if (node->getRightNode()->getNodeBalanceRange() == 1) rotationRL(node, node->getRightNode(), node->getRightNode()->getLeftNode());
		else rotationRR(node, node->getRightNode());

		rotated = true;
	}


	if (rotated)
	{
		if (rootOfNode == nullptr) MainRoot = node->getLocalRootNode();
		else if (rightOfRoot) rootOfNode->setRightNode(node->getLocalRootNode());
		else if (!rightOfRoot) rootOfNode->setLeftNode(node->getLocalRootNode());

		node->updateNodeInfo(MainRoot);
		showInOrder(MainRoot);
	}

	rotation(rootOfNode);
}

template<class T>
void treeAVL<T>::showInOrder(TreeNode<T> *node)
{
	if (node == nullptr) return;

	showInOrder(node->getLeftNode());

	for (int s = 0; s < 4; ++s)
	{
		for (int i = 0; i < node->getNodeLevel(); ++i) std::cout << "        ";

		//std::cout.width(15);

		switch (s)
		{
		case 0: std::cout << "| NodeID: "  << node->getNodeID();
			break;

		case 1: std::cout << "| NodeBR: "  << node->getNodeBalanceRange();
			break;

		case 2: std::cout << "| NodeVal: "  << node->getValue();
			break;

		case 3: std::cout << "| NodeAdrr: " << node;
			break;
		}

		std::cout << " |\n";
	}

	showInOrder(node->getRightNode());
}


template<class T>
void treeAVL<T>::addNode(const T &val)
{
	if (MainRoot == nullptr)
	{
		MainRoot = CurrentNode = new TreeNode<T>(val);
		return;
	}

	CurrentNode = MainRoot;

	do
	{
		if (val > CurrentNode->getValue())
		{
			if (CurrentNode->getRightNode() == nullptr)
			{
				CurrentNode->setRightNode(new TreeNode<T>(val, CurrentNode));
				break;
			}
			CurrentNode = CurrentNode->getRightNode();
		}
		else
		{
			if (CurrentNode->getLeftNode() == nullptr)
			{
				CurrentNode->setLeftNode(new TreeNode<T>(val, CurrentNode));
				break;
			}
			CurrentNode = CurrentNode->getLeftNode();
		}

	} while (1);

	MainRoot->updateNodeInfo(MainRoot);

	showInOrder(MainRoot);
	std::cout << "\n---------------------------\n";
	rotation(CurrentNode->getLocalRootNode());
	std::cout << "\n---------------------------\n";
}


template<class T>
void treeAVL<T>::removeNode()
{
	if (MainRoot == nullptr) return;

	CurrentNode = MainRoot;

	do
	{
		if (rand() % 2)
		{
			if (CurrentNode->getRightNode() == nullptr)
			{
				delete CurrentNode->getRightNode();
				break;
			}
			CurrentNode = CurrentNode->getRightNode();
			
		}
	} while (1);
}


template<class T>
void treeAVL<T>::showTree() const
{
	showInOrder(MainRoot);
}