#pragma once
#include <iostream>


template<class T>
class GraphAdjList
{
	int **AdjacencyMatrix;
	T* DataVector;

	int MatrixSize;
	int GraphSize;

	void resize();
	
public:
	GraphAdjList();
	~GraphAdjList();

	void addNode(const T&);
	void removeNode(const int&);
	void removeGraph();

	void showGraph() const;
};

template<class T>
GraphAdjList<T>::GraphAdjList()
{
	/* (1) Przygotownie przestrzeni poczatkowej grafu */
	MatrixSize = 8;

	AdjacencyMatrix = new int*[MatrixSize];
	for (int i = 0; i < MatrixSize; ++i)
		AdjacencyMatrix[i] = new int[MatrixSize];

	DataVector = new T[MatrixSize];

	// zerowanie wartosci

	for (int i = 0; i < MatrixSize; ++i)
	{
		DataVector[i] = 0;

		for (int j = 0; j < MatrixSize; ++j)
			AdjacencyMatrix[i][j] = 0;
	}

	GraphSize = 0;
}


template<class T>
GraphAdjList<T>::~GraphAdjList()
{
	removeGraph();
}


template<class T>
void GraphAdjList<T>::resize()
{
	double fillRange = GraphSize / (double)MatrixSize;
	int newSize = 0;

	if (fillRange > 0.8) newSize = 2 * MatrixSize;
	else if (fillRange < 0.45 && GraphSize > 8) newSize = MatrixSize / 2;

	if (!newSize) return;


	int **newMatrix;
	T *newVector;


	newMatrix = new int*[newSize];

	for (int i = 0; i < newSize; ++i)
		newMatrix[i] = new int[newSize];

	newVector = new T[newSize];


	for (int i = 0; i < newSize; ++i)
	{
		newVector[i] = 0;

		for (int j = 0; j < newSize; ++j)
			newMatrix[i][j] = 0;
	}


	for (int i = 0; i < MatrixSize; ++i)
	{
		newVector[i] = DataVector[i];

		for (int j = 0; j < MatrixSize; ++j)
			newMatrix[i][j] = AdjacencyMatrix[i][j];
	}

	
	removeGraph();


	DataVector = newVector;
	AdjacencyMatrix = newMatrix;
	MatrixSize = newSize;
}



template<class T>
void GraphAdjList<T>::addNode(const T&val)
{
	resize();

	GraphSize++;

	DataVector[GraphSize] = val;

	int *edgeNets = new int[GraphSize];

	for (int i = 0; i < GraphSize; ++i)
		edgeNets[i] = rand() % 5;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[GraphSize][i] = edgeNets[i];
		AdjacencyMatrix[i][GraphSize] = edgeNets[i];
	}
		
}


template<class T>
void GraphAdjList<T>::removeNode(const int&node)
{
	if (node > GraphSize) return;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[node][i] = 0;
		AdjacencyMatrix[i][node] = 0;
	}

	DataVector[node] = -1;
}


template<class T>
void GraphAdjList<T>::removeGraph()
{
	if (GraphSize)
	{
		delete[] DataVector;

		for (int i = 0; i < MatrixSize; ++i)
			delete[] AdjacencyMatrix[i];
		delete[] AdjacencyMatrix;
	}
}


template<class T>
void GraphAdjList<T>::showGraph() const
{
	std::cout << "----- Graf -----" << std::endl;
	std::cout << "DataVector: ";

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout.width(8);
		std::cout << DataVector[i];
	}

	std::cout << "\n\nAdjacencyMatrix:\n";

	//std::cout.width(8);
	std::cout << "[/]";

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout.width(6);
		std::cout << "[" << i << "]";
	}

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << i << "] ";

		for (int j = 0; j < GraphSize; ++j)
		{
			std::cout.width(8);
			std::cout << AdjacencyMatrix[i][j];
		}

		std::cout << std::endl;
	}
}