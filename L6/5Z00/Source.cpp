#include "GrapgAdjacencyMatrix.h"


int main()
{
	GraphAdjMatrix<int> graf;

	for (int i = 0; i < 10; ++i)
	{
		graf.addNode(i);
		graf.showGraph();
	}

	graf.removeNode(4);
	graf.showGraph();

	while (1);
	return 0;
}