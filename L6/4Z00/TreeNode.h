#pragma once

#include <iostream>
#include <string>

template<class T>
class TreeNode
{
	TreeNode* LeftNode;
	TreeNode* RightNode;
	TreeNode* LocalRootNode;

	int NodeHeight;				// wysokosc wezla - droga do lisci
	int NodeLevel;				// odleglosc od root'a
	int NodeDegree;				// ilosc polaczen w wezle
	int NodeBalanceRange;			// wspolczynnik rownowagi wezla

	static int GeneralNodeID;
	int NodeID;

	std::string NodeValuePath;
	std::string NodeIDPath;
	std::string NodeAddressPath;

	T Value;

	int UpdateNodeHeight(TreeNode*);
	int UpdateNodeLevel(TreeNode*);
	void UpdateNodeDegree();
	void UpdateNodeBalanceRange();
	
	std::string UpdateNodeValuePath(TreeNode*);
	std::string UpdateNodeIDPath(TreeNode*);
	std::string UpdateNodeAddressPath(TreeNode*);

	void UpdateNext(TreeNode*);

public:
	TreeNode(const T&, TreeNode *localRoot = nullptr);
	~TreeNode();

	void setValue(const T&);

	void setLeftNode(TreeNode*);
	void setRightNode(TreeNode*);
	void setLocalRootNode(TreeNode*);

	T getValue() const;

	TreeNode* getLeftNode() const;
	TreeNode* getRightNode() const;
	TreeNode* getLocalRootNode() const;

	int getNodeHeight() const;
	int getNodeLevel() const;
	int getNodeDegree() const;
	int getNodeBalanceRange() const;

	int getNodeID() const;

	std::string getNodeValuePath() const;
	std::string getNodeIDPath() const;
	std::string getNodeAddressePath() const;

	void updateNodeInfo(TreeNode*);
	
	void showNodeInfo() const;
};

template<class T>
int TreeNode<T>::GeneralNodeID = 0;

template<class T>
TreeNode<T>::TreeNode(const T &val, TreeNode<T> *localRoot)
{
	LocalRootNode = localRoot;
	LeftNode = RightNode = nullptr;

	NodeID = GeneralNodeID;
	GeneralNodeID++;
	Value = val;

	updateNodeInfo(this);
}

template<class T>
TreeNode<T>::~TreeNode()
{
	if (LeftNode != nullptr) delete LeftNode;
	if (RightNode != nullptr) delete RightNode;
}


template<class T>
int TreeNode<T>::UpdateNodeHeight(TreeNode *node)
{
	int Hl = 0, Hr = 0;

	if (node->getLeftNode() != nullptr) Hl = 1 + UpdateNodeHeight(node->getLeftNode());
	if (node->getRightNode() != nullptr) Hr = 1 + UpdateNodeHeight(node->getRightNode());

	if (Hl > Hr) return Hl;
	else return Hr;
}

template<class T>
int TreeNode<T>::UpdateNodeLevel(TreeNode *node)
{
	int L = 0;

	if (node->getLocalRootNode() != nullptr) L = 1 + UpdateNodeLevel(node->getLocalRootNode());

	return L;
}

template<class T>
void TreeNode<T>::UpdateNodeDegree()
{
	int D = 0;

	if (LocalRootNode != nullptr) ++D;
	if (LeftNode != nullptr) ++D;
	if (LeftNode != nullptr) ++D;

	NodeDegree = D;
}

template<class T>
void TreeNode<T>::UpdateNodeBalanceRange()
{
	int L = 0, R = 0;

	if (LeftNode != nullptr)
	{
		//LeftNode->updateNodeInfo();
		L = 1 + LeftNode->getNodeHeight();
	}
	if (RightNode != nullptr)
	{
		//RightNode->updateNodeInfo();
		R = 1 + RightNode->getNodeHeight();
	}

	NodeBalanceRange = L - R;
}

template<class T>
std::string TreeNode<T>::UpdateNodeValuePath(TreeNode<T> *node)
{
	std::string S = "";

	if (node->getLocalRootNode() != nullptr)
	{
		S = UpdateNodeValuePath(node->getLocalRootNode());
		S += '/';
	}

	S += std::to_string(node->getValue());

	return S;
}

template<class T>
std::string TreeNode<T>::UpdateNodeIDPath(TreeNode<T> *node)
{
	std::string S = "";

	if (node->getLocalRootNode() != nullptr)
	{
		S = UpdateNodeIDPath(node->getLocalRootNode());
		S += '/';
	}

	S += std::to_string(node->getNodeID());

	return S;
}

template<class T>
std::string TreeNode<T>::UpdateNodeAddressPath(TreeNode<T> *node)
{
	std::string S = "";

	if (node->getLocalRootNode() != nullptr)
	{
		S = UpdateNodeAddressPath(node->getLocalRootNode());
		S += '/';
	}

	S += std::to_string((int)node);

	return S;
}

template<class T>
void TreeNode<T>::UpdateNext(TreeNode<T> *enterNode)
{
	if (LocalRootNode != nullptr && LocalRootNode != enterNode) LocalRootNode->updateNodeInfo(this);
	if (RightNode != nullptr && RightNode != enterNode) RightNode->updateNodeInfo(this);
	if (LeftNode != nullptr && LeftNode != enterNode) LeftNode->updateNodeInfo(this);
}


template<class T>
void TreeNode<T>::setValue(const T&val)
{
	Value = val;
	updateNodeInfo(this);
}

template<class T>
void TreeNode<T>::setLeftNode(TreeNode<T>*node)
{
	LeftNode = node;
	updateNodeInfo(this);
}

template<class T>
void TreeNode<T>::setRightNode(TreeNode<T>*node)
{
	RightNode = node;
	updateNodeInfo(this);
}

template<class T>
void TreeNode<T>::setLocalRootNode(TreeNode<T>*node)
{
	LocalRootNode = node;
	updateNodeInfo(this);
}


template<class T>
T TreeNode<T>::getValue() const
{
	return Value;
}

template<class T>
TreeNode<T>* TreeNode<T>::getLeftNode() const
{
	return LeftNode;
}

template<class T>
TreeNode<T>* TreeNode<T>::getRightNode() const
{
	return RightNode;
}

template<class T>
TreeNode<T>* TreeNode<T>::getLocalRootNode() const
{
	return LocalRootNode;
}

template<class T>
int TreeNode<T>::getNodeHeight() const
{
	return NodeHeight;
}

template<class T>
int TreeNode<T>::getNodeLevel() const
{
	return NodeLevel;
}

template<class T>
int TreeNode<T>::getNodeDegree() const
{
	return NodeDegree;
}

template<class T>
int TreeNode<T>::getNodeBalanceRange() const
{
	return NodeBalanceRange;
}

template<class T>
int TreeNode<T>::getNodeID() const
{
	return NodeID;
}

template<class T>
std::string TreeNode<T>::getNodeValuePath() const
{
	return NodeValuePath;
}

template<class T>
std::string TreeNode<T>::getNodeIDPath() const
{
	return NodeIDPath;
}

template<class T>
std::string TreeNode<T>::getNodeAddressePath() const
{
	return NodeAddressPath;
}



template<class T>
void TreeNode<T>::updateNodeInfo(TreeNode<T> *enterNode)
{
	UpdateNext(enterNode);

	NodeHeight = UpdateNodeHeight(this);
	NodeLevel = UpdateNodeLevel(this);
	UpdateNodeDegree();
	UpdateNodeBalanceRange();

	NodeValuePath = UpdateNodeValuePath(this);
	NodeIDPath = UpdateNodeIDPath(this);
	NodeAddressPath = UpdateNodeAddressPath(this);
}


template<class T>
void TreeNode<T>::showNodeInfo() const
{
	std::cout << " ---- NodeInfo ---- "
		<< "\nNodeHeight: " << NodeHeight
		<< "\nNodeLevel: " << NodeLevel
		<< "\nNodeDegree: " << NodeDegree
		<< "\nNodeBalanceRange: " << NodeBalanceRange
		<< "\n\nNodeID: " << NodeID
		<< "\nNodeAdrress: " << this
		<< "\nLeftNode: " << LeftNode
		<< "\nRightNode: " << RightNode 
		<< "\n\nNodeValuePath: " << NodeValuePath
		<< "\nNodeIDPath: " << NodeIDPath
		<< "\nNodeAddressPath: " << NodeAddressPath
		<< std::endl << std::endl;
}
