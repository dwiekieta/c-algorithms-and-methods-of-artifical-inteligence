#include "treeAVL.h"

int main()
{
	TreeNode<int>* node = new TreeNode<int>(1);
	node->showNodeInfo();

	node->setLeftNode(new TreeNode<int>(13, node));
	node->getLeftNode()->showNodeInfo();
	node->showNodeInfo();

	while (1);
	return 0;
}