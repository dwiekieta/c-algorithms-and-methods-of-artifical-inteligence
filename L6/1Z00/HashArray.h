#pragma once

template<class T>
class LinearHashArray
{
	T* Array;
	int ArraySize;

	bool PrimaryTest(const int&);
	int NearstPrimary(const int&p, const bool&up = true);
	int HashFunction(const T&);

public:
	LinearHashArray();
	~LinearHashArray();

	void setArray();
	void addElement(const T&);
	int findElement(const T&);
	void removeElement(const T&);

	void show() const;

};

