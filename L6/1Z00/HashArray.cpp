#include "HashArray.h"
#include <math.h>
#include <iostream>


template<class T>
LinearHashArray<T>::LinearHashArray()
{
	Array = nullptr;
	ArraySize = 0;
}

template<class T>
LinearHashArray<T>::~LinearHashArray()
{
	if (ArraySize) delete[] Array;
}


template<class T>
bool LinearHashArray<T>::PrimaryTest(const int&n)
{
	if (n < 2) return false;

	if (!(n % 2))
	{
		if (n == 2) return true;
		else return false;
	}

	for (int i = 3; i < sqrt(n); i+=2)
		if (!(n%i)) return false;

	return true;
}


template<class T>
int LinearHashArray<T>::NearstPrimary(const int&p, const bool&up)
{
	int i = p;

	for (; !PrimaryTest(i) && i > 1; (up) ? ++i : --i);

	return i;
}

template<class T>
int LinearHashArray<T>::HashFunction(const T&arg)
{
	if (!ArraySize) return -1;

	if (arg < 0)
	{
		int a = arg * -1;
		return a%ArraySize;
	}

	return arg%ArraySize;
}



template<class T>
void LinearHashArray<T>::setArray()
{
	bool m;

	std::cout << "Wprowadz rozmiar tablicy: ";
	std::cin >> ArraySize;
	std::cout << std::endl << "min/max [0/1]: ";
	std::cin >> m;

	ArraySize = NearstPrimary(ArraySize, m);

	Array = new T[ArraySize];

	*Array = 1;
	for (int i = 1; i < ArraySize; ++i)
		Array[i] = 0;
}


template<class T>
void LinearHashArray<T>::addElement(const T&el)
{
	if (!ArraySize) return;

	if (!el)
	{
		*Array = 0;
		return;
	}

	int index = HashFunction(el);
	int i = index;

	for (;Array[i];)
	{
		if (Array[i] == el) return;

		i += index;
		if (i > ArraySize - 1) i = i - ArraySize;
		if (!i) return;
	}

	Array[i] = el;
}


template<class T>
int LinearHashArray<T>::findElement(const T&el)
{
	if (!ArraySize)return -1;

	int index = HashFunction(el);
	int i = index;

	for (; Array[i];)
	{
		if (Array[i] == el) return i;

		i += index;
		if (i > ArraySize - 1) i = i - ArraySize;
		if (!i) return -2;
	}

	return -2;
}


template<class T>
void LinearHashArray<T>::removeElement(const T&el)
{
	if (!ArraySize) return;

	int index = findElement(el);

	if (index < 0) return;

	if (!index) *Array = 1;
	else Array[index] = 0;
}

template<class T>
void LinearHashArray<T>::show() const
{
	if (!ArraySize) return;

	for (int i = 0; i < ArraySize; ++i)
		std::cout << "[" << i << "] " << Array[i] << std::endl;
}

template class LinearHashArray<int>;