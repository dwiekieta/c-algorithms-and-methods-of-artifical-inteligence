#pragma once

template<class T>
class DoubleHashArray
{
	T* Array;
	int ArraySize;
	int SecondCondition;

	bool PrimaryTest(const int&);
	int NearstPrimary(const int&p, const bool&up = true);
	int HashFunction(const T&);
	int SecondHashFunction(const int&, const T&);

public:
	DoubleHashArray();
	~DoubleHashArray();

	void setArray();
	void addElement(const T&);
	int findElement(const T&);
	void removeElement(const T&);

	void show() const;

};

