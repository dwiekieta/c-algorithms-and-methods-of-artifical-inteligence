In this repository are eight solutions referended to practical classes of programming at WUST 
	faculty of Electronics, field of study Control Engineering and Robotics

	L1 - Basic function in C++
		- creating dynamic arrays
		- creating menus
		- recursive functions
	L2 - Lists and queues
		- own implememntations
	L3 - Stacks and queues from STL
		- checks time consumptions of standard implememntation
		- solves Hanoi problem
	L4 - Priority queues and trees
		- own implementations
	L5 - Sorting
		- bubles
		- by inserts
		- by heaps
		- Shell's
		- by merges
		- quick
		- introspectives
	L6 - Template classes, hash tables, double hash tables, AVL Tree, Graph with adjacency matrix
	L7 - Shorter path in the graphs
		- Kruskal's algorithms
		- Prim's algorithms
		- Dijkstra's algorithms
	L8 - The game - Sea Battle
		- manual and auto game
