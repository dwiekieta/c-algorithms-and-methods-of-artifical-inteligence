#include "MyMath.h"



MyMath::MyMath()
{
}


MyMath::~MyMath()
{
}


long MyMath::Potega(int x, int p)
{
	if (!p) return 1;

	long t = x;
	for (int i = 1; i < p; ++i) t *= x;
	return t;
}


long MyMath::Silnia(int z)
{
	if (!z) return 1;

	long t = 1;
	for (int i = 1; i <= z; ++i) t = t * i;
	return t;
}