#include <iostream>
#include <string>

#include"MyPermutation.h"

int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyPermutation perm("kajak");
	
	timeStart = clock();
	perm.permutuj();
	timeStop = clock();
	
	perm.showPermutations();
	std::cout << "Czas wykonania permutacji: " << difftime(timeStop, timeStart) << " [ms]\n"; 

	timeStart = clock();
	perm.showPalindroms();
	timeStop = clock();
	std::cout << "Czas wykonania przetworzenia palindromow: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (1);
	return 0;
}