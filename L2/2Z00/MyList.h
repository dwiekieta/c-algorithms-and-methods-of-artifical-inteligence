#pragma once

template<class T>
class MyList
{
	struct MyListItem
	{
		T ItemValue;
		MyListItem *NextItem;
	};

	MyListItem *FirstItem;
	MyListItem *LastItem;
	MyListItem *CurrentItem;

public:
	MyList();
	~MyList();

	void setValue(T);
	T getValue() const;

	void addElement(T);
	void removeElement();
	void removeList();

	void showList();
};


template<class T>
MyList<T>::MyList()
{
	CurrentItem = LastItem = FirstItem = nullptr;
}


template<class T>
MyList<T>::~MyList()
{
	removeList();
}


template<class T>
void MyList<T>::setValue(T val)
{
	CurrentItem->ItemValue = val;
}


template<class T>
T MyList<T>::getValue() const
{
	return CurrentItem->ItemValue;
}

template<class T>
void MyList<T>::addElement(T val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyListItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		CurrentItem = LastItem = FirstItem;
		return;
	}
	
	MyListItem *temp = CurrentItem->NextItem;;

	CurrentItem->NextItem = new MyListItem;
	CurrentItem = CurrentItem->NextItem;

	CurrentItem->ItemValue = val;
	CurrentItem->NextItem = temp;

	LastItem = FirstItem;
	while (LastItem->NextItem != nullptr) LastItem = LastItem->NextItem;

}


template<class T>
void MyList<T>::removeElement()
{
	if (FirstItem == nullptr) return;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		CurrentItem = LastItem = nullptr;
		return;
	}

	MyListItem *temp = FirstItem;
	MyListItem *rm = CurrentItem;

	while (temp->NextItem != CurrentItem) temp = temp->NextItem;

	temp->NextItem = CurrentItem->NextItem;
	CurrentItem = temp;

	delete rm;

	LastItem = FirstItem;
	while (LastItem->NextItem != nullptr) LastItem = LastItem->NextItem;
}


template<class T>
void MyList<T>::removeList()
{
	while (FirstItem != nullptr)
	{
		CurrentItem = LastItem;
		removeElement();
	}
}


template<class T>
void MyList<T>::showList()
{
	std::cout << "Lista: " << std::endl;

	int el = 0;
	MyListItem *temp = FirstItem;

	do
	{
		std::cout << "[ " << el << " ] " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}
