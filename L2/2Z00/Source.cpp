#include <iostream>

#include "MyList.h"

int main()
{
	MyList<int> *lista = new MyList<int>;

	lista->addElement(1);
	lista->addElement(2);
	lista->removeElement();
	lista->addElement(3);
	lista->addElement(4);
	
	lista->showList();

	while (true);


	return 0;
}