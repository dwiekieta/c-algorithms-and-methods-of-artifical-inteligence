#include <iostream>
#include <ctime>

#include "MyList.h"

int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyList<int> *lista = new MyList<int>;

	timeStart = clock();

	for(int i=0;i<1000;++i)
		lista->addElement(i);

	for (int i = 0; i < 30; i++)
		lista->removeElement(i+935);

	for (int i = 0; i < 7000; ++i)
		lista->addElement(i + 1000);

	timeStop = clock();

	lista->showList();
	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (true);


	return 0;
}