#include "MyPalindrom.h"



MyPalindrom::MyPalindrom()
{
	Deque = new MyDeque<char>;
}


MyPalindrom::~MyPalindrom()
{
	delete Deque;
}


void MyPalindrom::ReadString(std::string s)
{
	for (int i = 0; i < s.length(); ++i)
		Deque->addEndElement(s[i]);

	Word = s;
}


bool MyPalindrom::CheckPalindrom()
{
	for (int i = 0; i <(Word.length() / 2); ++i)
	{
		if (Deque->getFirst() != Deque->getLast()) return false;

		Deque->removeEndElement();
		Deque->removeFrontElement();
	}

	return true;
}


bool MyPalindrom::palindrom(std::string s)
{
	Deque->removeDeqeue();

	ReadString(s);

	return(CheckPalindrom());
}