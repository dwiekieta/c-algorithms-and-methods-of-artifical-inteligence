#pragma once

#include<string>

#include"MyDeque.h"

class MyPalindrom
{
	MyDeque<char> *Deque;
	std::string Word;

	bool IsPalindrom;

	void ReadString(std::string);
	bool CheckPalindrom();

public:
	MyPalindrom();
	~MyPalindrom();

	bool palindrom(std::string);
};

