#pragma once

template<class T>
class MyDeque
{
	struct MyDequeItem
	{
		T ItemValue;
		MyDequeItem *NextItem;
	};

	MyDequeItem *FirstItem;
	MyDequeItem *LastItem;

public:
	MyDeque();
	~MyDeque();

	void addFrontElement(const T&);
	void addEndElement(const T&);
	void removeFrontElement();
	void removeEndElement();
	void removeDeqeue();

	void showDeqeue();

	T getFirst();
	T getLast();

};


template<class T>
MyDeque<T>::MyDeque()
{
	FirstItem = LastItem = nullptr;
}


template<class T>
MyDeque<T>::~MyDeque()
{
	removeDeqeue();
}


template<class T>
void MyDeque<T>::addFrontElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyDequeItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	MyDequeItem *temp = new MyDequeItem;
	temp->NextItem = FirstItem;

	FirstItem = temp;
	FirstItem->ItemValue = val;

}


template<class T>
void MyDeque<T>::addEndElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyDequeItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	LastItem->NextItem = new MyDequeItem;
	LastItem = LastItem->NextItem;

	LastItem->ItemValue = val;
	LastItem->NextItem = nullptr;
}


template<class T>
void MyDeque<T>::removeFrontElement()
{
	if (FirstItem == nullptr) return;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return;
	}

	MyDequeItem *temp = FirstItem;
	FirstItem = FirstItem->NextItem;

	delete temp;
}


template<class T>
void MyDeque<T>::removeEndElement()
{
	if (LastItem == nullptr) return;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return;
	}

	MyDequeItem *temp = FirstItem;
	while (temp->NextItem != LastItem)
		temp = temp->NextItem;
	
	delete LastItem;

	LastItem = temp;
}



template<class T>
void MyDeque<T>::removeDeqeue()
{
	while (FirstItem != nullptr)
		removeFrontElement();
}


template<class T>
void MyDeque<T>::showDeqeue()
{
	std::cout << "Kolejka z dwoma koncami: " << std::endl;

	int el = 0;
	MyDequeItem *temp = FirstItem;

	do
	{
		std::cout << "[ " << el << " ] " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}


template<class T>
T MyDeque<T>::getFirst()
{
	return FirstItem->ItemValue;
}


template<class T>
T MyDeque<T>::getLast()
{
	return LastItem->ItemValue;
}