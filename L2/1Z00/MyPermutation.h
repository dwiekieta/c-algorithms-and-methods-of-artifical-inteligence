#pragma once

#include <iostream>

class MyPermutation
{
	std::string Word;
	int WordLength;
	long WordPermutations;
	long WordPalindroms;

	std::string *WordPermutationsArray;
	std::string *WordPalindromArray;

	bool Exist;
	long PermutationArrayPointer;
	long PalindromArrayPointer;

	void przesun(int index, int ile);

	bool Palindrom(const std::string &, int);
	void setPalindromArray();

	bool porownajString(const std::string&, const std::string&) const;
	void kopiujString(std::string&, const std::string&);

	void deleteWord();
	

public:
	MyPermutation(std::string s = "");
	~MyPermutation();

	void setWord(const std::string &);
	std::string getWord() const;

	void permutuj(int index = 0);
	void showPermutations() const;
	void showPalindroms();
};

