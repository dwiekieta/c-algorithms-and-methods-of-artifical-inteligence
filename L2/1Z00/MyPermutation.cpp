#include "MyPermutation.h"
#include "MyMath.h"

#include <string>


MyPermutation::MyPermutation(std::string s)
{
	setWord(s.c_str());
}


MyPermutation::~MyPermutation()
{
	deleteWord();
}

void MyPermutation::przesun(int index, int ile)
{
	char t;

	for (int q = 0; q < ile; ++q)
	{
		t = Word[index];

		for (int i = index; i < WordLength - 1; ++i)
		{
			Word[i] = Word[i + 1];
		}

		Word[WordLength - 1] = t;
	}
}


bool MyPermutation::Palindrom(const std::string &s, int p)
{
	bool isP = true;
	int l = s.length();


	if (p < (l / 2))
		(s[p] == s[l - p - 1]) ? isP = Palindrom(s.c_str(), p + 1) : isP = false;

	return isP;
}


void MyPermutation::deleteWord()
{
	if (!Exist) return;

	delete[] WordPermutationsArray;
	delete[] WordPalindromArray;
}



void MyPermutation::permutuj(int index)
{
	for (int i = 0; i < WordLength - index; ++i)
	{
		przesun(index, 1);
		if (i < WordLength - index - 1)
		{
			if (Palindrom(Word.c_str(), 0))
			{
				WordPalindromArray[PalindromArrayPointer] = Word;
				PalindromArrayPointer++;
			}
			WordPermutationsArray[PermutationArrayPointer] = Word;
			(PermutationArrayPointer < WordPermutations) ? PermutationArrayPointer++ : PermutationArrayPointer = 0;
		}
		if (WordLength - index > 2) permutuj(index + 1);
	}
}


void MyPermutation::setWord(const std::string& s)
{
	if (s == "")
	{
		Exist = false;
		return;
	}

	deleteWord();

	MyMath mm;

	Word = s;
	WordLength = s.length();
	WordPermutations = mm.Silnia(WordLength);
	WordPalindroms = WordPermutations;

	WordPermutationsArray = new std::string[WordPalindroms];
	WordPalindromArray = new std::string[WordPermutations];

	WordPermutationsArray[WordPermutations - 1] = s;

	Exist = true;
	PermutationArrayPointer = 0;
	PalindromArrayPointer = 0;
}


std::string MyPermutation::getWord() const
{
	return Word;
}


void MyPermutation::showPermutations() const
{
	std::cout << "Permutacje slowa: \"" << Word << "\", ilosc permutacji: " << WordPermutations << std::endl;

	long k = WordPermutations / 6;
	
	for (long i = 0; i < k; ++i)
	{
		std::cout << "[" << i << "] " << WordPermutationsArray[i];
		(i + k < WordPermutations) ? std::cout << "     [" << i + k << "] " << WordPermutationsArray[i + k] : std::cout << std::endl;
		(i + (2 * k) < WordPermutations) ? std::cout << "     [" << i + (2 * k) << "] " << WordPermutationsArray[i + (2 * k)] : std::cout << std::endl;
		(i + (3 * k) < WordPermutations) ? std::cout << "     [" << i + (3 * k) << "] " << WordPermutationsArray[i + (3 * k)] : std::cout << std::endl;
		(i + (4 * k) < WordPermutations) ? std::cout << "     [" << i + (4 * k) << "] " << WordPermutationsArray[i + (4 * k)] : std::cout << std::endl;
		if (i + (5 * k) < WordPermutations) std::cout << "     [" << i + (5 * k) << "] " << WordPermutationsArray[i + (5 * k)] << std::endl;
	}

	if (!k)
		for (long i = 0; i < WordPermutations; ++i) std::cout << "[" << i << "] " << WordPermutationsArray[i] << std::endl;

}


void MyPermutation::setPalindromArray()
{
	long size = WordPalindroms;


	// wyznaczenie ilosci palindromow
	for (long i = 0; i < WordPalindroms; ++i)
		if (WordPalindromArray[i] == "")
		{
			size = i + 1;
			break;
		}

	std::string *temp = new std::string[size];


	// kopiowanie palindromow
	for (long i = 0; i < size; ++i)
		kopiujString(temp[i], WordPalindromArray[i]);


	delete[] WordPalindromArray;


	// usuwanie tych samych palindromow
	for (long i = 0; i < size - 1; ++i)
		for (long j = i + 1; j < size; ++j)
			if (porownajString(temp[i].c_str(), temp[j].c_str())) temp[j] = "";

	
	// kopiowanie palindromow
	long tempSize = size;
	size = 0;
	for (long i = 0; i < tempSize; ++i)
		if (temp[i] != "")
			size++;


	WordPalindromArray = new std::string[size];

	long tempW = 0;
	for (long i = 0; i < tempSize; ++i)
		if (temp[i] != "")
		{
			kopiujString(WordPalindromArray[tempW],temp[i]);
			tempW++;
		}

	delete[] temp;

	WordPalindroms = size;
}


bool MyPermutation::porownajString(const std::string &a, const std::string &b) const
{
	if (a.length() != b.length()) return false;

	for (int i = 0; i < a.length(); ++i)
		if (a[i] != b[i]) return false;

	return true;
}


void MyPermutation::kopiujString(std::string &a, const std::string& b)
{
	for (int i = 0; i < b.length(); ++i)
		a += b[i];
}


void MyPermutation::showPalindroms()
{
	setPalindromArray();

	std::cout << "Palindromy w permutacji slowa: \"" << Word << "\", ilosc permutacji: " << WordPalindroms << std::endl;

	long k = WordPalindroms / 6;

	for (long i = 0; i < k; ++i)
	{
		std::cout << "[" << i << "] " << WordPalindromArray[i];
		(i + k < WordPalindroms) ? std::cout << "     [" << i + k << "] " << WordPalindromArray[i + k] : std::cout << std::endl;
		(i + (2 * k) < WordPalindroms) ? std::cout << "     [" << i + (2 * k) << "] " << WordPalindromArray[i + (2 * k)] : std::cout << std::endl;
		(i + (3 * k) < WordPalindroms) ? std::cout << "     [" << i + (3 * k) << "] " << WordPalindromArray[i + (3 * k)] : std::cout << std::endl;
		(i + (4 * k) < WordPalindroms) ? std::cout << "     [" << i + (4 * k) << "] " << WordPalindromArray[i + (4 * k)] : std::cout << std::endl;
		if (i + (5 * k) < WordPalindroms) std::cout << "     [" << i + (5 * k) << "] " << WordPalindromArray[i + (5 * k)] << std::endl;
	}

	if (!k)
		for (long i = 0; i < WordPalindroms; ++i) std::cout << "[" << i << "] " << WordPalindromArray[i] << std::endl;
}