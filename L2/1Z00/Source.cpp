#include <iostream>
#include <string>

#include"MyPermutation.h"

int main()
{

	MyPermutation perm("123454321");
	perm.permutuj();
	perm.showPermutations();
	perm.showPalindroms();

	while (1);
	return 0;
}