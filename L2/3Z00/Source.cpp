#include <iostream>
#include <ctime>

#include"MyQueue.h"


int main()
{
	clock_t timeStart;
	clock_t timeStop;

	MyQueue<int> *kolejka = new MyQueue<int>;

	timeStart = clock();

	for(int i=0;i<10000;++i)
		kolejka->addElement(i);

	for (int i = 0; i<6000; ++i)
		kolejka->removeElement();

	for (int i = 0; i<3000; ++i)
		kolejka->addElement(i+10000);

	timeStop = clock();

	kolejka->showQueue();
	std::cout << "Czas wykonania: " << difftime(timeStop, timeStart) << " [ms]\n";

	while (true);

	return 0;
}