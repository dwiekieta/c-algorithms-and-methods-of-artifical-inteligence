#pragma once

template<class T>
class MyQueue
{
	struct MyQueueItem
	{
		T ItemValue;
		MyQueueItem *NextItem;
	};

	MyQueueItem *FirstItem;
	MyQueueItem *LastItem;

public:
	MyQueue();
	~MyQueue();
	
	void addElement(const T&);
	void removeElement();
	void removeQueue();

	void showQueue();

};


template<class T>
MyQueue<T>::MyQueue()
{
	FirstItem = LastItem = nullptr;
}


template<class T>
MyQueue<T>::~MyQueue()
{
	removeQueue();
}


template<class T>
void MyQueue<T>::addElement(const T& val)
{
	if (FirstItem == nullptr)
	{
		FirstItem = new MyQueueItem;

		FirstItem->ItemValue = val;
		FirstItem->NextItem = nullptr;

		LastItem = FirstItem;
		return;
	}

	LastItem->NextItem = new MyQueueItem;
	LastItem = LastItem->NextItem;

	LastItem->ItemValue = val;
	LastItem->NextItem = nullptr;
}


template<class T>
void MyQueue<T>::removeElement()
{
	if (FirstItem == nullptr) return;

	if (FirstItem->NextItem == nullptr)
	{
		delete FirstItem;
		LastItem = nullptr;

		return;
	}

	MyQueueItem *temp = FirstItem;

	FirstItem = FirstItem->NextItem;
	delete temp;
}


template<class T>
void MyQueue<T>::removeQueue()
{
	while (FirstItem != nullptr)
		removeElement();
}


template<class T>
void MyQueue<T>::showQueue()
{
	std::cout << "Kolejka: " << std::endl;

	int el = 0;
	MyQueueItem *temp = FirstItem;

	do
	{
		std::cout << "[ " << el << " ] " << temp->ItemValue << std::endl;

		temp = temp->NextItem;
		el++;
	} while (temp != nullptr);
}