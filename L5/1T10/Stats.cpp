#include "Stats.h"



ElementStats::ElementStats(const std::string&title)
{
	Title = title;
	Time = new MyQeueArray<double>;
	Mean = 0;
}


ElementStats::~ElementStats()
{
	delete Time;
}

void ElementStats::addTime(const double&time)
{
	Time->addElement(time);
}

double ElementStats::countMean()
{
	if (Time->sizeQueue() == 0) return 0;

	Mean = Time->operator[](0);

	for (int i = 1; i < Time->sizeQueue() - 1; ++i)
	{
		Mean += Time->operator[](i);
		Mean /= 2;
	}

	return Mean;
}

void ElementStats::showStats(const bool&showTime) const
{
	std::cout << Title << std::endl;
	std::cout << "Czas sortowania: " << Mean << "[ms] " << Mean / 1000 << "[s], " << Mean / 1000 / 60 << "[min], " << Mean / 1000 / 60 / 60 << "[h] \n\n";


	if (showTime)
	{
		std::cout << "Miedzyczasy:" << std::endl;
		Time->showQueue();
	}
}