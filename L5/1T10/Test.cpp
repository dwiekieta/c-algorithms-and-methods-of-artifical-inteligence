#include "Test.h"


Test::Test(const std::string&fn)
{
	file.open(fn, std::ios::out || std::ios::app);
	//file.clear();

	FileName = fn;
	Repeats = 1;
	Types = 6;
	NoOfTests = 0;
}


Test::~Test()
{
	file.close();
}

void Test::addArrSize(const int&size)
{
	ArrSize.addElement(size);
	NoOfTests = ArrSize.sizeQueue() * FillRange.sizeQueue() * Repeats * Types;
}

void Test::addFillRange(const double&fill)
{
	FillRange.addElement(fill);
	NoOfTests = ArrSize.sizeQueue() * FillRange.sizeQueue() * Types * Repeats;
}

void Test::addRepeats(const int&rep)
{
	Repeats = rep;
	NoOfTests = ArrSize.sizeQueue() * FillRange.sizeQueue() * Types * Repeats;
}

void Test::showProperites() const
{
	std::cout << "Nazwa pliku z danymi: " << FileName << '\n';
	if (NoOfTests)
	{
		std::cout << "Ilosc testow: " << NoOfTests << '\n'
			<< "\tIlosc powtorzen: " << Repeats << '\n'
			<< "\tRodzaje wypelnien: ";
		FillRange.showQueue(true);
		std::cout << "\tWielkosci tablic: ";
		ArrSize.showQueue(true);
	}

	std::cout << std::endl;
	std::cout << std::endl;
}

void Test::showStats() const
{
	std::cout << "Statystyki sortowania \n";

	for (int i = 0; i < Statystyki.sizeQueue(); ++i)
		Statystyki[i]->showStats();
}


void Test::runTest()
{
	int t = 0;
	std::string typ;

	for (int e = 0; e < ArrSize.sizeQueue(); ++e)
	{
		int elementy = ArrSize[e];

		for (int w = 0; w < FillRange.sizeQueue(); ++w)
		{
			double wypelnienie = FillRange[w];

			for (int s = 0; s < Types; ++s)
			{
				switch (s)
				{
				case 0: typ = "Sortowanie przez kopcowanie";
					break;
				case 1: typ = "Sortowanie bombelkowe";
					break;
				case 2: typ = "Sortowanie przez scalanie";
					break;
				case 3: typ = "Sortowanie Shella";
					break;
				case 4: typ = "Sortowanie szybkie";
					break;
				case 5: typ = "Sortowanie introspektywne";
					break;
				}

				typ += " E";
				typ += std::to_string(elementy);
				typ += " F";
				typ += std::to_string(wypelnienie);

				double time = 0;

				Statystyka = new ElementStats(typ);

				for (int p = 0; p < Repeats; ++p)
				{
					std::cout << "Test " << t++ << " z " << NoOfTests << " [" << (double)(t - 1)/NoOfTests*100 << "%] - " << typ << std::endl;
					std::cout << "El.: " << elementy << ", Wyp.: " << wypelnienie << "%, Powt.: " << p << std::endl;
				
					//(0) wyczyszczenie tablicy
					Dane.removeData();

					//(1) - wypelnienie tablicy uporzadkowanym ciagiem 
					//(1.1) - okreslenie ilosci znakow
					int ilosc = elementy * wypelnienie / 100;

					//(1.2) - okreslenie kierunku wypelnienia
					bool up = true;
					if (ilosc < 0)
					{
						ilosc *= -1;
						up = false;
					}

					//(1.3) - wypelnienie uporzadkowne
					if (up)
						for (int i = 0; i < ilosc; ++i)
							Dane.addElement(i);
					else
						for (int i = ilosc; i; --i)
							Dane.addElement(i);

					//(2) - wypelnienie losowe
					//(2.1) - ustawienie rand()
					srand(clock());

					//(2.2) - wypelnienie losowe
					for (int i = ilosc; i < elementy; ++i)
						Dane.addElement(rand() + ilosc);

					//(3) - sortowanie
					switch (s)
					{
					case 0: time += Dane.heapSort();
						break;
					case 1: time += Dane.bubleSort();
						break;
					case 2: time += Dane.mergeSort();
						break;
					case 3: time += Dane.shellSort();
						break;
					case 4: time += Dane.quickSort();
						break;
					case 5: time += Dane.introSort();
						break;
					}

					if (p) time /= 2;
					Statystyka->addTime(time);
					if (s == 5) break;
				}

				Statystyka->countMean();
				Statystyki.addElement(Statystyka);


				file << typ << '\n';
				file << "czas sortowania: " << time << "[ms], " << time/1000 << "[s], " << time/1000/60 << "[min], " << time/1000/60/60 << "[h] \n\n";
				
				std::cout << "czas sortowania: " << time << "[ms], " << time / 1000 << "[s], " << time / 1000 / 60 << "[min], " << time / 1000 / 60 / 60 << "[h] \n\n";

			
			}
		}
	}
}