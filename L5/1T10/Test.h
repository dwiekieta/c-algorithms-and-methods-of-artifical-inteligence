#pragma once

#include <fstream>
#include <string>
#include <iostream>

#include "MySort.h"
#include "Stats.h"


class Test
{
	std::fstream file;
	std::string FileName;

	MySort<int> Dane;
	MyQeueArray<ElementStats*> Statystyki;
	ElementStats *Statystyka;


	MyQeueArray<int> ArrSize;
	MyQeueArray<double> FillRange;
	int Repeats;
	int Types;
	
	int NoOfTests;

public:
	Test(const std::string&fn = "temp.text");
	~Test();

	void addArrSize(const int&);
	void addFillRange(const double&);
	void addRepeats(const int&);

	void showProperites() const;
	void showStats() const;

	void runTest();
};

