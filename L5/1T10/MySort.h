#pragma once

#include <ctime>

#include "MyQeueArray.h"


struct SortStats
{
	bool BubleSort;
	double BubleSortTime;
};


template<class T>
class MySort
{
	MyQeueArray<T> OrginalData;
	MyQeueArray<T> SortData;

	SortStats Stats;

	void resetStats();

	void showHeap(const int &pos = 0);

	void heapSplit(const int&index, const bool&up, const bool&show);
	void mergeSplit(const int&begin, const int&end, const bool&up, const bool&show);
	void quickSplit(const int&begin, const int&end, const bool&up, const bool&show);
	void introSplit(const int&deep, const int&logN, const int&begin, const int&end, const bool&up, const bool&show);

public:
	MySort();
	~MySort();

	void setData(const MyQeueArray<T>&);
	void addElement(const T&);

	void showData() const;
	void showSort() const;

	double insertSort(const T&element, const bool&up = true, const bool &show = false);
	double heapSort(const T&element, const bool&up = true, const bool &show = false);

	double heapSort(const bool&up = true, const bool &show = false);
	double bubleSort(const bool&up = true, const bool &show = false);
	double mergeSort(const bool&up = true, const bool &show = false);
	double shellSort(const bool&up = true, const bool &show = false);
	double quickSort(const bool&up = true, const bool &show = false);
	double introSort(const bool&up = true, const bool &show = false);

	void removeData();
};