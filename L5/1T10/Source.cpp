#include "Test.h"

#include<ctime>


int main()
{
	Test test("test.txt");

	test.showProperites();

	test.addArrSize(1000);
	test.addArrSize(50000);
	test.addArrSize(100000);
	test.addArrSize(500000);
	test.addArrSize(1000000);

	test.addFillRange(0);
	test.addFillRange(25);
	test.addFillRange(50);
	test.addFillRange(75);
	test.addFillRange(95);
	test.addFillRange(99);
	test.addFillRange(99.7);
	/*test.addFillRange(-25);
	test.addFillRange(-50);
	test.addFillRange(-75);
	test.addFillRange(-95);
	test.addFillRange(-99);
	test.addFillRange(-99.7);*/
	test.addFillRange(-100);

	test.addRepeats(10);

	test.showProperites();
	test.runTest();
	test.showStats();

	while (1);
	return 0;
}