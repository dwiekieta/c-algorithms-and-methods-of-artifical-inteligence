#pragma once

#include <iostream>
#include <string>

#include "MyQeueArray.h"

class ElementStats
{
	std::string Title;
	int SortSize;
	MyQeueArray<double> *Time;
	double Mean;

public:
	ElementStats(const std::string&title = "");
	~ElementStats();

	void addTime(const double&);
	double countMean();
	void showStats(const bool&showTime = false) const;
};

