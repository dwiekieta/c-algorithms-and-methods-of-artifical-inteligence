#pragma once

#include <ctime>

#include "MyQeueArray.h"


struct SortStats
{
	time_t BubleSortTime;
};


template<class T>
class MySort
{
	MyQeueArray<T> OrginalData;
	MyQeueArray<T> SortData;

	SortStats Stats;

public:
	MySort();
	~MySort();

	void setData(const MyQeueArray<T>&);
	void addElement(const T&);

	void showData() const;

};