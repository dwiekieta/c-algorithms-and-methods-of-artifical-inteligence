#include <iostream>

#include "MySort.h"

template<class T>
MySort<T>::MySort()
{
	Stats.BubleSortTime = 0;
}

template<class T>
MySort<T>::~MySort()
{
	OrginalData.removeQeue();
	SortData.removeQeue();
}

template<class T>
void MySort<T>::setData(const MyQeueArray<T> &q)
{
	OrginalData  = SortData = q;
}

template<class T>
void MySort<T>::addElement(const T &t)
{
	OrginalData.addElement(t);
	SortData.addElement(t);
}

template<class T>
void MySort<T>::showData() const
{
	std::cout << "Oryginal: \n";
	OrginalData.showQueue();
	std::cout << "Sortowanie: \n";
	SortData.showQueue();
}


template class MySort<int>;