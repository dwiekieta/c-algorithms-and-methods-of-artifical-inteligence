#include <iostream>

#include "MyQeueArray.h"

template<class T>
MyQeueArray<T>::MyQeueArray()
{
	Array = ArrayElement = ArrayEnd = ArrayBegin = nullptr;
}


template<class T>
MyQeueArray<T>::~MyQeueArray()
{
	removeQeue();
}


template<class T>
void MyQeueArray<T>::create()
{
	if (Array != nullptr) removeQeue();

	Array = new T[8];
	ArrayEnd = Array + 8;
	ArrayElement = ArrayBegin = Array;
}


template<class T>
void MyQeueArray<T>::resize()
{
	unsigned long size = ArrayEnd - Array;
	unsigned long fill = ArrayElement - ArrayBegin;
	double fillRange = (double)fill / size;

	//std::cout << "size: " << size << " fill: " << fill << " fillRange: " << fillRange << std::endl;

	T* temp;
	T* tempLast;

	if (fillRange > 0.9)
	{
		temp = new T[size * 2];
		tempLast = temp + (size * 2);
	}
	else if (fillRange < 0.45 && size > 8)
	{
		temp = new T[size / 2];
		tempLast = temp + (size / 2);
	}
	else return;

	for (unsigned long i = 0; i < fill; ++i)
		*(temp + i) = *(ArrayBegin + i);

	delete[] Array;
	Array = ArrayBegin = temp;
	ArrayEnd = tempLast;
	ArrayElement = Array + fill;
}


template<class T>
void MyQeueArray<T>::addElement(const T&val)
{
	if (Array == nullptr) create();

	resize();
	*ArrayElement = val;
	ArrayElement++;
}


template<class T>
bool MyQeueArray<T>::removeElement()
{
	if (!(ArrayElement - ArrayBegin)) return false;

	ArrayBegin++;
	resize();

	return true;
}


template<class T>
void MyQeueArray<T>::removeQeue()
{
	while (removeElement());
	Array = ArrayElement = ArrayEnd = ArrayBegin = nullptr;
}


template<class T>
void MyQeueArray<T>::showQueue() const
{
	if (Array == nullptr) return;


	unsigned long size = ArrayElement - ArrayBegin;

	for (unsigned long i = 0; i < size; ++i)
		std::cout << "[ " << i << " ] " << *(ArrayBegin + i) << std::endl;
}

template<class T>
int MyQeueArray<T>::sizeQueue() const
{
	return ArrayElement - ArrayBegin;
}

template<class T>
T& MyQeueArray<T>::operator[](const int &i) const
{
	return *(ArrayBegin + i);
}

template<class T>
T& MyQeueArray<T>::operator[](const int &i)
{
	return *(ArrayBegin + i);
}

template<class T>
MyQeueArray<T>& MyQeueArray<T>::operator=(const MyQeueArray<T> &q)
{
	if (this->sizeQueue()) this->removeQeue();

	for (int i = 0; i < q.sizeQueue(); ++i)
		this->addElement(q[i]);

	return *this;
}








template class MyQeueArray<int>;