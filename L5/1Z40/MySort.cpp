#include <iostream>

#include "MySort.h"

template<class T>
MySort<T>::MySort()
{
	resetStats();
}

template<class T>
MySort<T>::~MySort()
{
	OrginalData.removeQeue();
	SortData.removeQeue();
}

template<class T>
void MySort<T>::resetStats()
{
	Stats.BubleSort = false;
	Stats.BubleSortTime = 0;
}

template<class T>
void MySort<T>::showHeap(const int &pos)
{
	if (2 * pos + 1 < SortData.sizeQueue()) showHeap(2 * pos + 1);

	int deep = 0;
	int i = pos;
	if (pos)
	do
	{
		i = (i - 1) / 2;
		deep++;
	} while (i);

	for (int d = deep; d; --d) std::cout << " ";
	std::cout << "[" << deep << "] " << SortData[pos] << std::endl;

	if (2 * pos + 2 < SortData.sizeQueue()) showHeap(2 * pos + 2);
}

template<class T>
void MySort<T>::mergeSplit(const int&begin, const int&end, const bool&up, const bool&show)
{
	if ((end - begin) < 2) return;

	int half = ((begin + end) / 2);
	std::cout << "b: " << begin << " e: " << end << " p: " << half << std::endl;

	mergeSplit(begin, half, up, show);
	mergeSplit(half, end, up, show);

	int nextIndex, temp, zmian, s;
	do
	{
		zmian = 0, s = 0;

		for (int index = begin; index < (end-1); ++index)
		{
			nextIndex = index + 1;

			if ((up && SortData[index] > SortData[nextIndex]) || (!up && SortData[index] < SortData[nextIndex]))
			{
				temp = SortData[index];
				SortData[index] = SortData[nextIndex];
				SortData[nextIndex] = temp;

				zmian++;
			}
		}

		if (show)
		{
			std::cout << "[[" << s++ << "]] ";
			for (int i = 0; i < begin; ++i) std::cout << SortData[i] << " ";
			std::cout << "    (";
			for (int i = begin; i < end; ++i) std::cout << SortData[i] << " ";
			std::cout << ")    ";
			for (int i = end; i < SortData.sizeQueue(); ++i) std::cout << SortData[i] << " ";
			std::cout << std::endl;
		}
	} while (zmian);

}

template<class T>
void MySort<T>::setData(const MyQeueArray<T> &q)
{
	resetStats();
	OrginalData  = SortData = q;
}

template<class T>
void MySort<T>::addElement(const T &t)
{
	OrginalData.addElement(t);
	SortData.addElement(t);
}

template<class T>
void MySort<T>::showData() const
{
	std::cout << "Oryginal: \n";
	OrginalData.showQueue();
}

template<class T>
void MySort<T>::bubleSort(const bool &up, const bool &show)
{
	clock_t startTime, stopTime;
	int powtorzen = 0;
	double generalTime = 0;

	do
	{
		SortData = OrginalData;
		powtorzen++;

		startTime = clock();

		int s = 0;

		int nextIndex, temp, zmian;
		do
		{
			zmian = 0;
			
			for (int i = 0; i < SortData.sizeQueue()-1; ++i)
			{
				nextIndex = i + 1;

				if ((up && SortData[i] > SortData[nextIndex]) || (!up && SortData[i] < SortData[nextIndex]))
				{
					temp = SortData[i];
					SortData[i] = SortData[nextIndex];
					SortData[nextIndex] = temp;

					zmian++;

					if (show)
					{
						std::cout << "[" << s++ << "] ";
						SortData.showQueue(true);
					}
				}
			}
		} while (zmian);

		stopTime = clock();
		generalTime += difftime(stopTime, startTime);
	} while (generalTime < 10);


	std::cout << "Sortowanie babelkowe [";
	if(up) std::cout << "up] (";
	else std::cout << "down] (";
	std::cout << generalTime / powtorzen << " ms)\n";
	SortData.showQueue(true);

	Stats.BubleSort = true;
	Stats.BubleSortTime = generalTime / powtorzen;
}


template<class T>
void MySort<T>::insertSort(const T &el, const bool &up, const bool &show)
{
	clock_t startTime, stopTime;
	int powtorzen = 0;
	double generalTime = 0;

	OrginalData.addElement(el);
	SortData.addElement(el);

	if (OrginalData.sizeQueue() == 1) return;

	MyQeueArray<T> tempData;
	tempData = SortData;

	do
	{
		SortData = tempData;
		powtorzen++;

		startTime = clock();

		int s = 0;

		int index, nextIndex, temp;

		nextIndex = index = SortData.sizeQueue() - 1;;

		while (nextIndex)
		{
			nextIndex--;

			if ((!up && SortData[index] > SortData[nextIndex]) || (up && SortData[index] < SortData[nextIndex]))
			{
				temp = SortData[index];
				SortData[index] = SortData[nextIndex];
				SortData[nextIndex] = temp;

				if (show)
				{
					std::cout << "[" << s++ << "] ";
					SortData.showQueue(true);
				}
			}

			index--;
		}

		stopTime = clock();
		generalTime += difftime(stopTime, startTime);

	} while (generalTime < 10);


	std::cout << "Sortowanie przez wstawianie {" << el << "} [";
	if (up) std::cout << "up] (";
	else std::cout << "down] (";
	std::cout << generalTime / powtorzen << " ms)\n";
	SortData.showQueue(true);
}

template<class T>
void MySort<T>::heapSort(const T &el, const bool &up, const bool &show)
{
	clock_t startTime, stopTime;
	int powtorzen = 0;
	double generalTime = 0;

	OrginalData.addElement(el);
	SortData.addElement(el);

	if (OrginalData.sizeQueue() == 1) return;

	MyQeueArray<T> tempData;
	tempData = SortData;

	do
	{
		SortData = tempData;
		powtorzen++;

		startTime = clock();

		int s = 0;

		int index, nextIndex, temp;

		nextIndex = index = SortData.sizeQueue() - 1;;

		while (nextIndex)
		{
			nextIndex--;
			nextIndex /= 2;

			if ((up && SortData[index] > SortData[nextIndex]) || (!up && SortData[index] < SortData[nextIndex]))
			{
				temp = SortData[index];
				SortData[index] = SortData[nextIndex];
				SortData[nextIndex] = temp;

				if (show)
				{
					std::cout << "[[" << s++ << "]] \n";
					showHeap();
				}
			}

			index = nextIndex;
		}

		stopTime = clock();
		generalTime += difftime(stopTime, startTime);

	} while (generalTime < 10);


	std::cout << "Sortowanie przez kopcowanie {" << el << "} [";
	if (up) std::cout << "up] (";
	else std::cout << "down] (";
	std::cout << generalTime / powtorzen << " ms)\n";
	showHeap();
}

template<class T>
void MySort<T>::mergeSort(const bool &up, const bool &show)
{
	clock_t startTime, stopTime;
	int powtorzen = 0;
	double generalTime = 0;

	do
	{
		startTime = clock();

		if (SortData.sizeQueue()) mergeSplit(0, SortData.sizeQueue(), up, show);

		stopTime = clock();
		generalTime += difftime(stopTime, startTime);
	} while (generalTime < 10);


	std::cout << "Sortowanie przez scalanie [";
	if (up) std::cout << "up] (";
	else std::cout << "down] (";
	std::cout << generalTime / powtorzen << " ms)\n";
	SortData.showQueue(true);

	Stats.BubleSort = true;
	Stats.BubleSortTime = generalTime / powtorzen;
}

template class MySort<int>;