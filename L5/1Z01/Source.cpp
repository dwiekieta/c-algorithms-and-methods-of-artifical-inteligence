#include <iostream>

#include "MyQeueArray.h"


int main()
{
	MyQeueArray<int> kolejka, k2;

	for (int i = 0; i < 10; ++i)kolejka.addElement(i);
	kolejka.removeElement();
	kolejka.showQueue();

	std::cout << kolejka.sizeQueue() << std::endl;

	k2 = kolejka;

	k2.showQueue();

	kolejka.removeQeue();

	std::cout << k2[2] << std::endl;

	while (1);
	return 0;
}