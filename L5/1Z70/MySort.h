#pragma once

#include <ctime>

#include "MyQeueArray.h"


struct SortStats
{
	bool BubleSort;
	double BubleSortTime;
};


template<class T>
class MySort
{
	MyQeueArray<T> OrginalData;
	MyQeueArray<T> SortData;

	SortStats Stats;

	void resetStats();

	void showHeap(const int &pos = 0);

	void heapSplit(const int&index, const bool&up, const bool&show);
	void mergeSplit(const int&begin, const int&end, const bool&up, const bool&show);
	void quickSplit(const int&begin, const int&end, const bool&up, const bool&show);
	void introSplit(const int&deep, const int&logN, const int&begin, const int&end, const bool&up, const bool&show);

public:
	MySort();
	~MySort();

	void setData(const MyQeueArray<T>&);
	void addElement(const T&);

	void showData() const;

	void insertSort(const T&element, const bool&up = true, const bool &show = false);
	void heapSort(const T&element, const bool&up = true, const bool &show = false);

	void heapSort(const bool&up = true, const bool &show = false);
	void bubleSort(const bool&up = true, const bool &show = false);
	void mergeSort(const bool&up = true, const bool &show = false);
	void shellSort(const bool&up = true, const bool &show = false);
	void quickSort(const bool&up = true, const bool &show = false);
	void introSort(const bool&up = true, const bool &show = false);
};