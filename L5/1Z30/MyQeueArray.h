#pragma once


template<class T>
class MyQeueArray
{
	T* Array;
	T* ArrayBegin;
	T* ArrayEnd;
	T* ArrayElement;

	void create();
	void resize();

public:
	MyQeueArray();
	~MyQeueArray();

	void addElement(const T&);
	bool removeElement();
	void removeQeue();

	void showQueue(const bool &horizontal = false) const;

	int sizeQueue() const;

	T& operator[](const int&) const;
	T& operator[](const int&);

	MyQeueArray& operator=(const MyQeueArray&);
};
