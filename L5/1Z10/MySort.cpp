#include <iostream>

#include "MySort.h"

template<class T>
MySort<T>::MySort()
{
	resetStats();
}

template<class T>
MySort<T>::~MySort()
{
	OrginalData.removeQeue();
	SortData.removeQeue();
}

template<class T>
void MySort<T>::resetStats()
{
	Stats.BubleSort = false;
	Stats.BubleSortTime = 0;
}

template<class T>
void MySort<T>::setData(const MyQeueArray<T> &q)
{
	resetStats();
	OrginalData  = SortData = q;
}

template<class T>
void MySort<T>::addElement(const T &t)
{
	OrginalData.addElement(t);
	SortData.addElement(t);
}

template<class T>
void MySort<T>::showData() const
{
	std::cout << "Oryginal: \n";
	OrginalData.showQueue();
}

template<class T>
void MySort<T>::bubleSort(const bool &up)
{
	clock_t startTime, stopTime, generalTime;
	double powtorzen = 0;

	generalTime = 0;

	do
	{
		SortData = OrginalData;
		powtorzen++;

		startTime = clock();

		int nextIndex, temp, zmian;
		do
		{
			zmian = 0;

			for (int i = 0; i < SortData.sizeQueue()-1; ++i)
			{
				nextIndex = i + 1;

				if (SortData[i] > SortData[nextIndex])
				{
					temp = SortData[i];
					SortData[i] = SortData[nextIndex];
					SortData[nextIndex] = temp;

					zmian++;
				}
			}


			
			std::cout << temp << std::endl;

		} while (zmian);

		stopTime = clock();
		generalTime += difftime(stopTime, startTime);
	} while (generalTime < 10);


	std::cout << "Sortowanie babelkowe [" << powtorzen << "] (" << generalTime / powtorzen << " ms)\n";
	SortData.showQueue(true);

	Stats.BubleSort = true;
	Stats.BubleSortTime = generalTime / powtorzen;
}


template class MySort<int>;