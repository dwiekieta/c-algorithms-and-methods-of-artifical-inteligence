#pragma once

#include <ctime>

#include "MyQeueArray.h"


struct SortStats
{
	bool BubleSort;
	clock_t BubleSortTime;
};


template<class T>
class MySort
{
	MyQeueArray<T> OrginalData;
	MyQeueArray<T> SortData;

	SortStats Stats;

	void resetStats();

public:
	MySort();
	~MySort();

	void setData(const MyQeueArray<T>&);
	void addElement(const T&);

	void showData() const;

	void bubleSort(const bool &up = true);
};