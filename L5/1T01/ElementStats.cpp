#include "ElementStats.h"



ElementStats::ElementStats(const std::string&title, const int&size)
{
	Title = title;
	SortSize = size;
	Time = new MyQeueArray<double>;
	Mean = 0;
}


ElementStats::~ElementStats()
{
	delete Time;
}

void ElementStats::addTime(const double&time)
{
	Time->addElement(time);
}

std::string ElementStats::getTitle() const
{
	return Title;
}

MyQeueArray<double>* ElementStats::getTime()
{
	return Time;
}

double ElementStats::getMean()
{
	return Mean;
}

int ElementStats::getSortSize()
{
	return SortSize;
}

int ElementStats::getTimeRep()
{
	return Time->sizeQueue();
}

void ElementStats::countMean()
{
	if (Time->sizeQueue() == 0) return;

	Mean = Time->operator[](0);

	for (int i = 1; i < Time->sizeQueue() - 1; ++i)
	{
		Mean += Time->operator[](i);
		Mean /= 2;
	}
}

void ElementStats::showStats(const bool&showTime) const
{
	std::cout << Title << " dla " << SortSize << " el." << std::endl;
	std::cout << "Czas sortowania: " << Mean << "[ms]" << std::endl;

	if (showTime)
	{
		std::cout << "Miedzyczasy:" << std::endl;
		Time->showQueue();
	}
}