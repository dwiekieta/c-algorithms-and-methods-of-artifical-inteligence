#include "GeneralStats.h"



GeneralStats::GeneralStats()
{
	file.open("Statystyki sortowania.txt", std::ios::in || std::ios::trunc);
}


GeneralStats::~GeneralStats()
{
	file.close();
}

void GeneralStats::addStats(ElementStats*el)
{
	Stats.addElement(el);

	file << "            " << el->getTitle << '\n'
		<< "wielkosc tablicy: " << el->getSortSize << '\n'
		<< "ilosc powtorzen: " << el->getTimeRep() << '\n'
		<< "sredni czas: " << el->getMean << '\n'
		<< "czasy: \n";

	for (int i = 0; i < el->getTimeRep(); ++i)
		file << "[" << i << "] " << el->getTime[i] << '\n';

}
