#include "ElementStats.h"
#include "GeneralStats.h"

#include<ctime>


int main()
{
	GeneralStats gs;
	ElementStats *staty = new ElementStats("testowe statystyki", 10);
	srand(clock());
	for (int i = 0; i < 100; ++i) staty->addTime(rand() % 100);
	staty->countMean();
	staty->showStats(true);

	gs.addStats(staty);

	while (1);
	return 0;
}