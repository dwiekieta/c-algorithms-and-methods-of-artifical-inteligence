#pragma once

#include <iostream>
#include <string>

#include "MyQeueArray.h"

class ElementStats
{
	std::string Title;
	int SortSize;
	MyQeueArray<double> *Time;
	double Mean;

public:
	ElementStats(const std::string&title = "", const int&size = 0);
	~ElementStats();

	void addTime(const double&);

	std::string getTitle() const;
	MyQeueArray<double>* getTime();
	double getMean();
	int getSortSize();
	int getTimeRep();

	void countMean();
	void showStats(const bool&showTime = false) const;
};

