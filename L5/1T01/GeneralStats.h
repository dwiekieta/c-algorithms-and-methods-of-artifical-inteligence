#pragma once
#include <fstream>

#include "MyQeueArray.h"
#include "ElementStats.h"
class GeneralStats
{
	std::fstream file;
	MyQeueArray<ElementStats*> Stats;

public:
	GeneralStats();
	~GeneralStats();

	void addStats(ElementStats*);
};

