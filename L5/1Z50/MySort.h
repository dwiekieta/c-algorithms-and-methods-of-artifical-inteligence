#pragma once

#include <ctime>

#include "MyQeueArray.h"


struct SortStats
{
	bool BubleSort;
	double BubleSortTime;
};


template<class T>
class MySort
{
	MyQeueArray<T> OrginalData;
	MyQeueArray<T> SortData;

	SortStats Stats;

	void resetStats();

	void showHeap(const int &pos = 0);

	void mergeSplit(const int&begin, const int&end, const bool&up, const bool&show);

public:
	MySort();
	~MySort();

	void setData(const MyQeueArray<T>&);
	void addElement(const T&);

	void showData() const;

	void insertSort(const T&element, const bool&up = true, const bool &show = false);
	void heapSort(const T&element, const bool&up = true, const bool &show = false);

	void bubleSort(const bool&up = true, const bool &show = false);
	void mergeSort(const bool&up = true, const bool &show = false);
	void shellSort(const bool&up = true, const bool &show = false);
};