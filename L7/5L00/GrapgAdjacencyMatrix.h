#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <cstdlib>


template<class T>
class GraphAdjMatrix
{
	int **AdjacencyMatrix;
	T **DataVector;

	int MatrixSize;
	int GraphSize;
	int GraphNettoSize;

	void resize(const int&overSize = 6); // <-------------- ROZMIAR GRAFU
	int max(const int&, const int&);

	void showMatrix(int**, std::string);

public:
	GraphAdjMatrix();
	~GraphAdjMatrix();

	void addNode(const T&);
	void addNode(const int&, const T&);

	void addEdge(const int&p, const int&k, const int&r, const bool&symetrical = true);

	void autoFill(const int&size, const double&fillRange);

	void removeNode(const int&);
	void removeGraph();

	int readFile(const std::string&);
	int writeFile(const std::string&);

	void prim();
	void dijkstra(const int&);

	void showGraph() const;
};

template<class T>
GraphAdjMatrix<T>::GraphAdjMatrix()
{
	/* (1) Przygotownie przestrzeni poczatkowej grafu */
	GraphSize = GraphNettoSize = MatrixSize = 0;
	resize();

	//showGraph();
}


template<class T>
GraphAdjMatrix<T>::~GraphAdjMatrix()
{
	removeGraph();
}


template<class T>
void GraphAdjMatrix<T>::resize(const int &overSize)
{
	double fillRange, sizeRange;
	int newSize = 0;

	/* Metoda moze byc wykonana w dwoch wariantach *
	*	(1) Utworzenia macierzy i wektora danych dla konstruktora grafu
	*	(2) Zmiany rozmiaru macierzy grafu i wektora danych bez mozliwosci ustalenia wielkosci mniejszej niz 8x8
	*/

	// Sprawdzenie wariantu otwarcia metody 
	if (!MatrixSize)
	{
		newSize = overSize;

		AdjacencyMatrix = new int*[newSize];
		DataVector = new T*[newSize];

		for (int i = 0; i < newSize; ++i)
		{
			AdjacencyMatrix[i] = new int[newSize];
			DataVector[i] = nullptr;

			for (int j = 0; j < newSize; ++j)
				AdjacencyMatrix[i][j] = 0;
		}

	}

	MatrixSize = newSize;

	if (overSize) GraphSize = GraphNettoSize = overSize;
	else if (GraphNettoSize < GraphSize) GraphSize = GraphNettoSize;
}


template<class T>
int GraphAdjMatrix<T>::max(const int&a, const int&b)
{
	if (a > b) return a;
	else return b;
}


template<class T>
void GraphAdjMatrix<T>::showMatrix(int **matrix, std::string name)
{
	std::cout << "\n\n" << name << ":\n";

	//std::cout.width(8);
	std::cout << "[/]     ";

	for (int i = 0; i < GraphSize; ++i)
		std::cout << "[" << std::setw(6) << i << "]";

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << std::setw(6) << i << "]";

		for (int j = 0; j < GraphSize; ++j)
			std::cout << std::setw(8) << matrix[i][j];

		std::cout << std::endl;
	}
}


template<class T>
void GraphAdjMatrix<T>::addNode(const T&val)
{
	/* Metoda dodaje okreslona wartosc z losowym wektorem polaczen;
	*
	*/
	resize();

	DataVector[GraphSize] = new T;
	*DataVector[GraphSize] = val;

	int *edgeNets = new int[GraphSize];

	for (int i = 0; i < GraphSize; ++i)
		edgeNets[i] = rand() % 10;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[GraphSize][i] = edgeNets[i];
		AdjacencyMatrix[i][GraphSize] = edgeNets[i];
	}
	
	GraphSize++;
	GraphNettoSize++;

	delete[] edgeNets;
}

template<class T>
void GraphAdjMatrix<T>::addNode(const int&index, const T&val)
{
	/* Metoda dodania konkretnego wezla */

	resize(index + 1);

	DataVector[index] = new T;
	*DataVector[index] = val;

}

template<class T>
void GraphAdjMatrix<T>::addEdge(const int&p, const int&k, const int&r, const bool&symmetrical)
{
	/* Metoda dodawania skierowanego 
	*	p - wezel poczatkowy
	*	k - wezel koncowy
	*	r - waga krawedzi
	*	sym. - symetrycznosc krawedzi
	*/

	AdjacencyMatrix[p][k] = r;
	if (symmetrical) AdjacencyMatrix[k][p] = r;
}


template<class T>
void GraphAdjMatrix<T>::autoFill(const int&size, const double&fillRange)
{
	int nets, step, randNet;
	int *netVect;

	for (int i = 0; i < size; ++i)
	{
		GraphSize++;
		GraphNettoSize++;

		resize();

		// Wyznaczenie ilosci polaczen dla danego wypelnienia grafu
		nets = (GraphSize * fillRange) + 1;
		if (nets > 1) step = ((GraphSize - nets) / (nets - 1)) + 1;
		else step = 0;

		// Wypelnienie wektora polaczen
		randNet = 0;
		netVect = new int[GraphSize];
		for (int n = 0; n < GraphSize; ++n)
		{
			if (n == randNet)
			{
				netVect[n] = (rand() % 100) + 1;
				randNet += step;
				continue;
			}

			netVect[n] = 0;
		}

		// Przepisanie wektora polaczen
		for (int v = 0; v < GraphSize; ++v)
		{
			AdjacencyMatrix[GraphSize][v] = netVect[v];
			AdjacencyMatrix[v][GraphSize] = netVect[v];
		}

		delete[] netVect;
	}
}

template<class T>
void GraphAdjMatrix<T>::removeNode(const int&node)
{
	if (node > GraphSize) return;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[node][i] = -1;
		AdjacencyMatrix[i][node] = -1;
	}

	GraphNettoSize--;

	resize();
}


template<class T>
void GraphAdjMatrix<T>::removeGraph()
{
	if (GraphSize)
	{
		delete[] DataVector;

		for (int i = 0; i < MatrixSize; ++i)
			delete[] AdjacencyMatrix[i];
		delete[] AdjacencyMatrix;
	}
}


template<class T>
void GraphAdjMatrix<T>::prim()
{
	/*
	*
	*/
	resize();

	bool *visitedArray = new bool[GraphSize];
	int **minimalEdges = new int*[GraphSize];

	int edges = 0;												// liczba znalezionych krawedzi

	for (int i = 0; i < GraphSize; ++i)
	{
		visitedArray[i] = false;

		minimalEdges[i] = new int[GraphSize];
		for (int j = 0; j < GraphSize; ++j)
			minimalEdges[i][j] = 0;
	}

	//showMatrix(AdjacencyMatrix, "Graf");
	std::cout << "Krawedzie Prima:\n";

	// ---------- Wyszukiwanie minimalnych krawedzi ----------
	int min, x, y;
	// Petla glowna
	while (edges < GraphSize - 1)
	{
		visitedArray[0] = true;

		// Sprawdzenie kazdej krawedzi odwiedzonych wezlow
		min = 0;
		for (int v = 0; v < GraphSize; ++v)
		{
			// Jezeli tu nie bylem to nie sprawdzam
			if (visitedArray[v] == false)
				break;

			for (int i = 0; i < GraphSize; ++i)
			{
				if (!min && AdjacencyMatrix[v][i] > 0 && visitedArray[i] == false)
				{
					min = AdjacencyMatrix[v][i];
					x = v;
					y = i;
				}
				if (min && AdjacencyMatrix[v][i] > 0 && AdjacencyMatrix[v][i] < min && visitedArray[i] == false)
				{
					min = AdjacencyMatrix[v][i];
					x = v;
					y = i;
				}
			}
		}

		if (min)
		{
			minimalEdges[x][y] = minimalEdges[y][x] = min;
			visitedArray[y] = true;
			std::cout << "[" << edges << "] x: " << x << " y: " << y << " min: " << min << std::endl;
			++edges;
		}
	};

}


template<class T>
void GraphAdjMatrix<T>::dijkstra(const int &v)
{
	/*
	*
	*/
	resize();

	bool *QVect = new bool[GraphSize];							// wektor przestrzeni wierzcholkow niezbadanych
	int *pArray = new int[GraphSize];							// tablica poprzednikow
	int *dArray = new int[GraphSize];							// tablica sum wag
	int **minimalEdges = new int*[GraphSize];					// macierz Dijkstry

	int edges = GraphSize;												// liczba znalezionych krawedzi

	for (int i = 0; i < GraphSize; ++i)
	{
		QVect[i] = true;
		dArray[i] = 99999;
		pArray[i] = -1;

		minimalEdges[i] = new int[GraphSize];
		for (int j = 0; j < GraphSize; ++j)
		{
			minimalEdges[i][j] = 0;
		}
	}

	//showMatrix(AdjacencyMatrix, "Graf");

	dArray[v] = 0;
	QVect[v] = false;

	int min, d, d_;
	d = v;
	while(edges)
	{
		// Wyszukanie krawedzi o najmniejszej wadze
		min = 0;
		for (int e = 0; e < GraphSize; ++e)
		{
			if (!min && AdjacencyMatrix[d][e] > 0 && QVect[e])
			{
				min = AdjacencyMatrix[d][e];
				d_ = e;
			}
			if (min && AdjacencyMatrix[d][e] > 0 && AdjacencyMatrix[d][e] < min && QVect[e]) min = AdjacencyMatrix[d][e];
		}

		// Dodanie krawedzi do scieszki
		if (min)
		{
			for (int e = 0; e < GraphSize; ++e)
			{
				if (AdjacencyMatrix[d][e] == min && dArray[e] > dArray[d] + min)
				{
					dArray[e] = dArray[d] + min;
					pArray[e] = d;

					d_ = e;

					QVect[e] = false;
				}
			}

			//std::cout << "d: " << d << " d_: " << d_ << " min: " << min << " edg: " << edges << std::endl;


			d = d_;
		}
		else
		{
			++d;
			if (d > GraphSize - 1) d = 0;
		}

		// Aktualizacja stanu aktywnych wierzcholkow
		edges = 0;
		for (int i = 0; i < GraphSize; ++i)
			if (QVect[i] == true) ++edges;

		//std::cout << "Do przetworzenia: " << edges / (double)GraphSize * 100 << "%\n";

	};

	for (int i = 0; i < GraphSize; ++i)
		std::cout << "[" << i << "] d: " << dArray[i] << " p: " << pArray[i] << std::endl;
	std::cout << std::endl;

	// Sciezka 
	int path;
	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << i << " to " << v << "] d:" << dArray[i] << " ";

		path = i;
		do
		{
			std::cout << path << " - ";

			path = pArray[path];
		} while (path != -1);
		
		std::cout << std::endl;
	}

}



template<class T>
void GraphAdjMatrix<T>::showGraph() const
{
	std::cout << "----- Graf -----" << std::endl;
	std::cout << "DataVector: ";

	for (int i = 0; i < GraphSize; ++i)
	{
		//std::cout.width(8);
		if (DataVector[i] != nullptr) std::cout << std::setw(5) << *DataVector[i];
		else std::cout << std::setw(5) << "ND";
	}

	std::cout << "\n\nAdjacencyMatrix:\n";

	//std::cout.width(8);
	std::cout << "[/]     ";

	for (int i = 0; i < GraphSize; ++i)
	{
		//std::cout.width(6);
		std::cout << "[" << std::setw(6) << i << "]";
	}

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << std::setw(6) << i << "]";

		for (int j = 0; j < GraphSize; ++j)
		{
			//std::cout.width(8);
			std::cout << std::setw(8) << AdjacencyMatrix[i][j];
		}

		std::cout << std::endl;
	}
}