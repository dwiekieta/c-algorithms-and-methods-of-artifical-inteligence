#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <cstdlib>


template<class T>
class GraphAdjMatrix
{
	int **AdjacencyMatrix;
	T **DataVector;

	int MatrixSize;
	int GraphSize;
	int GraphNettoSize;

	void resize(const int&overSize = 0);
	int max(const int&, const int&);

	void showMatrix(int**, std::string);

public:
	GraphAdjMatrix();
	~GraphAdjMatrix();

	void addNode(const T&);
	void addNode(const int&, const T&);

	void addEdge(const int&p, const int&k, const int&r, const bool&symetrical = true);

	void autoFill(const int&size, const double&fillRange);

	void removeNode(const int&);
	void removeGraph();

	int readFile(const std::string&);
	int writeFile(const std::string&);

	void kruskal();

	void showGraph() const;
};

template<class T>
GraphAdjMatrix<T>::GraphAdjMatrix()
{
	/* (1) Przygotownie przestrzeni poczatkowej grafu */
	GraphSize = GraphNettoSize = MatrixSize = 0;
	resize();

	//showGraph();
}


template<class T>
GraphAdjMatrix<T>::~GraphAdjMatrix()
{
	removeGraph();
}


template<class T>
void GraphAdjMatrix<T>::resize(const int &overSize)
{
	double fillRange, sizeRange;
	int newSize = 0;

	/* Metoda moze byc wykonana w dwoch wariantach *
	*	(1) Utworzenia macierzy i wektora danych dla konstruktora grafu
	*	(2) Zmiany rozmiaru macierzy grafu i wektora danych bez mozliwosci ustalenia wielkosci mniejszej niz 8x8
	*/

	// Sprawdzenie wariantu otwarcia metody 
	if (!MatrixSize) 
		newSize = 8;
	else
	{
		fillRange = GraphNettoSize / (double)MatrixSize;
		sizeRange = overSize / (double)MatrixSize;

		if (sizeRange > 0.85)
		{
			newSize = 2 * MatrixSize;

			sizeRange = overSize / (double)newSize;
			// Dostosowanie wilekosci do duzego zapisu ponad stan
			while (sizeRange > 0.85)
			{
				newSize *= 2;
				sizeRange = GraphSize / (double)newSize;
			}
		}
		else if (fillRange > 0.85) newSize = MatrixSize * 2;
		else if (fillRange < 0.35 && GraphSize > 8) newSize = MatrixSize / 2;
	}

	if (!newSize) return;

	// Przestrzen pamieci dla macierzy grafu i wektora wskaznikow na dane - ostatni krok procedury tworzenia macierzy
	int **newMatrix;
	T **newVector;

	newMatrix = new int*[newSize];
	newVector = new T*[newSize];

	for (int i = 0; i < newSize; ++i)
	{
		newMatrix[i] = new int[newSize];
		newVector[i] = nullptr;

		for (int j = 0; j < newSize; ++j)
			newMatrix[i][j] = 0;
	}
		
	if (!MatrixSize)
	{
		DataVector = newVector;
		AdjacencyMatrix = newMatrix;
		MatrixSize = newSize;

		return;
	}

	// Jezeli zachodzi zmiana rozmiaru nalezy skopiowac dane z istniejacej marcierzy sasedztwa i wektora danych pomijajac wartosci usuniete(krawedz -1)
	int IndexV = -1, IndexH;
	// Petla pionowa nowych danych
	for (int i = 0; i < newSize; ++i)
	{
		if (i >= MatrixSize)
			continue;
		
		// Modyfikacja polozenia petli starych danych 
		for (++IndexV; (IndexV < MatrixSize) && (AdjacencyMatrix[IndexV][IndexV] == -1); ++IndexV);

		if (IndexV >= GraphSize)
			continue;

		newVector[i] = DataVector[IndexV];

		// Petla pozioma nowych danych
		IndexH = -1;	// zerowanie zmiennej poziomej

		for (int j = 0; j < newSize; ++j)
		{
			if (j >= MatrixSize)
				continue;

			// Korekta wzgledem starej macierzy
			for (++IndexH; (IndexH < MatrixSize) && (AdjacencyMatrix[IndexV][IndexH] == -1); ++IndexH);

			if (IndexH >= GraphSize)
				continue;

			newMatrix[i][j] = AdjacencyMatrix[IndexV][IndexH];
		}
	}

	
	removeGraph();


	DataVector = newVector;
	AdjacencyMatrix = newMatrix;
	MatrixSize = newSize;

	if (overSize) GraphSize = GraphNettoSize = overSize;
	else if (GraphNettoSize < GraphSize) GraphSize = GraphNettoSize;
}


template<class T>
int GraphAdjMatrix<T>::max(const int&a, const int&b)
{
	if (a > b) return a;
	else return b;
}


template<class T>
void GraphAdjMatrix<T>::showMatrix(int **matrix, std::string name)
{
	std::cout << "\n\n" << name << ":\n";

	//std::cout.width(8);
	std::cout << "[/]     ";

	for (int i = 0; i < GraphSize; ++i)
		std::cout << "[" << std::setw(6) << i << "]";

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << std::setw(6) << i << "]";

		for (int j = 0; j < GraphSize; ++j)
			std::cout << std::setw(8) << matrix[i][j];

		std::cout << std::endl;
	}
}


template<class T>
void GraphAdjMatrix<T>::addNode(const T&val)
{
	/* Metoda dodaje okreslona wartosc z losowym wektorem polaczen;
	*
	*/
	resize();

	DataVector[GraphSize] = new T;
	*DataVector[GraphSize] = val;

	int *edgeNets = new int[GraphSize];

	for (int i = 0; i < GraphSize; ++i)
		edgeNets[i] = rand() % 10;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[GraphSize][i] = edgeNets[i];
		AdjacencyMatrix[i][GraphSize] = edgeNets[i];
	}
	
	GraphSize++;
	GraphNettoSize++;

	delete[] edgeNets;
}

template<class T>
void GraphAdjMatrix<T>::addNode(const int&index, const T&val)
{
	/* Metoda dodania konkretnego wezla */

	resize(index + 1);

	DataVector[index] = new T;
	*DataVector[index] = val;

}

template<class T>
void GraphAdjMatrix<T>::addEdge(const int&p, const int&k, const int&r, const bool&symmetrical)
{
	/* Metoda dodawania skierowanego 
	*	p - wezel poczatkowy
	*	k - wezel koncowy
	*	r - waga krawedzi
	*	sym. - symetrycznosc krawedzi
	*/

	resize(max(p, k) + 1);

	AdjacencyMatrix[p][k] = r;
	if (symmetrical) AdjacencyMatrix[k][p] = r;
}


template<class T>
void GraphAdjMatrix<T>::autoFill(const int&size, const double&fillRange)
{
	int nets, step, randNet;
	int *netVect;

	for (int i = 0; i < size; ++i)
	{
		GraphSize++;
		GraphNettoSize++;

		resize();

		// Wyznaczenie ilosci polaczen dla danego wypelnienia grafu
		nets = (GraphSize * fillRange) + 1;
		if (nets > 1) step = ((GraphSize - nets) / (nets - 1)) + 1;
		else step = 0;

		// Wypelnienie wektora polaczen
		randNet = 0;
		netVect = new int[GraphSize];
		for (int n = 0; n < GraphSize; ++n)
		{
			if (n == randNet)
			{
				netVect[n] = (rand() % 100) + 1;
				randNet += step;
				continue;
			}

			netVect[n] = 0;
		}

		// Przepisanie wektora polaczen
		for (int v = 0; v < GraphSize; ++v)
		{
			AdjacencyMatrix[GraphSize][v] = netVect[v];
			AdjacencyMatrix[v][GraphSize] = netVect[v];
		}

		delete[] netVect;
	}
}

template<class T>
void GraphAdjMatrix<T>::removeNode(const int&node)
{
	if (node > GraphSize) return;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[node][i] = -1;
		AdjacencyMatrix[i][node] = -1;
	}

	GraphNettoSize--;

	resize();
}


template<class T>
void GraphAdjMatrix<T>::removeGraph()
{
	if (GraphSize)
	{
		delete[] DataVector;

		for (int i = 0; i < MatrixSize; ++i)
			delete[] AdjacencyMatrix[i];
		delete[] AdjacencyMatrix;
	}
}


template<class T>
void GraphAdjMatrix<T>::kruskal()
{
	/*
	*
	*/
	resize();

	int edges = GraphSize*(GraphSize - 1) / 2;						// liczba krawedzi
	int newEdges = 0;												// liczba krawedzi przetworzonych przez algorytm

	bool *visitedArray = new bool[GraphSize];
	int **minimalEdges = new int*[GraphSize];
	int **nodeSets = new int*[GraphSize];							// zbior polaczen miedzy wierzcholkami
	int **coordinates = new int*[edges];							// macierz wsporzednych wierzcholkow i ich wag
	int *connectEeges = new int[edges];								// wektor indeksow pozwalajacych polaczyc zbiory
	int *usedEdges = new int[edges];								// wektor przechwywujacy indeksy krawedzi uzupelnionych w grafie


	for (int i = 0; i < GraphSize; ++i)
	{
		visitedArray[i] = false;

		minimalEdges[i] = new int[GraphSize];
		nodeSets[i] = new int[GraphSize];

		for (int j = 0; j < GraphSize; ++j)
		{
			nodeSets[i][j] = -1;

			if (j <= i)
			{
				minimalEdges[i][j] = 0;
				continue;
			}

			if (AdjacencyMatrix[i][j] < AdjacencyMatrix[j][i])
				minimalEdges[i][j] = AdjacencyMatrix[i][j];
			else
				minimalEdges[i][j] = AdjacencyMatrix[j][i];
		}
	}

	//showMatrix(minimalEdges, "test uzupelnienia");

	for (int i = 0; i < edges; ++i)
	{
		connectEeges[i] = -1;
		usedEdges[i] = -1;

		coordinates[i] = new int[3];

		coordinates[i][0] = coordinates[i][1] = coordinates[i][2] = 0;
	}

	// --------------- Przepisanie minimalnych krawedzi -------------------------------
	int min, x, y;

	for (int index = 0; index < edges; ++index)
	{
		// Wyszukanie minimum macierzy gornej grafu
		min = 0;
		for (int i = 0; i < GraphSize; ++i)
			for (int j = i; j < GraphSize; ++j)
			{
				if (!min && minimalEdges[i][j] > 0)
				{
					min = minimalEdges[i][j];
					x = i;
					y = j;
				}

				if (min && minimalEdges[i][j] > 0 && minimalEdges[i][j] < min)
				{
					min = minimalEdges[i][j];
					x = i;
					y = j;
				}
			}

		// Przepisanie wspozednych i wartosci krawedzi
		if (min)
		{
			coordinates[index][0] = x;
			coordinates[index][1] = y;
			coordinates[index][2] = min;

			minimalEdges[x][y] = 0;
		}
	}

	// ------------- Wyswietlanie krawedzi ------------------------- 
	/*for (int index = 0; index < edges; ++index)
	{
		std::cout << "[" << index << "] x:" << coordinates[index][0] << " y: " << coordinates[index][1] << " min: " << coordinates[index][2] << std::endl;
	}*/

	// ------------- Wielkie czyszczenie macierzy -----------------
	for (int i = 0; i < GraphSize; ++i)
		for (int j = 0; j < GraphSize; ++j)
			minimalEdges[i][j] = 0;

	// ----------- Wielkie uzupelnianie matrixa --------------------
	int useEdge = 0;
	for (int index = 0; index < edges; ++index)
	{
		if (!visitedArray[coordinates[index][0]] || !visitedArray[coordinates[index][1]])
		{
			minimalEdges[coordinates[index][0]][coordinates[index][1]] = coordinates[index][2];
			minimalEdges[coordinates[index][1]][coordinates[index][0]] = coordinates[index][2];

			visitedArray[coordinates[index][0]] = true;
			visitedArray[coordinates[index][1]] = true;

			// dodanie krawedzi do listy uzytych krawedzi
			usedEdges[useEdge++] = index;
			newEdges++;
		}
	}

	//showMatrix(minimalEdges, "MinimalEdges - Kruskal - rozdzielny");

	//--------- Ustalanie zbiorow krawedzi ------------
	bool xApear, yApear;
	useEdge = 0;

	for (int index = 0; index < edges; ++index)
	{
		// Jezeli nie ma juz krawedzi to wyjdz
		if (usedEdges[useEdge] == -1) break;
		if (newEdges == GraphSize - 2) break;

		// Jezeli index nie jest w dziedzinie uzytych indeksow to zwieksz indeks
		if (usedEdges[useEdge] != index) continue;
		else ++useEdge;

		for (int i = 0; i < GraphSize; ++i)
		{
			// Jezeli zbior "i" jest pusty to go zapisz
			if (nodeSets[i][0] == -1)
			{
				nodeSets[i][0] = coordinates[index][0];
				nodeSets[i][1] = coordinates[index][1];

				break;
			}

			// Jezeli zbior "i" ma elementy to sprawdz czy nie naleza do krawedzi
			xApear = false, yApear = false;
			for (int j = 0; j < GraphSize; ++j)
			{
				if (nodeSets[i][j] == -1) break;
				if (nodeSets[i][j] == coordinates[index][0]) xApear = true;
				if (nodeSets[i][j] == coordinates[index][1]) yApear = true;
			}

			// Uzupelnianie zbiorow
			if (xApear && !yApear)
				for (int j = 0; j < GraphSize; ++j)
					if (nodeSets[i][j] == -1)
					{
						nodeSets[i][j] = coordinates[index][1];
						break;
					}
			if (!xApear && yApear)
				for (int j = 0; j < GraphSize; ++j)
					if (nodeSets[i][j] == -1)
					{
						nodeSets[i][j] = coordinates[index][0];
						break;
					}

			// Warunek konca sprawdzania zniorow dla krawedzi
			if (xApear || yApear) break;
		}
	}

	//showMatrix(nodeSets, "test zbiorow");

	// ----------------- Polaczenie zbiorow -----------------------
	// Petla glowna sprawdza wektory zbiorow po kolei az do przedostatniego 
	for (int i = 0; i < GraphSize - 1; ++i)
	{
		// Jezeli kolejny wektor pusty to koniec laczenia
		if (nodeSets[i + 1][0] == -1) break;
		if (newEdges == GraphSize - 2) break;

		// Wyszukanie najlzejszego polaczenia miedzy wektorami(przejscie po kolei po wektorze krawedzi i szukanie dopasowania)
		useEdge = 0;
		for (int conIndex = 0; conIndex < edges; ++conIndex)
		{
			// Jezeli nie ma juz krawedzi to wyjdz
			if (usedEdges[useEdge] == -1) break;

			// Jezeli index jest w dziedzinie uzytych indeksow to zwieksz indeks
			if (usedEdges[useEdge] == conIndex) continue;
			else ++useEdge;

			x = coordinates[conIndex][0];
			y = coordinates[conIndex][1];

			// Przesukanie zbioru w celu znalezienia wierzcholkow 
			xApear = yApear = false;
			// Przeszukanie zbioru 1
			for (int z1 = 0; z1 < GraphSize; ++z1)
			{
				if (nodeSets[i][z1] == x) xApear = true;
				if (nodeSets[i][z1] == y) yApear = true;
			}

			// Sprawdzanie zbioru 2
			if (xApear && !yApear)
				for (int z2 = 0; z2 < GraphSize; ++z2)
					if (nodeSets[i + 1][z2] == y) yApear = true;
				
			if (!xApear && yApear)
				for (int z2 = 0; z2 < GraphSize; ++z2)
					if (nodeSets[i + 1][z2] == x) xApear = true;

			if (xApear && yApear)
			{
				xApear = yApear = false;

				for (int z2 = 0; z2 < GraphSize; ++z2)
					if (nodeSets[i + 1][z2] == x || nodeSets[i + 1][z2] == y) 
						xApear = yApear = true;
			}

			if (xApear && yApear)
			{
				minimalEdges[x][y] = coordinates[conIndex][2];
				minimalEdges[y][x] = coordinates[conIndex][2];

				// Dodanie polacznenia do listy 
				for (int e = 0; e < edges; ++e)
					if (usedEdges[e] == -1)
					{
						usedEdges[e] = conIndex;
						break;
					}

				break;
			}
		}

	}

	//showMatrix(minimalEdges, "MinimalEdges - Kruskal");

	// ------------- Wyswietlanie krawedzi ------------------------- 
	std::cout << "Krawedzie Kruskala:\n";
	useEdge = 0;
	for (int index = 0; index < edges; ++index)
	{
		// Jezeli nie ma juz krawedzi to wyjdz
		if (usedEdges[index] == -1) break;

		std::cout << "[" << index << "] x:" << coordinates[usedEdges[index]][0] << " y: " << coordinates[usedEdges[index]][1] << " min: " << coordinates[usedEdges[index]][2] << std::endl;
	}



	delete[] visitedArray;
	for (int i = 0; i < GraphSize; ++i)
	{
		delete[] minimalEdges[i];
		delete[] nodeSets[i];
	}

	for (int i = 0; i < edges; ++i)
	{
		delete[] coordinates[i];
	}

	delete[] minimalEdges;
	delete[] nodeSets;
	delete[] coordinates;
	delete[] connectEeges;
	delete[] usedEdges;
}


template<class T>
void GraphAdjMatrix<T>::showGraph() const
{
	std::cout << "----- Graf -----" << std::endl;
	std::cout << "DataVector: ";

	for (int i = 0; i < GraphSize; ++i)
	{
		//std::cout.width(8);
		if (DataVector[i] != nullptr) std::cout << std::setw(5) << *DataVector[i];
		else std::cout << std::setw(5) << "ND";
	}

	std::cout << "\n\nAdjacencyMatrix:\n";

	//std::cout.width(8);
	std::cout << "[/]     ";

	for (int i = 0; i < GraphSize; ++i)
	{
		//std::cout.width(6);
		std::cout << "[" << std::setw(6) << i << "]";
	}

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << std::setw(6) << i << "]";

		for (int j = 0; j < GraphSize; ++j)
		{
			//std::cout.width(8);
			std::cout << std::setw(8) << AdjacencyMatrix[i][j];
		}

		std::cout << std::endl;
	}
}