#include "GrapgAdjacencyMatrix.h"


int main()
{
	GraphAdjMatrix<int> graf;

	/*for (int i = 0; i < 100; ++i)
	{
		graf.addNode(i);
		//graf.showGraph();
	}*/

	graf.addEdge(0, 1, 1);
	graf.addEdge(0, 2, 1);
	graf.addEdge(0, 3, 2);
	graf.addEdge(5, 4, 2);
	graf.addEdge(3, 4, 3);
	graf.addEdge(2, 4, 6);
	graf.addEdge(1, 2, 7);
	graf.addEdge(1, 3, 7);
	graf.addEdge(2, 5, 9);

	graf.showGraph();

	clock_t start, stop;
	int P, p;
	double czas = 0, Czas = 0;
	P = 0;
	for (int i = 0; i < 1; ++i)
	{
		p = 0;
		do
		{
			start = clock();

			graf.dijkstra(5);
			//graf.prim();
			//graf.kruskal();

			stop = clock();
			czas += difftime(stop, start);
			++p;
		} while (0);
		czas /= p;

		Czas += czas;

		++P;
	}

	Czas /= P;

	std::cout << "\n time: " << Czas << " ms" << std::endl;

	while (1);
	return 0;
}