#pragma once
#include <iostream>


template<class T>
class GraphAdjMatrix
{
	int **AdjacencyMatrix;
	T **DataVector;

	int MatrixSize;
	int GraphSize;
	int GraphNettoSize;

	void resize();
	int max(const int&, const int&);

public:
	GraphAdjMatrix();
	~GraphAdjMatrix();

	void addNode(const T&);
	void addNode(const int&, const T&);

	void addEdge(const int&p, const int&k, const int&r, const bool&symetrical = true);

	void autoFill(const int&size, const double&fillRange);

	void removeNode(const int&);
	void removeGraph();

	int readVectorFile(const std::string&);
	int readEdgeFile(const std::string&);

	int writeVectorFile(const std::string&);
	int writeEdgeFile(const std::string&);

	

	void showGraph() const;
};

template<class T>
GraphAdjMatrix<T>::GraphAdjMatrix()
{
	/* (1) Przygotownie przestrzeni poczatkowej grafu */
	GraphSize = GraphNettoSize = MatrixSize = 0;
	resize();

	showGraph();
}


template<class T>
GraphAdjMatrix<T>::~GraphAdjMatrix()
{
	removeGraph();
}


template<class T>
void GraphAdjMatrix<T>::resize()
{
	double fillRange, sizeRange;
	int newSize = 0;

	/* Metoda moze byc wykonana w dwoch wariantach *
	*	(1) Utworzenia macierzy i wektora danych dla konstruktora grafu
	*	(2) Zmiany rozmiaru macierzy grafu i wektora danych bez mozliwosci ustalenia wielkosci mniejszej niz 8x8
	*/

	// Sprawdzenie wariantu otwarcia metody 
	if (!MatrixSize) 
		newSize = 8;
	else
	{
		fillRange = GraphNettoSize / (double)MatrixSize;
		sizeRange = GraphSize / (double)MatrixSize;

		if (sizeRange > 0.85)
		{
			newSize = 2 * MatrixSize;

			sizeRange = GraphSize / (double)newSize;
			// Dostosowanie wilekosci do duzego zapisu ponad stan
			while (sizeRange > 0.85)
			{
				newSize *= 2;
				sizeRange = GraphSize / (double)newSize;
			}
		}
		else if (fillRange < 0.35 && GraphSize > 8) newSize = MatrixSize / 2;
	}

	if (!newSize) return;

	// Przestrzen pamieci dla macierzy grafu i wektora wskaznikow na dane - ostatni krok procedury tworzenia macierzy
	int **newMatrix;
	T **newVector;

	newMatrix = new int*[newSize];
	newVector = new T*[newSize];

	for (int i = 0; i < newSize; ++i)
	{
		newMatrix[i] = new int[newSize];
		newVector[i] = nullptr;

		for (int j = 0; j < newSize; ++j)
			newMatrix[i][j] = 0;
	}
		
	if (!MatrixSize)
	{
		DataVector = newVector;
		AdjacencyMatrix = newMatrix;
		MatrixSize = newSize;

		return;
	}

	// Jezeli zachodzi zmiana rozmiaru nalezy skopiowac dane z istniejacej marcierzy sasedztwa i wektora danych pomijajac wartosci usuniete(krawedz -1)
	int IndexV = -1, IndexH;
	// Petla pionowa nowych danych
	for (int i = 0; i < newSize; ++i)
	{
		if (i >= MatrixSize)
			continue;
		
		// Modyfikacja polozenia petli starych danych 
		for (++IndexV; (IndexV < MatrixSize) && (AdjacencyMatrix[IndexV][IndexV] == -1); ++IndexV);

		if (IndexV >= GraphSize)
			continue;

		newVector[i] = DataVector[IndexV];

		// Petla pozioma nowych danych
		IndexH = -1;	// zerowanie zmiennej poziomej

		for (int j = 0; j < newSize; ++j)
		{
			if (j >= MatrixSize)
				continue;

			// Korekta wzgledem starej macierzy
			for (++IndexH; (IndexH < MatrixSize) && (AdjacencyMatrix[IndexV][IndexH] == -1); ++IndexH);

			if (IndexH >= GraphSize)
				continue;

			newMatrix[i][j] = AdjacencyMatrix[IndexV][IndexH];
		}
	}

	
	removeGraph();


	DataVector = newVector;
	AdjacencyMatrix = newMatrix;
	MatrixSize = newSize;
	if (GraphNettoSize < GraphSize) GraphSize = GraphNettoSize;

	showGraph();
}


template<class T>
int GraphAdjMatrix<T>::max(const int&a, const int&b)
{
	if (a > b) return a;
	else return b;
}


template<class T>
void GraphAdjMatrix<T>::addNode(const T&val)
{
	resize();

	DataVector[GraphSize] = new T;
	*DataVector[GraphSize] = val;

	GraphSize++;
	GraphNettoSize++;

	int *edgeNets = new int[GraphSize];

	for (int i = 0; i < GraphSize; ++i)
		edgeNets[i] = rand() % 5;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[GraphSize][i] = edgeNets[i];
		AdjacencyMatrix[i][GraphSize] = edgeNets[i];
	}
		
}

template<class T>
void GraphAdjMatrix<T>::addEdge(const int&p, const int&k, const int&r, const bool&symmetrical)
{
	/* Metoda dodawania skierowanego 
	*	p - wezel poczatkowy
	*	k - wezel koncowy
	*	r - waga krawedzi
	*	sym. - symetrycznosc krawedzi
	*/



	if (GraphSize < max(p, k) + 1)
	{
		GraphNettoSize += max(p, k) - GraphSize + 1;
		GraphSize = max(p, k) + 1;
	}

	resize();

	AdjacencyMatrix[p][k] = r;
	if (symmetrical) AdjacencyMatrix[k][p] = r;
}


template<class T>
void GraphAdjMatrix<T>::removeNode(const int&node)
{
	if (node > GraphSize) return;

	for (int i = 0; i < GraphSize; i++)
	{
		AdjacencyMatrix[node][i] = -1;
		AdjacencyMatrix[i][node] = -1;
	}

	GraphNettoSize--;

	resize();
}


template<class T>
void GraphAdjMatrix<T>::removeGraph()
{
	if (GraphSize)
	{
		delete[] DataVector;

		for (int i = 0; i < MatrixSize; ++i)
			delete[] AdjacencyMatrix[i];
		delete[] AdjacencyMatrix;
	}
}


template<class T>
void GraphAdjMatrix<T>::showGraph() const
{
	std::cout << "----- Graf -----" << std::endl;
	std::cout << "DataVector: ";

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout.width(8);
		if (DataVector[i] != nullptr) std::cout << *DataVector[i];
		else std::cout << "-1";
	}

	std::cout << "\n\nAdjacencyMatrix:\n";

	//std::cout.width(8);
	std::cout << "[/]";

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout.width(6);
		std::cout << "[" << i << "]";
	}

	std::cout << std::endl;

	for (int i = 0; i < GraphSize; ++i)
	{
		std::cout << "[" << i << "] ";

		for (int j = 0; j < GraphSize; ++j)
		{
			std::cout.width(8);
			std::cout << AdjacencyMatrix[i][j];
		}

		std::cout << std::endl;
	}
}