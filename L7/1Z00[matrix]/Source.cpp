#include "GrapgAdjacencyMatrix.h"


int main()
{
	GraphAdjMatrix<int> graf;

	for (int i = 0; i < 10; ++i)
	{
		graf.addNode(i);
		graf.showGraph();
	}

	for (int i = 0; i < 7; ++i)
	{
		graf.removeNode(i);
		graf.showGraph();
	}

	graf.addEdge(19, 20, 9);
	graf.showGraph();

	while (1);
	return 0;
}