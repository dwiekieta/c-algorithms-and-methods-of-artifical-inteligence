#include <iostream>

#include "Statki.h"

int main()
{
	Statki gra;

	gra.utworzPlansze(4);
	gra.dodajStatek(2);
	gra.dodajStatek(3);
	gra.zwodujStatek(0, 2, 0, 2);
	gra.zwodujStatek(1, 1, 2, 1);
	gra.show();

	std::cout << gra.oddajStrzal(gra, 2, 1) << std::endl;
	gra.show();

	std::cout << gra.oddajStrzal(gra, 2, 3) << std::endl;
	gra.show();

	std::cout << gra.oddajStrzal(gra, 2, 2) << std::endl;
	gra.show();

	std::cout << gra.oddajStrzal(gra, 2, 0) << std::endl;
	gra.show();

	while (1);
	return 0;
}