#include "Statki.h"

#include <iostream>
#include <iomanip>

Statki::Statki()
{
	RozmiarTablicyStatkow = 0;
}


Statki::~Statki()
{
	if (!RozmiarTablicyStatkow)
		return;

	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
		delete[] TablicaStatkow[i];

	delete[] TablicaStatkow;
}


/*	Tworzenie planszy do gry
*		metoda utworzPlansze pozwala na utworzenie macierzy kwadratowej o boku n
*			bedacej podstawowa mapa obiektow oraz tablica strzalow.
*
*		parametry wejsciowe:
*			bok macierzy
*
*		parametry wyjsciowe:
*			0 - utworzono
*			1 - macierz juz istnieje
*/
int Statki::utworzPlansze(const int&n)
{
	if (RozmiarTablicyStatkow) return 1;

	TablicaStatkow = new int*[n];
	TablicaStrzalow = new int*[n];

	for (int i = 0; i < n; ++i)
	{
		TablicaStatkow[i] = new int[n];
		TablicaStrzalow[i] = new int[n];

		for (int j = 0; j < n; ++j)
			TablicaStatkow[i][j] = TablicaStrzalow[i][j] = 0;
	}

	RozmiarTablicyStatkow = n;
}


/*	Dodawanie nowego obiektu do listy obiektow 
*		metoda dodajStatek pozwala na dodanie nowego statku, ktory zostanie umieszczony
*			na planszy pod warunkiem, ze suma pol wszsytkich obecnych obiektow
*			oraz nowo dodanego bedzie mniejsza niz pole planszy. Ponadto wielkosc jednostki
*			musi byc mniejsza od dlugosci planszy.
*
*		parametr wejsciowy:
*			dlugosc statku
*
*		parametr wyjsciowy:
*			0 - dodano
*			1 - brak wystarczajacego miejsca na planszy
*/
int Statki::dodajStatek(const int&dlugosc)
{
	if (dlugosc >= RozmiarTablicyStatkow) return 1;

	int sumaObiektow = 0, wolnyObszar;

	for (int i = 0; i < Obiekty.size(); ++i)
		sumaObiektow += Obiekty[i];

	wolnyObszar = RozmiarTablicyStatkow * RozmiarTablicyStatkow - sumaObiektow;

	if (wolnyObszar < 0) return 1;

	Obiekty.push_back(dlugosc);
	ObiektyWykorzystane.push_back(false);

	return 0;
}

/*	Ustawianie obiektu na planszy
*		metoda zwodujStatek pozwala na umieszczenie obiektu na wybranym polu oraz 
*			w okreslonym kierunku. Przed dodaniem obiektu sprawdzana jest mozliwosc 
*			dokonania akcji tj.:
*				1 - czy podany indeks jest w dziedzinie obiektow
*				2 - czy obiekt o podanym indeksie nie zostal juz wykorzystany
*				3 - czy wspolrzedne nalerza do planszy
*				4 - czy w danym kiedunku i okreslonej dlugosci nie ma innego obiektu
*				5 - czy w danym kierunku i okreslonej dlugosci nie ma konca planszy
*			w przypadku spelnienia chociaz jednego warunku metoda nie dodaje obiektu 
*			oraz zwraca wartosc bledu.
*
*		parametry wejsciowe:
*			1 - indeks obiektu 
*			2 - indeks pola startu
*			3 - kierunek dodania obiektu
*				0 = gora
*				1 = prawo
*				2 = dol
*				3 = lewo
*
*		parametry wyjsciowe:
*			0 - obiekt dodano
*			1 - nie ma takiego obiektu
*			2 - obiekt zostal juz wykorzystany
*			3 - wsporzedne nie naleza do planszy
*			4 - w podanym kierunku jest inny obiekt
*			5 - w podanym kierunku jest koniec planszy
*/
int Statki::zwodujStatek(const int&obiekt, const int&Xpole, const int&Ypole, const int&kierunek)
{
	if (obiekt >= Obiekty.size()) return 1;
	if (ObiektyWykorzystane[obiekt]) return 2;
	if (Xpole >= RozmiarTablicyStatkow || Ypole >= RozmiarTablicyStatkow) return 3;

	switch (kierunek)
	{
	case 0: 
	{
		if (Ypole - Obiekty[obiekt] < 0) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole][Ypole - i]) return 4;
	} break;
	case 1:
	{
		if (Xpole + Obiekty[obiekt] > RozmiarTablicyStatkow) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole + i][Ypole]) return 4;
	} break;
	case 2:
	{
		if (Ypole + Obiekty[obiekt] < 0) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole][Ypole + i]) return 4;
	} break;
	case 3:
	{
		if (Xpole - Obiekty[obiekt] > RozmiarTablicyStatkow) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole - i][Ypole]) return 4;
	} break;
	}


	switch (kierunek)
	{
	case 0:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole][Ypole - i] = obiekt + 1;
	 break;
	case 1:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole + i][Ypole] = obiekt + 1;
	break;
	case 2:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole][Ypole + i] = obiekt + 1;
	break;
	case 3:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole - i][Ypole] = obiekt + 1;
	break;
	}

	ObiektyWykorzystane[obiekt] = true;

	return 0;
}


/*	Oddawanie strzalow 
*		metoda oddajStrzal realzuje operacje strzelenia w plansze przeciwnika
*			oraz interpretacji dokonanego strzalu:
*				0 - pudlo
*				1 - strzal udany
*			poprzez otrzymanie odpowiedzi od obiektu celu. 
*			Po zweryfikowaniu odpowiedzi zostaje uzupelniona tablica wykonanych strzalow:
*				-1 - pusty strzal
*				1 - trafienie
*
*		parametry wejsciowe:
*			cel - obiekt klasy Statki posiadajacy rozplanowane okrety oraz przechowywujacy informacje 
*				o stanie swojej floty
*			wspolrzedne ataku
*
*		parametry wyjsciowe:
*			-3 - blad obiektu celu
*			-2 - wsp. poza zakresem
*			-1 - wspolrzedne zostaly juz wykorzystane
*			0 - strzal nieudany
*			1 - strzal udany
*			2 - w wyniku strzalu zatopiono okret
*/
int Statki::oddajStrzal(Statki&cel, const int&Xpole, const int&Ypole)
{
	if (Xpole >= RozmiarTablicyStatkow || Ypole > RozmiarTablicyStatkow) return -2;
	if (TablicaStrzalow[Xpole][Ypole] != 0) return -1;

	int s = cel.odbierzStrzal(Xpole, Ypole);

	if (s < 0) return -3;
	if (s)TablicaStrzalow[Xpole][Ypole] = 1;
	else TablicaStrzalow[Xpole][Ypole] = -1;

	if (s == 2) ObiektyZatopionePrzeciwnika.push_back(cel.getOstatniZatopiony());

	return s;
}


/*	Odbieranie strzalow
*		metoda odbierzStrzal interpretuje atak przeciwnika:
*			atak jest skuteczny wtedy i tylko wtedy gdy:
*				1 - wpspolrzedne ataku sa w zakresie planszy
*				2 - wartosc na planszy pod podanymi wsp. jest dodatnia
*			Skuteczny atak powoduje zmiane elementu o podanych wsp. na element przeciwny, nastepnie
*			sprawdzana jest plansza w okolicy ataku w celu okreslenia skali zniszczenia okretu:
*				1 - wywolywana jest rekurencyjna funkcja sprawdzajaca w kazdym kierunku od podanych		
*						wsp.
*				2 - jezeli w danym kierunku istnieja elementy o tej samej wartosci bezwzglednej to
*						funkcja wywoluje sie dlaej w tym kierunku
*				3 - dojscie do el o innej wartosci badz konca planszy konczy wywolywanie funkcji
*			Jezeli zostala zestrzelona cala jednostka to metoda zwraca odpowiednia wartosc
*
*		parametry wejsciowe:
*			wsp. ataku
*
*		parametry wyjsciowe:
*			-2 - wsp. poza zakresem
*			-1 - w podany punkt juz strzelano
*			0 - pudlo
*			1 - sukces
*			2 - zatopiono jednostke
*/
int Statki::odbierzStrzal(const int&Xpole, const int&Ypole)
{
	if (Xpole >= RozmiarTablicyStatkow || Ypole >= RozmiarTablicyStatkow) return -2;
	if (TablicaStatkow[Xpole][Ypole] < 0) return -1;

	if (TablicaStatkow[Xpole][Ypole] > 0)
	{
		int w = TablicaStatkow[Xpole][Ypole] *= -1;

		if (sprawdzJednostke(w, Xpole, Ypole, 0) && sprawdzJednostke(w, Xpole, Ypole, 1)
			&& sprawdzJednostke(w, Xpole, Ypole, 2) && sprawdzJednostke(w, Xpole, Ypole, 3))
		{
			ObiektyZatopioneGracza.push_back(-w);
			return 2;
		}

		return 1;
	}

	return 0;
}


/*	Sprawdzanie integralnosci jednostki
*		metoda sprawdzJednostke pozwala rekurencyjnie sprawdzic czy okret zawiera niezatopione czesci:
*			jezeli zostanie napotkana niezatopiona czesc metoda zwroci falsz i zakonczy swoje dzialanie.
*
*		parametry wejsciowe:
*			wartosc - determinant wyjscia
*			wsporzedne
*			kierunek sprawdzania:
*				0 - gora
*				1 - prawo
*				2 - dol
*				3 - lewo
*
*		parametry wyjsciowe:
*			prawda - jednostka w tym kierunki jest cala zatopiona
*			falsz - plywa
*/
bool Statki::sprawdzJednostke(const int&wartosc, const int&Xpole, const int&Ypole, const int&kierunek)
{
	if (Xpole < 0 || Xpole >= RozmiarTablicyStatkow || Ypole < 0 || Ypole >= RozmiarTablicyStatkow)
		return true;

	if (TablicaStatkow[Xpole][Ypole] == -wartosc)
		return false;

	if (TablicaStatkow[Xpole][Ypole] != wartosc)
		return true;

	switch (kierunek)
	{
	case 0: return sprawdzJednostke(wartosc, Xpole, Ypole - 1, kierunek);
		break;
	case 1: return sprawdzJednostke(wartosc, Xpole + 1, Ypole, kierunek);
		break;
	case 2: return sprawdzJednostke(wartosc, Xpole, Ypole + 1, kierunek);
		break;
	case 3: return sprawdzJednostke(wartosc, Xpole - 1, Ypole, kierunek);
		break;
	}

	return true;
}


/* Przekazanie informacji o ostanio zatopionym okrecie
*	
*/
int Statki::getOstatniZatopiony() const
{
	return ObiektyZatopioneGracza[ObiektyZatopioneGracza.size() - 1];
}


/* Przekazanie informacji o zwyciestwie gracza
*
*/
int Statki::getWygrana() const
{
	if (ObiektyZatopioneGracza.size() == Obiekty.size()) return -1;
	if (ObiektyZatopionePrzeciwnika.size() == Obiekty.size()) return 1;

	return 0;
}

/*	Czyszczenie tablic pozycji i strzalow
*
*/
void Statki::wyczysc()
{
	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
	{
		ObiektyWykorzystane[i] = false;
		for (int j = 0; j < RozmiarTablicyStatkow; ++j)
			TablicaStatkow[i][j] = TablicaStrzalow[i][j] = 0;
	}

	ObiektyZatopioneGracza.clear();
	ObiektyZatopionePrzeciwnika.clear();
}

/* Informacja o obiektach nie zwodowanych
*		metoda getNieZwodowane zwraca liczbe statkow nie zwodowanych 
*/
int Statki::getNieZwodowane() const
{
	int z = 0;

	for (int i = 0; i < ObiektyWykorzystane.size(); ++i)
		z += (int)(!ObiektyWykorzystane[i]);

	return z;
}

/* Informacja o liczbie statkow
*
*/
int Statki::getIloscObiektow() const
{
	return Obiekty.size();
}

/* Informacja o rozmiarze planszy
*
*/
int Statki::getRozmiar() const
{
	return RozmiarTablicyStatkow;
}


/*	Wyswietlanie zawartosci planszy
*
*/
void Statki::show() const
{
	std::cout << "Obiekty: ";
	for (int i = 0; i < Obiekty.size(); ++i)
		std::cout << Obiekty[i] << ", ";
	std::cout << std::endl;
	std::cout << "Obiekty wykorzystane: ";
	for (int i = 0; i < ObiektyWykorzystane.size(); ++i)
		std::cout << ObiektyWykorzystane[i] << ", ";
	std::cout << std::endl;
	std::cout << "Obiekty zatopione gracza: ";
	for (int i = 0; i < ObiektyZatopioneGracza.size(); ++i)
		std::cout << ObiektyZatopioneGracza[i] << ", ";
	std::cout << std::endl;
	std::cout << "Obiekty zatopione przeciwnika: ";
	for (int i = 0; i < ObiektyZatopionePrzeciwnika.size(); ++i)
		std::cout << ObiektyZatopionePrzeciwnika[i] << ", ";

	std::cout << std::endl;

	std::cout << std::setw((RozmiarTablicyStatkow + 1)  * 6) << "Tablica statkow" 
		<< std::setw((RozmiarTablicyStatkow + 2) * 6) << "Tablica strzalow" << std::endl;

	std::cout << std::setw(6) << "";
	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
		std::cout << "[" << std::setw(4) << i << "]";

	std::cout << std::setw(6) << "" << std::setw(6) << "";
	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
		std::cout << "[" << std::setw(4) << i << "]";

	std::cout << std::endl;

	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
	{
		std::cout << "[" << std::setw(4) << i << "]";

		for (int j = 0; j < RozmiarTablicyStatkow; ++j)
			std::cout << std::setw(6) << TablicaStatkow[j][i];

		std::cout << std::setw(6) << "";

		std::cout << "[" << std::setw(4) << i << "]";

		for (int j = 0; j < RozmiarTablicyStatkow; ++j)
			std::cout << std::setw(6) << (TablicaStrzalow[j][i] ? (TablicaStrzalow[j][i] < 0) ? "-" : "+" : "0");
		std::cout << std::endl;
	}

	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
}