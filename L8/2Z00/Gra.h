#pragma once

#include "Statki.h"

class Gra
{
	Statki *Komputer, *Gracz;

	bool menu();

	bool ustawienia();
	bool gra();
	bool graRozstawienie();
	bool graRozgrywka();

	void naglowek(const std::string&);

	void autoRozstawienie(Statki*);
	void autoRozgrywka(Statki*);

public:
	Gra();
	~Gra();

	void graj();
};

