#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>

#include "Gra.h"



Gra::Gra()
{
}


Gra::~Gra()
{
}

/*	Menu gry
*		[
*/
bool Gra::menu()
{
	system("cls");
	naglowek(" MENU ");

	std::cout << "[  1] Ustawienia\n";
	if (Komputer != nullptr)
		std::cout << "[  2] Graj\n"
		"[  3] Wyjdz";
	else
		std::cout << "[  2] Wyjdz\n";

	int w;
	std::cout << "Wprowadz wybor: ";
	std::cin >> w;

	if (Komputer != nullptr)
		switch (w)
		{
		case 1: while(ustawienia());
			break;
		case 2: while(gra());
			break;
		case 3: return false;
			break;
		}
	else
		switch (w)
		{
		case 1: while(ustawienia());
			break;
		case 2: return false;
			break;
		}

	return true;
}

/* Ustawienia gry
*
*/
bool Gra::ustawienia()
{
	system("cls");
	naglowek(" USTAWIENIA ");

	std::cout << "[  1] Wielkosc planszy\n"
		"[  2] Rodzaje statkow\n"
		"[  3] Powrot\n";

	int w;

	std::cout << "Wprowadz wybor: ";
	if ((std::cin >> w).fail())
	{
		std::cin.clear();
		std::cin.sync();

		return true;
	}

	switch (w)
	{
	case 1: 
	{
		if (Komputer != nullptr)
		{
			delete Komputer;
			delete Gracz;
		}

		Komputer = new Statki;
		Gracz = new Statki;

		system("cls");
		naglowek(" USTAWIENIA - Wielkosc planszy ");
		int p;
		std::cout << "Wprowadz wielkosc planszy: ";
		if ((std::cin >> p).good())
		{
			Komputer->utworzPlansze(p);
			Gracz->utworzPlansze(p);
		}
	} break;
	case 2:
	{
		if (Komputer == nullptr)
		{
			std::cout << "Nie ma planszy!";
			break;
		}

		system("cls");
		naglowek(" USTAWIENIA - Rodzaje statkow ");
		int p;
		std::cout << "Wprowadzaj dlugosci statkow(koniec poprzez wprowadzenie wartosci 9999): ";
		while ((std::cin >> p).good() && p != 9999)
		{
			Komputer->dodajStatek(p);
			Gracz->dodajStatek(p);
		};
	}break;
	case 3: return false;
		break;
	}

	std::cin.clear();
	std::cin.sync();
	
	return true;
}


/* Rozgrywka
*		metoda gra zawiera podstawowe elemnty rozgrywki:
*			1 - ulorzenie statkow
*			2 - rozgrywka
*/
bool Gra::gra()
{
	system("cls");
	naglowek(" GRA ");

	std::cout << "[  1] Rozmieszczenie statkow\n"
		"[  2] Graj\n"
		"[  3] Powrot\n"
		"Wprowadz wybor: ";

	int w;
	std::cin >> w;

	switch (w)
	{
	case 1: 
	{
		while (graRozstawienie());
		autoRozstawienie(Komputer);
	}
		break;
	case 2: 
	{
		while (!Gracz->getWygrana())
		{
			while (graRozgrywka());
			autoRozgrywka(Komputer);
		}

		if (Gracz->getWygrana() == 1)
			std::cout << "Wygrales z komputerem! \n";
		else 
			std::cout << "Przegrales z komputerem! \n";

		while (1);
	}
		break;
	case 3: return false;
		break;
	}

	return true;
}

/* Rozmieszczenie okretow na planszy
*		metoda graRozstawienie pozwala na rozmieszczenie statkow
*/
bool Gra::graRozstawienie()
{
	system("cls");
	naglowek(" GRA - Rozstawienie ");
	Gracz->show();

	int o, x, y, k;

	std::cout << "Wprowadz nr. obiektu, wspolrzedne poczatku statku i kierunek jego polozenia\n"
		"\t(0 - gora, 1 - prawo, 2 - dol, 3 - lewo): [o x y k] = ";
	std::cin >> o >> x >> y >> k;

	Gracz->zwodujStatek(o, x, y, k);

	if (Gracz->getNieZwodowane()) return true;
	return false;
}

void Gra::autoRozstawienie(Statki*gracz)
{
	int x, y, k;

	for (int i = 0; i < gracz->getIloscObiektow(); ++i)
	{
		do
		{
			x = rand() % gracz->getRozmiar();
			y = rand() % gracz->getRozmiar();
			k = rand() % 4;
		} while (gracz->zwodujStatek(i, x, y, k));
	}
}

/* Gra 
*
*/
bool Gra::graRozgrywka()
{
	system("cls");
	naglowek(" GRA - Rozgrywka ");
	Gracz->show();

	std::cout << "Widok planszy komputera\n";
	Komputer->show();

	int x, y;
	std::cout << "Wprowadz wspolrzedne celu: [x y] = ";
	std::cin >> x >> y;

	if(Gracz->oddajStrzal(*Komputer, x, y)) return true;
	return false;
}

void Gra::autoRozgrywka(Statki*gracz)
{
	int x, y, k;
	x = rand() % gracz->getRozmiar();
	y = rand() % gracz->getRozmiar();
	k = -1;

	while (gracz->oddajStrzal(((gracz == Gracz) ? *Komputer : *Gracz), x, y))
	{
		if (k < 0) k = rand() % 4;

		switch (k)
		{
		case 0: --y;
			break;
		case 1: ++x;
			break;
		case 2: ++y;
			break;
		case 3: --x;
			break;
		}
	}
}

/* Watek gry
*
*/
void Gra::graj()
{
	while(menu());
}

/* Grafika naglowka
*
*/
void Gra::naglowek(const std::string&napis)
{
	std::cout << " ______|______             ______|______\n"
		" |\\\\\\\\\\ //////|            |///// \\\\\\\\\\\|\n"
		" |\\\\\\\\\\ //////|            |///// \\\\\\\\\\\|\n"
		" |\\\\\\\\\\ //////|   STATKI   |///// \\\\\\\\\\\|\n"
		" ------|-------            ------|------\n"
		"\\______|_______/          \\______|______/\n"
		" \\            /            \\           /\n"
		"/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\\n"
		<< std::setfill('-') << std::setw(25) << napis << std::setfill('-') << std::setw(20) << ""
		<< std::endl << std::setfill(' ');
	
}