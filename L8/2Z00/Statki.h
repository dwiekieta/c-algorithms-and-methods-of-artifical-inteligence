#pragma once

#include <vector>

class Statki
{
	int RozmiarTablicyStatkow;
	int **TablicaStatkow;
	int **TablicaStrzalow;
	
	std::vector<int> Obiekty;		// lista stakow mozliwych do umieszczenia w tablicy 
	std::vector<bool> ObiektyWykorzystane;
	std::vector<int> ObiektyZatopioneGracza;
	std::vector<int> ObiektyZatopionePrzeciwnika;

	bool sprawdzJednostke(const int&wartosc, const int&Xpole, const int&Ypole, const int&kierunek);

public:
	Statki();
	~Statki();

	int utworzPlansze(const int&);
	int dodajStatek(const int&);
	int zwodujStatek(const int&obiekt, const int&Xpole, const int&Ypole, const int&kierunek);

	int oddajStrzal(Statki&cel, const int&Xpole, const int&Ypole);
	int odbierzStrzal(const int&Xpole, const int&Ypole);

	int getOstatniZatopiony() const;
	int getWygrana() const;

	int getNieZwodowane() const;
	int getIloscObiektow() const;
	int getRozmiar() const;

	void wyczysc();

	void show() const;
};

