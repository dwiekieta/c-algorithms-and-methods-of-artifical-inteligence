#pragma once

#include <vector>

class Statki
{
	int RozmiarTablicyStatkow;
	int **TablicaStatkow;
	int **TablicaStrzalow;
	
	std::vector<int> Obiekty;		// lista stakow mozliwych do umieszczenia w tablicy 
	std::vector<bool> ObiektyWykorzystane;

public:
	Statki();
	~Statki();

	int utworzPlansze(const int&);
	int dodajStatek(const int&);
	int zwodujStatek(const int&obiekt, const int&Xpole, const int&Ypole, const int&kierunek);

	int oddajStrzal(Statki&cel, const int&Xpole, const int&Ypole);
	int odbierzStrzal(const int&Xpole, const int&Ypole);

	void show() const;
};

