#include "Statki.h"

#include <iostream>
#include <iomanip>

Statki::Statki()
{
	RozmiarTablicyStatkow = 0;
}


Statki::~Statki()
{
	if (!RozmiarTablicyStatkow)
		return;

	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
		delete[] TablicaStatkow[i];

	delete[] TablicaStatkow;
}


/*	Tworzenie planszy do gry
*		metoda utworzPlansze pozwala na utworzenie macierzy kwadratowej o boku n
*			bedacej podstawowa mapa obiektow oraz tablica strzalow.
*
*		parametry wejsciowe:
*			bok macierzy
*
*		parametry wyjsciowe:
*			0 - utworzono
*			1 - macierz juz istnieje
*/
int Statki::utworzPlansze(const int&n)
{
	if (RozmiarTablicyStatkow) return 1;

	TablicaStatkow = new int*[n];
	for (int i = 0; i < n; ++i)
	{
		TablicaStatkow[i] = new int[n];

		for (int j = 0; j < n; ++j)
			TablicaStatkow[i][j] = 0;
	}

	RozmiarTablicyStatkow = n;
}


/*	Dodawanie nowego obiektu do listy obiektow 
*		metoda dodajStatek pozwala na dodanie nowego statku, ktory zostanie umieszczony
*			na planszy pod warunkiem, ze suma pol wszsytkich obecnych obiektow
*			oraz nowo dodanego bedzie mniejsza niz pole planszy.
*
*		parametr wejsciowy:
*			dlugosc statku
*
*		parametr wyjsciowy:
*			0 - dodano
*			1 - brak wystarczajacego miejsca na planszy
*/
int Statki::dodajStatek(const int&dlugosc)
{
	int sumaObiektow = 0, wolnyObszar;

	for (int i = 0; i < Obiekty.size(); ++i)
		sumaObiektow += Obiekty[i];

	wolnyObszar = RozmiarTablicyStatkow * RozmiarTablicyStatkow - sumaObiektow;

	if (wolnyObszar < 0) return 1;

	Obiekty.push_back(dlugosc);
	ObiektyWykorzystane.push_back(false);

	return 0;
}

/*	Ustawianie obiektu na planszy
*		metoda zwodujStatek pozwala na umieszczenie obiektu na wybranym polu oraz 
*			w okreslonym kierunku. Przed dodaniem obiektu sprawdzana jest mozliwosc 
*			dokonania akcji tj.:
*				1 - czy podany indeks jest w dziedzinie obiektow
*				2 - czy obiekt o podanym indeksie nie zostal juz wykorzystany
*				3 - czy wspolrzedne nalerza do planszy
*				4 - czy w danym kiedunku i okreslonej dlugosci nie ma innego obiektu
*				5 - czy w danym kierunku i okreslonej dlugosci nie ma konca planszy
*			w przypadku spelnienia chociaz jednego warunku metoda nie dodaje obiektu 
*			oraz zwraca wartosc bledu.
*
*		parametry wejsciowe:
*			1 - indeks obiektu 
*			2 - indeks pola startu
*			3 - kierunek dodania obiektu
*				0 = gora
*				1 = prawo
*				2 = dol
*				3 = lewo
*
*		parametry wyjsciowe:
*			0 - obiekt dodano
*			1 - nie ma takiego obiektu
*			2 - obiekt zostal juz wykorzystany
*			3 - wsporzedne nie naleza do planszy
*			4 - w podanym kierunku jest inny obiekt
*			5 - w podanym kierunku jest koniec planszy
*/
int Statki::zwodujStatek(const int&obiekt, const int&Xpole, const int&Ypole, const int&kierunek)
{
	if (obiekt >= Obiekty.size()) return 1;
	if (ObiektyWykorzystane[obiekt]) return 2;
	if (Xpole >= RozmiarTablicyStatkow || Ypole >= RozmiarTablicyStatkow) return 3;

	switch (kierunek)
	{
	case 0: 
	{
		if (Ypole - Obiekty[obiekt] < 0) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole][Ypole - i]) return 4;
	} break;
	case 1:
	{
		if (Xpole + Obiekty[obiekt] > RozmiarTablicyStatkow) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole + i][Ypole]) return 4;
	} break;
	case 2:
	{
		if (Ypole + Obiekty[obiekt] < 0) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole][Ypole + i]) return 4;
	} break;
	case 3:
	{
		if (Xpole - Obiekty[obiekt] > RozmiarTablicyStatkow) return 5;
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			if (TablicaStatkow[Xpole - i][Ypole]) return 4;
	} break;
	}


	switch (kierunek)
	{
	case 0:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole][Ypole - i] = obiekt + 1;
	 break;
	case 1:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole + i][Ypole] = obiekt + 1;
	break;
	case 2:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole][Ypole + i] = obiekt + 1;
	break;
	case 3:
		for (int i = 0; i < Obiekty[obiekt]; ++i)
			TablicaStatkow[Xpole - i][Ypole] = obiekt + 1;
	break;
	}

	ObiektyWykorzystane[obiekt] = true;

	return 0;
}


/*	Wyswietlanie zawartosci planszy
*
*/
void Statki::show() const
{
	std::cout << "Obiekty: ";
	for (int i = 0; i < Obiekty.size(); ++i)
		std::cout << Obiekty[i] << ", ";

	std::cout << std::endl;
	std::cout << "Tablica statkow\n";
	std::cout << std::setw(6) << "";
	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
		std::cout << "[" << std::setw(4) << i << "]";

	std::cout << std::endl;

	for (int i = 0; i < RozmiarTablicyStatkow; ++i)
	{
		std::cout << "[" << std::setw(4) << i << "]";

		for (int j = 0; j < RozmiarTablicyStatkow; ++j)
			std::cout << std::setw(6) << TablicaStatkow[j][i];
		std::cout << std::endl;
	}

}